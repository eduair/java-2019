import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PanelModificarUsuario extends JPanel {
	private JTextField txtNombre;
	private JPasswordField txtPass;
	private JLabel txtCi;
	private JTextField telefono1field;
	public PanelModificarUsuario(UsuDT usuario) {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JLabel lblCrearUsuario = new JLabel("Modificar Usuario");
		lblCrearUsuario.setFont(new Font("Tekton Pro Cond", Font.PLAIN, 50));
		lblCrearUsuario.setAlignmentX(0.5f);
		add(lblCrearUsuario);
		
		JPanel panel = new JPanel();
		add(panel);
		
		JLabel label_1 = new JLabel("Nombre");
		panel.add(label_1);
		
		txtNombre = new JTextField(usuario.getNombre());
		txtNombre.setColumns(10);
		panel.add(txtNombre);
		
		JPanel panel_1 = new JPanel();
		add(panel_1);
		
		JLabel label_2 = new JLabel("Clave");
		panel_1.add(label_2);
		
		txtPass = new JPasswordField(usuario.getClave());
		txtPass.setColumns(10);
		panel_1.add(txtPass);
		
		JPanel panel_2 = new JPanel();
		add(panel_2);
		
		JLabel label_3 = new JLabel("C�dula: ");
		panel_2.add(label_3);
		
		txtCi = new JLabel(String.valueOf(usuario.getCi()));
		panel_2.add(txtCi);
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		
		JPanel panel_4 = new JPanel();
		add(panel_4);
		
		JLabel lblTelfono = new JLabel("Tel\u00E9fono 1");
		panel_4.add(lblTelfono);
		
		telefono1field = new JTextField((String) null);
		telefono1field.setColumns(10);
		panel_4.add(telefono1field);
		
		JButton btnGuardarCambios = new JButton("Guardar Cambios");
		btnGuardarCambios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				UsuDT usu = new UsuDT(txtNombre.getText(), txtPass.getText(), Integer.parseInt(txtCi.getText()),Integer.parseInt(telefono1field.getText()) );
				ViewController.getInstance().modificarUsuario(usu);	
			}
		});
		panel_3.add(btnGuardarCambios);	
		
	}

}
