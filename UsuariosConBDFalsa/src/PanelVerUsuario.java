import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JLabel;

import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.SystemColor;

public class PanelVerUsuario extends JPanel {
	private JLabel txtNombre;
	private JLabel txtPass;
	private JLabel txtCi;
	public PanelVerUsuario(UsuDT usuario) {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JLabel lblCrearUsuario = new JLabel("Ver Usuario");
		lblCrearUsuario.setFont(new Font("Tekton Pro Cond", Font.PLAIN, 50));
		lblCrearUsuario.setAlignmentX(0.5f);
		add(lblCrearUsuario);
		
		JPanel panel = new JPanel();
		add(panel);
		
		JLabel label_1 = new JLabel("Nombre: ");
		label_1.setBackground(SystemColor.activeCaption);
		label_1.setPreferredSize(new Dimension(75, 50));
		panel.add(label_1);
		
		txtNombre = new JLabel(usuario.getNombre());
		txtNombre.setPreferredSize(new Dimension(150, 50));
		panel.add(txtNombre);
		
		JPanel panel_1 = new JPanel();
		add(panel_1);
		
		JLabel label_2 = new JLabel("Clave: ");
		label_2.setBackground(SystemColor.activeCaption);
		label_2.setPreferredSize(new Dimension(75, 50));
		panel_1.add(label_2);
		
		txtPass = new JLabel(usuario.getClave());
		txtPass.setPreferredSize(new Dimension(150, 50));
		
		panel_1.add(txtPass);
		
		JPanel panel_2 = new JPanel();
		add(panel_2);
		
		JLabel label_3 = new JLabel("C�dula: ");
		label_3.setPreferredSize(new Dimension(75, 50));
		panel_2.add(label_3);
		
		txtCi = new JLabel(String.valueOf(usuario.getCi()));
		txtCi.setPreferredSize(new Dimension(150, 50));
		panel_2.add(txtCi);
		
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		
		
	}

}
