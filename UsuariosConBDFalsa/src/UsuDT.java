
public class UsuDT {
	private static int TELEFONO_INVALIDO = -1;
	private String nombre;
	private String clave;
	private int ci;
	private int telefono;
	
	public UsuDT(String nombre, String clave, int ci, int telefono) {
		super();
		this.nombre = nombre;
		this.clave = clave;
		this.ci = ci;
		this.telefono = telefono;
	}
	
	public UsuDT(String nombre, String clave, int ci) {
		this(nombre, clave, ci, TELEFONO_INVALIDO );
	}

	public String getNombre() {
		return nombre;
	}
	
	public int getTelefono() {
		return telefono;
	}

	public String getClave() {
		return clave;
	}

	public int getCi() {
		return ci;
	}

	@Override
	public boolean equals(Object usuObj) {
		UsuDT usu =  (UsuDT)usuObj;
		return usu.getNombre().equals(this.nombre)
				&& usu.getClave().equals(this.clave);
	}
	
	
}