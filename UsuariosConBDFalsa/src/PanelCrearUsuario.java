import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PanelCrearUsuario extends JPanel {
	private JTextField txtNombre;
	private JPasswordField txtPass;
	private JTextField txtCi;
	public PanelCrearUsuario() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JLabel lblCrearUsuario = new JLabel("Crear Usuario");
		lblCrearUsuario.setFont(new Font("Tekton Pro Cond", Font.PLAIN, 50));
		lblCrearUsuario.setAlignmentX(0.5f);
		add(lblCrearUsuario);
		
		JPanel panel = new JPanel();
		add(panel);
		
		JLabel label_1 = new JLabel("Nombre");
		panel.add(label_1);
		
		txtNombre = new JTextField();
		txtNombre.setColumns(10);
		panel.add(txtNombre);
		
		JPanel panel_1 = new JPanel();
		add(panel_1);
		
		JLabel label_2 = new JLabel("Passguord");
		panel_1.add(label_2);
		
		txtPass = new JPasswordField();
		txtPass.setColumns(10);
		panel_1.add(txtPass);
		
		JPanel panel_2 = new JPanel();
		add(panel_2);
		
		JLabel label_3 = new JLabel("C\u00E9dula");
		panel_2.add(label_3);
		
		txtCi = new JTextField();
		txtCi.setColumns(10);
		panel_2.add(txtCi);
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		
		JButton btnConfirmar = new JButton("Confirmar");
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				UsuDT usu = new UsuDT(txtNombre.getText(), txtPass.getText(), Integer.parseInt(txtCi.getText()));
				ViewController.getInstance().crearUsuario(usu);
			}
		});
		panel_3.add(btnConfirmar);
	}

}
