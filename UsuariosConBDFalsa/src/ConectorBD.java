import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConectorBD {
	
	private Connection _connection;

	public ConectorBD()
	{
		try
		{
			_connection = DriverManager.getConnection("jdbc:mysql://localhost/tinky_wynky?useSSL=false&serverTimezone=UTC&allowMultiQueries=true", "root", "");
			// allowMultiQueries=true: indica que se permiten m�ltiples consultas en un mismo "impacto" a la base de datos.
			// serverTimezone=UTC: Indica que se usar� el tiempo universal coordinado.
			// useSSL=false: Indica que no se usar� el protocolo de validaci�n SSL para mayor seguridad. Lo desactivamos porque usamos la conexi�n local y queremos evitar las advertencias que salen si no lo hacemos.
			if(_connection != null)
			{
				System.out.println("Conexi�n exitosa mismo.");
			}
			else
			{
				System.out.println("Conexi�n fallida.");
			}
		}
		catch (SQLException e) { 
			System.out.println("Conexi�n fallida con excepci�n capturada.");
			e.printStackTrace(); 
		}

	}
	
	public boolean validarUsu(UsuDT usu) 
	{
		try
		{
			Statement stmt = _connection.createStatement();
			
			
			String query = "SELECT * FROM usuarios WHERE nombre = '"+usu.getNombre()+"' AND pass = '"+usu.getClave()+"';";
			
			
			
			ResultSet rs = stmt.executeQuery(query);
			
			//return rs.next();
			if(rs.next())
			{
				return true;
			}
			else
			{
				return false;
			}
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return false;
		}
	}

	public boolean crearUsuario(UsuDT usu) {
		try
		{
			Statement stmt = _connection.createStatement();
			
			
			String query = "INSERT INTO usuarios (CI, nombre, pass) VALUES ("+usu.getCi()+", '" + usu.getNombre() + "', '" + usu.getClave() + "');";
			System.out.println("Ejecutando consulta: " + query);
			
			
			
			stmt.execute(query);
			
			return true;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return false;
		}
	}

	public UsuDT cargarUsuario(String ci) 
	{
		try
		{
			Statement stmt = _connection.createStatement();
			
			
			String query = "SELECT * FROM usuarios WHERE CI = "+ci+";";
			
			ResultSet rs = stmt.executeQuery(query);
			
			//return rs.next();
			if(rs.next())
			{
				String nombre = rs.getString("nombre");
				String clave = rs.getString("pass");
				int ciCargada = rs.getInt("CI");
				return new UsuDT(nombre,clave,ciCargada);
			}
			else
			{
				return null;
			}
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public UsuDT[] getAllUsuarios() {
		UsuDT [] usuarios;
		try
		{
			Statement stmt = _connection.createStatement();
			
			
			String query = "SELECT * FROM usuarios;";
			
			ResultSet rs = stmt.executeQuery(query);
			int index = 0;
			
			int size =0;
			if (rs != null) 
			{
			  rs.last();    // moves cursor to the last row
			  size = rs.getRow(); // get row id 
			}
			
			rs.beforeFirst();
			 usuarios = new UsuDT[size];
			 
			 System.out.println("SIZE::" + size);
			
			while(rs.next())
			{
				String nombre = rs.getString("nombre");
				String clave = rs.getString("pass");
				int ciCargada = rs.getInt("CI");
				usuarios[index] = new UsuDT(nombre,clave,ciCargada);
				index++;
			}
			System.out.println("INDEX:: " + index);
			return usuarios;
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean modificarUsuario(UsuDT usu) {
		try
		{
			_connection.setAutoCommit(false);
			Statement stmt = _connection.createStatement();
			
			String query = "UPDATE usuarios SET nombre = '"+usu.getNombre()+"', pass = '"+usu.getClave()+"' WHERE CI = '"+usu.getCi()+"'";
			System.out.println("UPDATE " + query);
			System.out.println("Ejecutando consulta: " + query);
			
			
			
			stmt.execute(query);
			
			// consulta tel�fono
			
			stmt = _connection.createStatement();
			
			String queryTelfono = "INSERT INTO telefonos (ci_usu, num) VALUES ("+usu.getCi()+", '" + usu.getTelefono() + "');";
			System.out.println("Ejecutando consulta: " + queryTelfono);
			
			stmt.execute(queryTelfono);
			_connection.commit();
			_connection.setAutoCommit(true);
			
			return true;
		}
		catch(SQLException e)
		{
			try {
				_connection.rollback();
				_connection.setAutoCommit(true);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			return false;
		}
	}

	public boolean agregarTel�fonoUsuario(TelefonoDT telefonoDT) {
		try
		{
			Statement stmt = _connection.createStatement();
			
			
			String query = "INSERT INTO telefonos (ci_usu, num) VALUES ("+telefonoDT.getCi()+", '" + telefonoDT.getTelefono() + "');";
			System.out.println("Ejecutando consulta: " + query);
			
			
			stmt.execute(query);
			
			return true;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public TelefonoDT[] getAllTelefonosFromUsuario(UsuDT usuario) {
		TelefonoDT [] telefonos;
		try
		{
			Statement stmt = _connection.createStatement();
			
			
			String query = "SELECT * FROM telefonos WHERE ci_usu = '" + usuario.getCi() + "'";
			
			ResultSet rs = stmt.executeQuery(query);
			int index = 0;
			
			int size =0;
			if (rs != null) 
			{
			  rs.last();    // moves cursor to the last row
			  size = rs.getRow(); // get row id 
			}
			
			rs.beforeFirst();
			telefonos = new TelefonoDT[size];
			 
			 System.out.println("SIZE::" + size);
			
			while(rs.next())
			{
				int telefono = rs.getInt("num");
				int ciCargada = rs.getInt("CI");
				telefonos[index] = new TelefonoDT(ciCargada,telefono);
				index++;
			}
			System.out.println("INDEX:: " + index);
			return telefonos;
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
}