import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JButton;

public class VentanaUnica extends JFrame 
{
	
	private static final String LOG_IN = "Login";
	private static final String INFO_USUARIO_LOGUEADO = "Info usu log";
	private JPanel contentPane;
	private JPanel pnlAuxiliar;
	private String _estadoActual;
	private JMenuItem logout;
	private PanelLogIn _pnlLogin;
	private JPanel pnlPrincipal;
	private PanelListarUsuarios panelListarUsuarios;
	
	/**
	 * Create the frame.
	 */
	public VentanaUnica() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		pnlPrincipal = new JPanel();
		contentPane.add(pnlPrincipal, BorderLayout.CENTER);
		
		JButton btnNewButton = new JButton("New button");
		pnlPrincipal.add(btnNewButton);
		
		pnlAuxiliar = new JPanel();
		contentPane.add(pnlAuxiliar, BorderLayout.EAST);
		pnlAuxiliar.setLayout(new CardLayout(0, 0));
		
		_pnlLogin = new PanelLogIn();
		pnlAuxiliar.add(_pnlLogin, LOG_IN);
		_pnlLogin.onAgregadoAVista();
		
		PanelInfoUsuarioLogueado pnlInfoUsuario = new PanelInfoUsuarioLogueado();
		pnlAuxiliar.add(pnlInfoUsuario, INFO_USUARIO_LOGUEADO);
		
		
		_estadoActual = LOG_IN;
		((CardLayout)pnlAuxiliar.getLayout()).show(pnlAuxiliar, _estadoActual);
		
		
		setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
		
		JMenuBar menubar = new JMenuBar();
		JMenu menGestionarUsuarios = new JMenu("Gestionar usuarios");
		JMenu menNavegacion = new JMenu("Navegaci�n");
		logout = new JMenuItem("Logout");
		JMenuItem menItCrearUsuario = new JMenuItem("Crear");
		JMenuItem menItEditarUsuario = new JMenuItem("Editar");
		JMenuItem menItBorrarUsuario = new JMenuItem("Borrar");
		JMenuItem menItVerUsuario = new JMenuItem("Ver");
		JMenuItem menItListarUsuarios = new JMenuItem("Listar Usuarios");
		menubar.add(menGestionarUsuarios);
		menubar.add(menNavegacion);
		menubar.add(logout);
		menGestionarUsuarios.add(menItCrearUsuario);
		menGestionarUsuarios.add(menItEditarUsuario);
		menGestionarUsuarios.add(menItBorrarUsuario);
		menGestionarUsuarios.add(menItVerUsuario);
		menGestionarUsuarios.add(menItListarUsuarios);
		
		logout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, ActionEvent.CTRL_MASK));
		logout.setEnabled(false);
		menGestionarUsuarios.setMnemonic('G');
		menItCrearUsuario.setMnemonic('r');
		

		menItCrearUsuario.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().mostrarCrearUsuario();
				
			}
		});
		
		menItEditarUsuario.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String ci = JOptionPane.showInputDialog("Ingres� un n�mero");
				if(!ci.equals(null)) {
					ViewController.getInstance().verUsuarioPorCI(ci,true);
				}
				
			}
		});
		
		menItListarUsuarios.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().mostrarListarUsuarios();
				
			}
		});
		
		menItVerUsuario.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {	
					String ci = JOptionPane.showInputDialog("Ingres� un n�mero");
					if(!ci.equals(null)) {
						ViewController.getInstance().verUsuarioPorCI(ci,false);
					}
				
			}
		});
		
		logout.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().logout();
				
			}
		});
		
		
		
		setJMenuBar(menubar);
	}
	
	/**
	 * Se encarga de intercambiar la carta del panel auxiliar.
	 */
	public void mostrarLogin() {
		_estadoActual = LOG_IN;
		_pnlLogin.limpiarCampos();
		((CardLayout)pnlAuxiliar.getLayout()).show(pnlAuxiliar, _estadoActual);
	}
	
	public void mostrarInfoUsuarioLogueado() {
		_estadoActual = INFO_USUARIO_LOGUEADO;
		((CardLayout)pnlAuxiliar.getLayout()).show(pnlAuxiliar, _estadoActual);
		logout.setEnabled(true);
	}
	
	public void mostrarErrorLogin() {
		JOptionPane.showMessageDialog(this, "Error en el logueo");
	}
	
	public void mostrarErrorUsuarioInexistente() {
		JOptionPane.showMessageDialog(this, "Usuario inexistente");
	}
	
	public void generalMsg(String msg) {
		JOptionPane.showMessageDialog(this, msg);
	}
	
	public void mostrarCrearUsuario()
	{
		pnlPrincipal.removeAll();
		pnlPrincipal.add(new PanelCrearUsuario());
		pnlPrincipal.validate();
		pnlPrincipal.revalidate();
		pnlPrincipal.repaint();
	}
	
	public void mostrarVerUsuario(UsuDT usuario)
	{
		pnlPrincipal.removeAll();
//		PanelVerUsuario miPanel = new PanelVerUsuario(usuario);
		this.pnlPrincipal.add(new PanelVerUsuario(usuario));
//		miPanel.textfield_2.setText();
		pnlPrincipal.validate();
		pnlPrincipal.revalidate();
		pnlPrincipal.repaint();
	}

	public void mostrarListarUsuarios() {
		// TODO Auto-generated method stub
		pnlPrincipal.removeAll();
		panelListarUsuarios = new PanelListarUsuarios();
		pnlPrincipal.add(panelListarUsuarios);
		pnlPrincipal.validate();
		pnlPrincipal.revalidate();
		pnlPrincipal.repaint();
	}

	public void exhibirUsuarios(UsuDT[] usuarios) {
		panelListarUsuarios.mostrarInfoUsuarios(usuarios);
	}

	public void mostrarEditarUsuario(UsuDT usuario) {
		if(panelListarUsuarios != null) {
			pnlPrincipal.remove(panelListarUsuarios);
			panelListarUsuarios.validate();
			pnlPrincipal.validate();
		}
		pnlPrincipal.removeAll();
//		PanelVerUsuario miPanel = new PanelVerUsuario(usuario);
		this.pnlPrincipal.add(new PanelModificarUsuario(usuario));
//		miPanel.textfield_2.setText();
		pnlPrincipal.validate();
		pnlPrincipal.revalidate();
		pnlPrincipal.repaint();
	}
}