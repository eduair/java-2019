
public class TelefonoDT {
	private int telefono;
	private int ci;
	
	public TelefonoDT(int ci, int telefono) {
		super();
		this.telefono = telefono;
		this.ci = ci;
	}

	public int getCi() {
		return ci;
	}
	
	public int getTelefono() {
		return telefono;
	}

	@Override
	public boolean equals(Object usuObj) {
		TelefonoDT usu =  (TelefonoDT)usuObj;
		return usu.getCi() == this.ci
				&& usu.getTelefono() == this.telefono;
	}
	
	
}