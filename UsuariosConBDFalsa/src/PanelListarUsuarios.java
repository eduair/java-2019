import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PanelListarUsuarios extends JPanel {
	private UsuDT[] usuarios = new UsuDT[100];
	private JTable table;
	private DefaultTableModel dataModel;
    private String ci_selected;
    private String nombre_usuario_selected;

	public PanelListarUsuarios() {
		setLayout(new BorderLayout(0, 0));
		JLabel lblCrearUsuario = new JLabel("Listar Usuarios");
		lblCrearUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		lblCrearUsuario.setFont(new Font("Tekton Pro Cond", Font.PLAIN, 50));
		lblCrearUsuario.setAlignmentX(0.5f);
		add(lblCrearUsuario, BorderLayout.NORTH);
		table = new JTable();
		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane, BorderLayout.CENTER);
		dataModel = new DefaultTableModel() {
			
		};
		
		String[] headers = {"CI", "Nombre", "Clave"};
		dataModel.setColumnIdentifiers(headers);
		table.setModel(dataModel);
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.SOUTH);
		JButton btnNewButton = new JButton("Agregar tel\u00E9fono");

		table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent event) {
	            // do some actions here, for example
	            // print first column value from selected row
				nombre_usuario_selected = table.getValueAt(table.getSelectedRow(), 1).toString();
	        	ci_selected = table.getValueAt(table.getSelectedRow(), 0).toString();
	        	btnNewButton.setText("Agregar tel�fono a " + nombre_usuario_selected);
	        }
	    });
		
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String telefono = JOptionPane.showInputDialog("Telefono a agregar a " + nombre_usuario_selected);
				if(!telefono.equals(null)) {
					ViewController.getInstance().agregarTelefonoUsuario(ci_selected, telefono);
				}
			}
		});
		panel.add(btnNewButton);
	}
	
	public void mostrarInfoUsuarios(UsuDT[] usuarios)
	{
		for (int i = 0; i < usuarios.length; i++) {
			UsuDT usuario = usuarios[i];
			String[] personaInfo;
			personaInfo = new String[3];
			personaInfo[0] = String.valueOf(usuario.getCi());
			personaInfo[1] = usuario.getNombre();
			personaInfo[2] = usuario.getClave();
			dataModel.addRow(personaInfo);
		}
	}
}
