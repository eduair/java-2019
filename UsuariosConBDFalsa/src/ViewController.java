import javax.swing.JOptionPane;

public class ViewController {
	private static ViewController _instance;
	private VentanaUnica _ventanaUnica;
	private ConectorBD _bd;
	
	public static ViewController getInstance()
	{
		if(_instance == null)
		{
			_instance = new ViewController();
		}	
		return _instance;
	}
	
	private ViewController()
	{
		System.out.println("Creando instancia de ViewController");
	}
	
	public boolean crearUsuario(UsuDT usu)
	{
		return _bd.crearUsuario(usu);
	}
	
	public void inicializarVista()
	{
		System.out.println("Inicializando vista");
		
		_bd = new ConectorBD();
		
		_ventanaUnica = new VentanaUnica();
		_ventanaUnica.setVisible(true);
	}

	public void validarUsuario(UsuDT usu) {
		
		
		if(_bd.validarUsu(usu))
		{
			_ventanaUnica.mostrarInfoUsuarioLogueado();
		}
		else
		{
			_ventanaUnica.mostrarErrorLogin();
		}
	}

	public void logout() {
		_ventanaUnica.mostrarLogin();
	}

	public void mostrarCrearUsuario() {
		_ventanaUnica.mostrarCrearUsuario();
		
	}

	public void verUsuarioPorCI(String ci, boolean editar) 
	{
		System.out.println("verUsuarioPorCI " + ci);
		UsuDT usuCargado = _bd.cargarUsuario(ci);
		if(usuCargado == null) {
			_ventanaUnica.mostrarErrorUsuarioInexistente();
		} else {
			System.out.println("usuCargado " + usuCargado.getNombre());
			if(!editar) {
				_ventanaUnica.mostrarVerUsuario(usuCargado);
			} else {
				_ventanaUnica.mostrarEditarUsuario(usuCargado);
			}
		}
	}

	public void mostrarListarUsuarios() {
		_ventanaUnica.mostrarListarUsuarios();
		_ventanaUnica.exhibirUsuarios(_bd.getAllUsuarios());
		
	}

	public void mostrarEditarUsuario() {
		// TODO Auto-generated method stub
		
	}

	public boolean modificarUsuario(UsuDT usu) {
		boolean result = _bd.modificarUsuario(usu);
		if(result) {
			_ventanaUnica.generalMsg("Usuario editado con �xito");
		} else {
			_ventanaUnica.generalMsg("Problema al editar usuario");
		}
		return result;
	}

	public boolean agregarTelefonoUsuario(String ci_string, String telefono_string) {
		boolean result = false;
		try {
			int ci = Integer.parseInt(ci_string);
			int telefono = Integer.parseInt(telefono_string);
			TelefonoDT telfonoDT = new TelefonoDT(ci,telefono);
			result = _bd.agregarTel�fonoUsuario(telfonoDT);
			if (result) {
				_ventanaUnica.generalMsg("Telef�no agregado exitosamente");
			} else {
				_ventanaUnica.generalMsg("Error al agregar tel�fono");
			}
		} catch(Exception e) {
			_ventanaUnica.generalMsg("Error al agregar tel�fono");
		}
		return result;
		
	}
}