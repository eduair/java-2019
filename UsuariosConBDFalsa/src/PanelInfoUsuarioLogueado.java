import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class PanelInfoUsuarioLogueado extends JPanel {
	private JTextField txtNombre;
	private JPasswordField txtPassword;
	private JTextField txtCedula;
	public PanelInfoUsuarioLogueado() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JLabel lblInfoUsuarioLogueado = new JLabel("Info Usuario Logueado");
		lblInfoUsuarioLogueado.setFont(new Font("Tekton Pro Cond", Font.PLAIN, 50));
		lblInfoUsuarioLogueado.setAlignmentX(0.5f);
		add(lblInfoUsuarioLogueado);
		
		JPanel panel = new JPanel();
		add(panel);
		
		JLabel label_1 = new JLabel("Nombre");
		panel.add(label_1);
		
		txtNombre = new JTextField();
		txtNombre.setColumns(10);
		panel.add(txtNombre);
		
		JPanel panel_1 = new JPanel();
		add(panel_1);
		
		JLabel label_2 = new JLabel("Passguord");
		panel_1.add(label_2);
		
		txtPassword = new JPasswordField();
		txtPassword.setColumns(10);
		panel_1.add(txtPassword);
		
		JPanel panel_2 = new JPanel();
		add(panel_2);
		
		JLabel lblCdula = new JLabel("C\u00E9dula");
		panel_2.add(lblCdula);
		
		txtCedula = new JTextField();
		txtCedula.setColumns(10);
		panel_2.add(txtCedula);
	}

}
