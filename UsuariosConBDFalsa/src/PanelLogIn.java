import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PanelLogIn extends JPanel {
	private JTextField txtNombre;
	private JPasswordField txtPassword;
	private JButton btnIngresar;
	
	public PanelLogIn() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setPreferredSize(new Dimension(600, 400));
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));
		JLabel lblLogIn = new JLabel("Log In");
		panel_3.add(lblLogIn);
		lblLogIn.setFont(new Font("Tekton Pro Cond", Font.PLAIN, 75));
		lblLogIn.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		JPanel panel = new JPanel();
		panel_3.add(panel);
		
		JLabel lblNewLabel = new JLabel("Nombre");
		panel.add(lblNewLabel);
		
		txtNombre = new JTextField();
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_3.add(panel_1);
		
		JLabel lblPassuord = new JLabel("Passguord");
		panel_1.add(lblPassuord);
		
		txtPassword = new JPasswordField();
		panel_1.add(txtPassword);
		txtPassword.setColumns(10);
		
		JPanel panel_2 = new JPanel();
		panel_3.add(panel_2);
		
		btnIngresar = new JButton("Ingresar");
		panel_2.add(btnIngresar);
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				ViewController.getInstance().validarUsuario(new UsuDT(txtNombre.getText(), txtPassword.getText(), 0));
			}
		});
	}

	public void onAgregadoAVista() {
		this.getRootPane().setDefaultButton(btnIngresar);
	}

	public void limpiarCampos() {
		txtNombre.setText("");
		txtPassword.setText("");
	}

}
