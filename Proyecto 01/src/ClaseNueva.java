
public class ClaseNueva {

	public static void main(String[] args) {
		// File - New - JAVA Project - ClaseNueva
		// Public static void main, syso + Ctrl Espacio
		// println crea un salto de linea
		// String - Clase empieza con may�scula.
		//System.out.println(12);
		//System.out.print("");
		//System.out.println(14);
		// [tipo de datos] [nombre variable]
		// Si quiero colocar una comilla en un string uso \ antes
		// 1) Defino el string
		String nombre = "\" Alex";
		String noclase = "OtroNombre <- Esta no es una clase";
		String apellido = "Apellido <- Este no es un apellido";
		// 2) Print del string
		//System.out.println(noclase);
		//System.out.println(apellido);
		//System.out.println(nombre);
		// Concatenar variables con signo +
		nombre = "Eduardo";
		apellido = "Airaudo";  
		//System.out.println(nombre + " " + apellido);
		//System.out.println(nombre + " " + 5);
		// Si se hace una suma m�s una concatenaci�n
		//System.out.println(5 + 5 + " " + nombre + " " + (5 + 5));
		// palabras reservadas para el lenguaje, no pueden ser usadas para otra cosa, estan a color
		// Las variables pueden tener caract�res especiales
		int a�osQueFaltanParaJubilarse = 13;
		int edad = 13;
		int edadJubilacion = edad + a�osQueFaltanParaJubilarse;
		nombre = "Cyro";
		System.out.println("Cuando se jubile " + nombre + " tendr� " + edadJubilacion + " a�os");
		// int tipo primivito de datos, tiop b�sico, string es una clase, por eso int se escribe en min�scula.
		// \n - Salto de linea
		
		// if condicional S� - Si ocurre tal cosa entonces hace tal otra cosa
		// las llaves encierran lo que se har�
		// Comparadores > mayor < menor >= mayor igual >= menor igual == igual != distinto
		// El punto y coma termina una instrucci�n
		if (a�osQueFaltanParaJubilarse < 5) {
			System.out.println("Est�s a punto de jubilarte");
		} else if(a�osQueFaltanParaJubilarse <= 10) {
			System.out.println("Te queda poco para jubilarte.");
		} else {
			System.out.println("Segu� laburando un poco m�s");
		}
		boolean esNi�o = edad < 12;
		boolean esAdultoMayor = edad > 100;
		if(esNi�o || esAdultoMayor) {
			System.out.println("Es ni�o o adulto mayor");
		}
		
		
		a�osQueFaltanParaJubilarse = 4;
		edad = 14;
		boolean noEsNi�o = edad > 10;
		boolean faltaJubilarse = a�osQueFaltanParaJubilarse < 5;
		boolean ambos = noEsNi�o && faltaJubilarse;
		
		if((noEsNi�o || faltaJubilarse) && !ambos) {
			System.out.println("Le faltan menos de 5 a�os para jubilarse o tieene m�s de 10 a�os pero no ambos");
		} else if (ambos) {
			System.out.println("Cumple ambas condiciones");
		} else {
			System.out.println("No cumple ninguna");
		}
		
	}

}
