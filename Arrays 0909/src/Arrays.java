import java.util.Random;

public class Arrays {

	public static void main(String[] args) {
		int [] num = new int[6];
		Random rand = new Random();
		for (int i = 0; i < num.length; i++) {
			num[i] = rand.nextInt(10) + 1;
		}
		//num[499] = 2;
		//mostrarArray(num);
		String arrayShow = mostrarArray(num);
		System.out.println(arrayShow);
		
		int mayor = buscarMayorArray(num,true);
		System.out.println("MAXIMO:: " +  mayor);
		
		// MENOR
		int menor = buscarMayorArray(num,false);
		System.out.println("M�NIMO:: " +  menor);
				
		// Calcular el promedio
		double promedio = promedioArray(num);
		System.out.println("PROMEDIO:: " +  promedio);
		
		//BUSCAR UN N�MERO
		int numeroAbuscar = 5;
		String buscarNumero = busquedaNumeroFunction(num,numeroAbuscar);
		System.out.println("RESULTADO BUSQUEDA:: " +  buscarNumero);
		
		// Contar la cantidad de pares
		int pares = contarCantidadPares(num);
		System.out.println("RESULTADO PARES:: " +  pares);
		
		// Ordenar de menor a mayor
		//int [] num2 = ordenarMenorAMayor(num);
		
		// ordenar letras abecedario array
		int lado = 10;
		char cuadrado[][] = crearCuadrado(lado);
		String resultado = mostrarArrayBidimensional(cuadrado);
		System.out.println(resultado);	
		
		char triangulo[][] = crearTriangulo(lado,0);
		resultado = mostrarArrayBidimensional(triangulo);
		System.out.println(resultado);
		
		resultado = mostrarSumaArrayBidimensional(triangulo,cuadrado);
		System.out.println(resultado);
	}
	
	private static String  mostrarArray(int[] num) {
		String arrayString = "";
		for (int i = 0; i < num.length; i++) {
			if (i == 0) {
				arrayString = "[" + num[i];
			} else if (i == (num.length - 1)) {
				arrayString = arrayString + "," + num[i] + "]";
			} else {
				arrayString = arrayString + "," + num[i];
			}
		}
		return arrayString;
	}
	
	private static int buscarMayorArray(int[] num, boolean mayor) {
		int numero = 0;
		for (int i = 0; i < num.length; i++) {
			if (i == 0) {
				numero = num[i];
			} else {
				if (mayor) {
					numero = Math.max(numero,num[i]);
				} else {
					numero = Math.min(numero,num[i]);
				}
			}
		}
		return numero;
	}
	
	private static double promedioArray(int[] num) {
		int promedio = 0;
		for (int i = 0; i < num.length; i++) {
			promedio = promedio + num[i];
		}
		return promedio/num.length;
	}
	
	private static int contarCantidadPares(int[] num) {
		int pares = 0;
		for (int i = 0; i < num.length; i++) {
			if (num[i]%2 == 0) {
				pares++;
			}
		}
		return pares;
	}
	
	private static String busquedaNumeroFunction(int[] num, int buscar) {
		int i = 0;
		//System.out.println(num[i]);
		boolean encontrado = false;
		boolean seguir = true;
		while (!encontrado && seguir) {
			encontrado = (num[i] == buscar);
			seguir = i < (num.length - 1);
			i++;
		}
		
		if (!seguir && !encontrado) {
			return "No se encontr� el n�mero buscado";
		} else {
			return "Se encontr� el n�mero en la posici�n " + (i - 1);
		}
	}
	
	private static char[][] crearCuadrado(int lado) {
		char matriz[][] = new char[lado][lado];
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				if (i == 0 || i == (matriz.length -1)) {
					matriz[i][j] = 'o';
				} else {
					if (j == 0 || j == (matriz[i].length -1)) {
						matriz[i][j] = 'o';
					} else {
						matriz[i][j] = '-';
					}
				}
			}
		}
		return matriz;
	}
	
	private static char[][] crearTriangulo(int lado,int traslacion) {
		char matriz[][] = new char[lado][lado];
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				if ((j == 0 + traslacion && i < matriz.length-traslacion)  || (i == matriz.length-1-traslacion && j > traslacion) || i+traslacion == j) {
					matriz[i][j] = 'o';
				} else {
					matriz[i][j] = '-';
				}
			}
		}
		return matriz;
	}
	
	private static String mostrarArrayBidimensional(char[][] matriz) {
		String resultado = "";
			for (int i = 0; i < matriz.length; i++) {
				for (int j = 0; j < matriz[i].length; j++) {
					resultado = resultado + "  " + matriz[i][j];
				}
				resultado = resultado + "\r";
				
			}
			return resultado;
	}
	
	private static String mostrarSumaArrayBidimensional(char[][] matriz,char[][] matriz2) {
		String resultado = "";
			for (int i = 0; i < matriz.length; i++) {
				for (int j = 0; j < matriz[i].length; j++) {
					if (matriz2[i][j] == 'o') {
						matriz[i][j] = 'o';
					}
					resultado = resultado + "  " + matriz[i][j];
				}
				resultado = resultado + "\r";
				
			}
			return resultado;
	}
}
