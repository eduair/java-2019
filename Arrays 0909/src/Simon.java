import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.Timer;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import java.util.TimerTask;
import java.util.concurrent.TimeUnit;


public class Simon extends JFrame {

	private JPanel contentPane;
	private static JButton yellow = new JButton("");
	private static JButton red = new JButton("");
	private static JButton green = new JButton("");
	private static JButton blue = new JButton("");
	private static JButton btnStart = new JButton("START");
	private static Random rand = new Random();
	private static int buttonToPress;
	private static int globalRepeat = 0;
	private static JLabel label = new JLabel("0/0");
	private static int [] recordButtons = new int[2000];
	private static Timer timer;
	private static int checkPosition;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Simon frame = new Simon();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Simon() {
		enableBtns(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		green.setBackground(Color.BLACK);
		green.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checkIfTrue(1);
			}
		});
		green.setBounds(143, 11, 115, 68);
		contentPane.add(green);
		red.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checkIfTrue(2);
			}
		});
		
		red.setBackground(Color.BLACK);
		red.setBounds(272, 95, 115, 77);
		contentPane.add(red);
		
		blue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checkIfTrue(3);
			}
		});
		blue.setBackground(Color.BLACK);
		blue.setBounds(11, 95, 115, 77);
		contentPane.add(blue);
		yellow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checkIfTrue(4);
			}
		});
		
		yellow.setBackground(Color.BLACK);
		yellow.setBounds(143, 183, 115, 68);
		contentPane.add(yellow);
		
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String text = btnStart.getText();
				if (text == "Reiniciar") {
					btnStart.setText("START");
				}
				label.setText(0 + "/" + globalRepeat);
				buttonToPress = getRandomInt(1,4);
				recordButtons[globalRepeat] = buttonToPress;
				for (int i = 0; i < recordButtons.length; i++) {
					if (recordButtons[i] != 0) {
						System.out.println(recordButtons[i] + " POSITION " + i);
					}
				}
				turnOnAndOff(recordButtons);
				globalRepeat++;
			}
		});
		btnStart.setBounds(143, 95, 115, 77);
		contentPane.add(btnStart);
		
		
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Tahoma", Font.PLAIN, 25));
		label.setBounds(291, 198, 116, 53);
		contentPane.add(label);
		changeOnHover(1,green);
		changeOnHover(2,red);
		changeOnHover(3,blue);
		changeOnHover(4,yellow);
	}
	
	private static void turnlight(int num) {
		if (num == 1) {
			green.setBackground(Color.GREEN.brighter());
		} else if (num == 2) {
			red.setBackground(Color.RED.brighter());
		} else if (num == 3) {
			blue.setBackground(Color.CYAN.brighter());
		} else if (num == 4) {
			yellow.setBackground(Color.YELLOW.brighter());
		}
	}
	
	private static void turnOfflight(int num) {
		if (num == 1) {
			green.setBackground(Color.BLACK);
		} else if (num == 2) {
			red.setBackground(Color.BLACK);
		} else if (num == 3) {
			blue.setBackground(Color.BLACK);
		} else if (num == 4) {
			yellow.setBackground(Color.BLACK);
		}
	}
	
	private int getRandomInt(int min,int max) {
		return rand.nextInt((max - min)+1) + min;
	};
	
	private static void enableBtns(boolean status) {
		red.setEnabled(status);
		green.setEnabled(status);
		yellow.setEnabled(status);
		blue.setEnabled(status);
		red.setEnabled(status);
	}
	
	private static boolean turnOnAndOff(int [] num) {
	    timer = new Timer();
	    enableBtns(false);
		btnStart.setEnabled(false);
		TimerTask task = new TimerTask() {
	        int tic = 1;
	        int i = 0;
	        int t = 0;
	        @Override
	        public void run()
	        {
            	tic = num[i];
            	System.out.println(i);
	            if (tic != 0) {
	            	if(t%2 == 0) {
		            	turnlight(tic);
		            	label.setText(i+1 + "/" + globalRepeat);
	            	} else {
	            		turnOfflight(tic);
	            		i++;
	            		int ticSiguiente = num[i];
	            		if (ticSiguiente == 0)
	    	            {
	    					enableBtns(true);
	    					label.setText(0 + "/" + globalRepeat);
	    					timer.cancel();
	    				}
	            	}
	            	t++;
				} 
	        }
	  };
	  timer.schedule(task, 500, 250);
	  return true;
	}
	
	private static void checkIfTrue(int position) {
		changeColorOnClick(position);
		label.setText(checkPosition + 1 + "/" + globalRepeat);
		int toCheck = recordButtons[checkPosition];
		int toCheckO = recordButtons[checkPosition+1];
		System.out.println("POSITION::" + position + " TO CHECK::" + toCheck);
		if ((position == toCheck) && (toCheckO == 0)) {
			System.out.println("Ganaste");
			checkPosition = 0;
			enableBtns(false);
			btnStart.setEnabled(true);
		} else if (position != toCheck) {
			label.setText("Perdiste");
			enableBtns(false);
			btnStart.setText("Reiniciar");
			recordButtons = new int[2000];
			globalRepeat = 0;
			checkPosition = 0;
			btnStart.setEnabled(true);
		} else {
			checkPosition++;
		}
	}
	
	private static void changeColorOnClick(int num) {
		Timer timerGlobal = new Timer();
		int tic = num;
		TimerTask task = new TimerTask() {
			int t = 0;
			@Override
	        public void run()
	        {
				if(t%2 == 0) {
		        	turnlight(tic);
		    	} else {
		    		turnOfflight(tic);
		    		timerGlobal.cancel();
		    	}
				t++;
	        }
		};
		timerGlobal.schedule(task, 10, 500);
	}
	
	private static void changeOnHover(int num, JButton button) {
		button.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	if(button.isEnabled()) {
			    	if (num == 1) {
						green.setBackground(Color.GREEN.darker());
					} else if (num == 2) {
						red.setBackground(Color.RED.darker());
					} else if (num == 3) {
						blue.setBackground(Color.CYAN.darker());
					} else if (num == 4) {
						yellow.setBackground(Color.YELLOW.darker());
					}
		    	}
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	button.setBackground(Color.BLACK);
		    }
		});
	}
}
