
public class VerificacionCÚdula {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int cedula = 42557966;
		boolean resultado = verificarCedula(cedula);
		if (resultado) {
			System.out.println(cedula + " es una cÚdula Uruguaya");
		} else {
			System.out.println(cedula + " NO es una cÚdula Uruguaya");
		}
	}
	
	private static boolean verificarCedula(int num) {
		int ultimoDigito = num%10;
		int digitoCedula;
		int base = 2987634;
		String baseToString = String.valueOf(base);
		String largoCedula = String.valueOf(num);
		int largodelaCedula = largoCedula.length();
		int empezar =  0;
		int digitoVerificador;
		int resultadoSuma = 0;
		for (int i = 0; i < (largodelaCedula - 1); i++) {
			char numCedula = largoCedula.charAt(i);
			digitoCedula =Character.getNumericValue(numCedula);
			if (i == 0) {
				empezar =  (baseToString.length() - largodelaCedula) + 1;
			}
			char multiplicar = baseToString.charAt(empezar);
			digitoVerificador = Character.getNumericValue(multiplicar);
			resultadoSuma = resultadoSuma + digitoVerificador * digitoCedula;
			empezar++;
		}
		int resultadoFinal;
		int numerMasCercaEnCero = resultadoSuma + 10 - resultadoSuma%10;
		resultadoFinal = numerMasCercaEnCero - resultadoSuma;
		if (resultadoFinal == ultimoDigito) {
			return true;
		} else {
			return false;
		}
	}
}
