
public class DTEstado 
{
	private int posX1;
	private int posY1;

	private int posX2;
	private int posY2;

	private int posX3;
	private int posY3;

	private int posX4;
	private int posY4;

	private int posX5;
	private int posY5;
	public DTEstado(int posX1, int posY1, int posX2, int posY2, int posX3, int posY3, int posX4, int posY4, int posX5,
			int posY5) {
		super();
		this.posX1 = posX1;
		this.posY1 = posY1;
		this.posX2 = posX2;
		this.posY2 = posY2;
		this.posX3 = posX3;
		this.posY3 = posY3;
		this.posX4 = posX4;
		this.posY4 = posY4;
		this.posX5 = posX5;
		this.posY5 = posY5;
	}
	public int getPosX1() {
		return posX1;
	}
	public int getPosY1() {
		return posY1;
	}
	public int getPosX2() {
		return posX2;
	}
	public int getPosY2() {
		return posY2;
	}
	public int getPosX3() {
		return posX3;
	}
	public int getPosY3() {
		return posY3;
	}
	public int getPosX4() {
		return posX4;
	}
	public int getPosY4() {
		return posY4;
	}
	public int getPosX5() {
		return posX5;
	}
	public int getPosY5() {
		return posY5;
	}
	
	
}
