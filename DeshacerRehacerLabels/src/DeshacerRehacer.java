import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Iterator;
import java.util.Stack;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

public class DeshacerRehacer extends JFrame 
{

	private JPanel contentPane;
	private Stack<DTEstado> pilaDeshacer;
	private Stack<DTEstado> pilaRehacer;
	private JButton btnRehacer;
	private JButton btnDeshacer;
	private JPanel panel;
	private JLabel lbl1;
	private JLabel lbl2;
	private JLabel lbl3;
	private JLabel lbl4;
	private JLabel lbl5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeshacerRehacer frame = new DeshacerRehacer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DeshacerRehacer() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 600, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new FlowLayout());
		setContentPane(contentPane);
		
		
		/**
		 * Creaci�n de las pilas para deshacer / rehacer.
		 */
		
		pilaDeshacer = new Stack<DTEstado>();
		pilaRehacer = new Stack<DTEstado>();
		
		
		
		/**
		 * Creaci�n de la interfaz gr�fica.
		 */
		DefaultListModel<String> modelo = new DefaultListModel<String>();
		

		DefaultListModel<String> modelo2 = new DefaultListModel<String>();
		
		JButton btnGuardarEstado = new JButton("Guardar estado");
		btnGuardarEstado.addActionListener(new ActionListener() 
		{
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				guardarEstadoActual();
			}
		});
		getContentPane().add(btnGuardarEstado);
		
		btnDeshacer = new JButton("Deshacer");
		btnDeshacer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				deshacer();
				
			}
		});
		getContentPane().add(btnDeshacer);
		btnRehacer = new JButton("Rehacer");
		
//		Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener(){
//
//            @Override
//            public void eventDispatched(AWTEvent event) {
//	            System.out.println("EVENT");
//	            if(event instanceof KeyEvent){
//	                KeyEvent kEvent = (KeyEvent) event;
//	                System.out.println(kEvent.getKeyCode());
//	            }
//            }
//
//        }, AWTEvent.KEY_EVENT_MASK);
		
		
		btnRehacer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				rehacer();
				
			}
		});
		
		
		contentPane.add(btnRehacer);
		
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(500, 500));
		panel.setLayout(null);
		contentPane.add(panel);
		
		
		MouseMotionListener escuchadorUnico = new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseDragged(MouseEvent arg0) {
				JLabel lblDisparador = (JLabel)arg0.getSource(); // Source es sobre qui�n fue disparado el evento.
				
				lblDisparador.setBounds(arg0.getXOnScreen() - panel.getX(), arg0.getYOnScreen() - panel.getY(), lblDisparador.getWidth(), lblDisparador.getHeight());
			}
		};
		
		
		lbl1 = new JLabel("1");
		lbl1.setBounds(0,0,256,256);
		lbl1.addMouseMotionListener(escuchadorUnico);
		lbl1.setIcon(new ImageIcon("img/HormigaAtomica.png"));
		
		lbl2 = new JLabel("2");
		lbl2.setBounds(20,0,10,10);
		lbl2.addMouseMotionListener(escuchadorUnico);
		
		lbl3 = new JLabel("3");
		lbl3.setBounds(30,0,10,10);
		lbl3.addMouseMotionListener(escuchadorUnico);
		
		lbl4 = new JLabel("4");
		lbl4.setBounds(40,0,10,10);
		lbl4.addMouseMotionListener(escuchadorUnico);
		
		lbl5 = new JLabel("5");
		lbl5.setBounds(50,50,10,10);
		lbl5.addMouseMotionListener(escuchadorUnico);
		
		panel.add(lbl1);
		panel.add(lbl2);
		panel.add(lbl3);
		panel.add(lbl4);
		panel.add(lbl5);
		
	}
	
	
	protected void rehacer() 
	{
		if(!pilaRehacer.isEmpty())
		{
			DTEstado estadoASerActual = pilaRehacer.pop();
			
			pilaDeshacer.push(obtenerEstadoActual());
			
			aplicarEstadoActual(estadoASerActual);
			
//			refrescarListas();
		}
		
	}

	
	
	
	
	
	
	protected void deshacer() 
	{
		if(!pilaDeshacer.isEmpty())
		{
			DTEstado estadoDeshecho = pilaDeshacer.pop();
			
			pilaRehacer.push(obtenerEstadoActual());
			
			aplicarEstadoActual(estadoDeshecho);
			
//			refrescarListas();
		}
	}

	private void aplicarEstadoActual(DTEstado estadoActual)
	{
		lbl1.setBounds(estadoActual.getPosX1(), estadoActual.getPosY1(), lbl1.getWidth(), lbl1.getHeight());
		lbl2.setBounds(estadoActual.getPosX2(), estadoActual.getPosY2(), lbl2.getWidth(), lbl2.getHeight());
		lbl3.setBounds(estadoActual.getPosX3(), estadoActual.getPosY3(), lbl3.getWidth(), lbl3.getHeight());
		lbl4.setBounds(estadoActual.getPosX4(), estadoActual.getPosY4(), lbl4.getWidth(), lbl4.getHeight());
		lbl5.setBounds(estadoActual.getPosX5(), estadoActual.getPosY5(), lbl5.getWidth(), lbl5.getHeight());
	}

	private DTEstado obtenerEstadoActual() 
	{
		return new DTEstado(
				lbl1.getX(), lbl1.getY(),
				lbl2.getX(), lbl2.getY(),
				lbl3.getX(), lbl3.getY(),
				lbl4.getX(), lbl4.getY(),
				lbl5.getX(), lbl5.getY()
				
				);
	}

	protected void guardarEstadoActual() 
	{
		DTEstado estadoActual = obtenerEstadoActual();
		
		pilaDeshacer.push(estadoActual);
		
		pilaRehacer.clear();
		
//		refrescarListas();
	}
//
//	private void refrescarListas()
//	{
//		/**
//		 * Refresco lista deshacer.
//		 */
//		Iterator<DTEstado> it = pilaDeshacer.iterator();
//		
//		DefaultListModel<String> modelo = (DefaultListModel<String>) lstDeshacer.getModel();
//		
//		modelo.clear();
//		
//		
//		while (it.hasNext()) 
//		{
//			DTEstado dtEstado = (DTEstado) it.next();
//			modelo.addElement(dtEstado.getFrase());
//		}
//		
//		
//		
//		/**
//		 * Refresco lista rehacer.
//		 */
//		it = pilaRehacer.iterator();
//		
//		modelo = (DefaultListModel<String>) lstRehacer.getModel();
//		
//		modelo.clear();
//		
//		
//		while (it.hasNext()) 
//		{
//			DTEstado dtEstado = (DTEstado) it.next();
//			modelo.addElement(dtEstado.getFrase());
//		}
//		
//	}

}
