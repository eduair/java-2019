
public class PalindromoClase {

	public static void main(String[] args) {
		String palabra = "peppep";
		boolean resultado = calcularPalindromo(palabra);
		System.out.println("ES PALIDNROME:: " + resultado);
		int factorialNumero = factorial(4);
		System.out.println("FACTORIAL:: " + factorialNumero);
		int factorialNumeroRec = factorialRecursive(4);
		System.out.println("FACTORIAL RECURSIVO:: " + factorialNumeroRec);

	}
	
	private static boolean calcularPalindromo(String palabra) {
		int lenght = (palabra.length())/2;
		int valorFinal = palabra.length() - 1;
		char letraPrimero;
		char letraUltimo;
		boolean cortar = false;
		int i = 0;
		while (!cortar && i < lenght) {
			letraPrimero = palabra.charAt(i);
			letraUltimo = palabra.charAt(valorFinal - i);
			/*System.out.println(letraPrimero +" "+ letraUltimo);*/
			if(letraPrimero != letraUltimo) {
				cortar = true;
			}
			i++;
		}
		
		return !cortar;
		
		/*
		for (int i = 0; i < lenght; i++) {
			letraPrimero = palabra.charAt(i);
			letraUltimo = palabra.charAt(valorFinal - i);
			if(letraPrimero != letraUltimo) {
				return false;
			}
			System.out.println(letraPrimero +" "+ letraUltimo);
		}
		return true;
		 */
	}
	
	private static int factorial(int i) {
		int resultado = 1;
		for (i = i; i > 1; i--) {
			resultado = resultado*i;
		}
		return resultado;
	}
	
	private static int factorialRecursive(int i) {
		if (i > 1) {
			return i*factorialRecursive(i-1);
		} else {
			return i;
		}
	}

}
