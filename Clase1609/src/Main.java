import java.util.Random;

public class Main {
	private static Random rand = new Random();
	public static void main(String[] args) {
		Persona[] personas = new Persona[100];
		for (int i = 0; i < personas.length; i++) {
			boolean esAlumno = rand.nextBoolean();
			personas[i] = new Persona(esAlumno, i,"Pepe");
			String edadPersona = personas[i].getInfo();
			System.out.println(edadPersona);
		}
		encontrarMayorPersona(personas);
		/*new Persona();
		Persona per = new Persona();
		per.cumplirAņo();
		int newEdad = per.getEdad();
		System.out.println(newEdad);
		per.setEdad(-4);
		per = new Persona();
		 newEdad = per.getEdad();
		System.out.println(newEdad);*/
	}
	
	private static void encontrarMayorPersona(Persona[] personas){
		Persona personaMayor = null;
		for (int i = 0; i < personas.length; i++) {
			Persona per = personas[i];
			if(personaMayor == null) {personaMayor = per;}
			else if(per.esMayor(personaMayor)) {
				personaMayor = per;
			}
		}
		String resultado = "persona mayor es: " + personaMayor.getNombre() + " con una edad de " + personaMayor.getEdad();
		System.out.println(resultado);
	}

}
