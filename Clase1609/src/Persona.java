import java.util.Random;

public class Persona {
	private int edad;
	private double notaPromedio;
	private String nombre;
	private String respuestaAlumnos;
	private int clase;

	public Persona(boolean esAlumno, int clase, String nombre) {
		this.clase = clase;
		this.nombre = nombre;
		if (esAlumno) {
			respuestaAlumnos = " es alumno";
		} else {
			respuestaAlumnos = " no es alumno";
		}
		int rangoSuperiorNum = 88;
		int rangoInferiorNum = 10;
		Random rand = new Random();
		
		edad = rand.nextInt((rangoSuperiorNum - rangoInferiorNum)+1) + rangoInferiorNum;
		//System.out.println(edad);
	}
	/* ALT  +  SHIFT + S 
	 ABRIR VENTANA PARA ESTABLECER GETTER Y SETTER AUTOMÁTICAMENTE*/

	public String getInfo() {
		String datos = this.nombre + " de " + this.edad + this.respuestaAlumnos + " de la clase " + this.clase ;
		return datos;
	}
	
	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		edad = Math.abs(edad);
		this.edad = edad;
	}
	
	public void cumplirAño() {
		this.edad++;
	}
	
	public boolean esMayor(Persona personaMayor) {
		return (this.edad > personaMayor.getEdad());
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
