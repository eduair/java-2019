-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 03-12-2019 a las 00:53:17
-- Versión del servidor: 5.7.19
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto_java`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campeonatos`
--

DROP TABLE IF EXISTS `campeonatos`;
CREATE TABLE IF NOT EXISTS `campeonatos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

DROP TABLE IF EXISTS `equipos`;
CREATE TABLE IF NOT EXISTS `equipos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adress` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creation_date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_equipo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `equipos`
--

INSERT INTO `equipos` (`id`, `name`, `adress`, `phone`, `creation_date`) VALUES
(2, 'Equipo Ejemplo', 'Dirección', '123213123213', '2019-11-08'),
(3, 'Equipo Ejemplo 2', 'Otra Dirección', '213123123', '2019-11-01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

DROP TABLE IF EXISTS `estados`;
CREATE TABLE IF NOT EXISTS `estados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `nombre`) VALUES
(1, 'ACTIVO'),
(2, 'BAJA SOLICITADA'),
(3, 'BAJA LÓGICA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_partidos`
--

DROP TABLE IF EXISTS `estado_partidos`;
CREATE TABLE IF NOT EXISTS `estado_partidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `estado_partidos`
--

INSERT INTO `estado_partidos` (`id`, `name`) VALUES
(1, 'EMPEZADO'),
(2, 'CULMINADO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `integrantes`
--

DROP TABLE IF EXISTS `integrantes`;
CREATE TABLE IF NOT EXISTS `integrantes` (
  `ci` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rol_integrante` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ci`),
  KEY `rol_integrante` (`rol_integrante`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `integrantes`
--

INSERT INTO `integrantes` (`ci`, `name`, `rol_integrante`, `status`) VALUES
(-123, '123', 2, 1),
(2, '123', 2, 2),
(23, '123', 2, 2),
(234, '123', 2, 1),
(1234, '123', 2, 1),
(2345, '123', 2, 3),
(12345, '123', 2, 1),
(23456, '123', 2, 1),
(123457, '123', 2, 3),
(1231231, 'Prueba', 1, 1),
(1234578, '123', 2, 1),
(2345678, '123', 2, 3),
(23456789, '123', 2, 1),
(123123123, 'Test', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `integrantes_equipo`
--

DROP TABLE IF EXISTS `integrantes_equipo`;
CREATE TABLE IF NOT EXISTS `integrantes_equipo` (
  `id_integrante` int(11) DEFAULT NULL,
  `id_equipo` int(11) DEFAULT NULL,
  UNIQUE KEY `id_integrante` (`id_integrante`),
  KEY `id_equipo` (`id_equipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partidos`
--

DROP TABLE IF EXISTS `partidos`;
CREATE TABLE IF NOT EXISTS `partidos` (
  `id_campeonato` int(11) NOT NULL,
  `id_equipo_local` int(11) NOT NULL,
  `id_equipo_visitante` int(11) NOT NULL,
  `goals_local` int(11) NOT NULL,
  `goals_visitante` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  KEY `id_equipo_local` (`id_equipo_local`),
  KEY `id_equipo_visitante` (`id_equipo_visitante`),
  KEY `status` (`status`),
  KEY `id_campeonato` (`id_campeonato`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Administrador'),
(2, 'Gestor de Equipos'),
(3, 'Integrante');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_integrante`
--

DROP TABLE IF EXISTS `roles_integrante`;
CREATE TABLE IF NOT EXISTS `roles_integrante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles_integrante`
--

INSERT INTO `roles_integrante` (`id`, `name`) VALUES
(2, 'Director Técnico'),
(1, 'Jugador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pass` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rol` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  KEY `rol` (`rol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`email`, `pass`, `rol`, `status`) VALUES
('admin@mail.com', '123', 1, 1),
('gestor@mail.com', '123', 2, 1),
('fdasfdsaf@mail.com', 'fsdafds', 3, 1),
('test1@mail.com', 'fsdafds', 3, 1),
('test12@mail.com', 'fsdafds', 3, 1),
('test123@mail.com', 'fsdafds', 3, 1),
('test1234@mail.com', 'fsdafds', 3, 1),
('test2234@mail.com', 'fsdafds', 3, 1),
('test3234@mail.com', 'fsdafds', 3, 1),
('test5234@mail.com', 'fsdafds', 3, 0),
('test6234@mail.com', 'fsdafds', 2, 1),
('test7234@mail.com', 'fsdafds', 2, 1),
('test8234@mail.com', 'fsdafds', 2, 1),
('test9234@mail.com', 'fsdafds', 2, 1),
('test10234@mail.com', 'fsdafds', 2, 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `integrantes`
--
ALTER TABLE `integrantes`
  ADD CONSTRAINT `integrantes_ibfk_1` FOREIGN KEY (`rol_integrante`) REFERENCES `roles_integrante` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `integrantes_ibfk_2` FOREIGN KEY (`status`) REFERENCES `estados` (`id`);

--
-- Filtros para la tabla `integrantes_equipo`
--
ALTER TABLE `integrantes_equipo`
  ADD CONSTRAINT `integrantes_equipo_ibfk_2` FOREIGN KEY (`id_equipo`) REFERENCES `equipos` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `integrantes_equipo_ibfk_3` FOREIGN KEY (`id_integrante`) REFERENCES `integrantes` (`ci`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `partidos`
--
ALTER TABLE `partidos`
  ADD CONSTRAINT `partidos_ibfk_1` FOREIGN KEY (`id_equipo_local`) REFERENCES `equipos` (`id`),
  ADD CONSTRAINT `partidos_ibfk_2` FOREIGN KEY (`id_equipo_visitante`) REFERENCES `equipos` (`id`),
  ADD CONSTRAINT `partidos_ibfk_3` FOREIGN KEY (`status`) REFERENCES `estado_partidos` (`id`),
  ADD CONSTRAINT `partidos_ibfk_4` FOREIGN KEY (`id_campeonato`) REFERENCES `campeonatos` (`id`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`rol`) REFERENCES `roles` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
