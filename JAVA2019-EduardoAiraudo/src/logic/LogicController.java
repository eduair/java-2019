package logic;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import datatypes.EquipoDT;
import datatypes.IntegranteDT;
import datatypes.PartidoDT;
import datatypes.UserDT;
import enums.EnumCrearPartido;
import enums.EnumEstadosItem;
import interfaces.ILogicController;
import main.Factory;
import model.CRUDPartido;
import view.ViewController;

public class LogicController implements ILogicController {
	private static LogicController _instance;
	private UserDT _usuarioLogueado;
	
	public static LogicController getInstance()
	{
		if(_instance == null)
		{
			_instance = new LogicController();
		}	
		return _instance;
	}
	
	public void inicializar() {
		Factory.getViewCtrl().inicializarVista();
		Factory.getModelCtrl().conectarABaseDatos();
	}
	
	public boolean validateEmail(String emailStr) {
		Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
	}

	public EnumCrearPartido crearPartidoPedido(PartidoDT partido) {
		// TODO Auto-generated method stub
		if(partido.getGolesLocal() < 0 || partido.getGolesVisitante() < 0) {
			return EnumCrearPartido.GOLES_INVALIDO;
		} else {
			CRUDPartido crudPartido = new CRUDPartido();
			return crudPartido.crear(partido);
		}
	}
	
	public UserDT getUsuarioLogueado(UserDT usuario) {
		if(this._usuarioLogueado == null) {
			this._usuarioLogueado = Factory.getModelCtrl().getUser(usuario);
			return this._usuarioLogueado;
		} else {
			return this._usuarioLogueado;
		}
	}
	
	// en equipoo agregar jugadores, tabla  , checkbox, 
	
	public LocalDate convertToLocalDate(Date dateToConvert) {
	    return Instant.ofEpochMilli(dateToConvert.getTime())
	      .atZone(ZoneId.systemDefault())
	      .toLocalDate();
	}
	
	public String getStringFromLocalDate(LocalDate date) {
		Date dt = convertToDateViaInstant(date);
		
		java.text.SimpleDateFormat sdf = 
		     new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String currentTime = sdf.format(dt);
		return currentTime;
	}
	
	
	public Date convertToDateViaInstant(LocalDate dateToConvert) {
	    return (Date) java.util.Date.from(dateToConvert.atStartOfDay()
	      .atZone(ZoneId.systemDefault())
	      .toInstant());
	}
	
	public UserDT getUsuarioLogueado() {
		return this._usuarioLogueado;
	} 

	@Override
	public boolean login(UserDT usuario) {
		return Factory.getModelCtrl().validateUser(usuario);
	}

	@Override
	public void logout() {
		this._usuarioLogueado = null;
	}

	@Override
	public boolean editarUsuario(UserDT usuario) {
		return Factory.getModelCtrl().editarUsuario(usuario);
	}

	@Override
	public boolean crearUsuario(UserDT usuario) {
		return Factory.getModelCtrl().crearUsuario(usuario);
	}

	@Override
	public boolean usuarioBaja(UserDT usuario) {
		return Factory.getModelCtrl().usuarioBaja(usuario);
	}

	@Override
	public boolean usuarioDelete(UserDT usuario) {
		return Factory.getModelCtrl().usuarioDelete(usuario);
	}

	@Override
	public boolean restaurarUsuario(UserDT usuario) {
		return Factory.getModelCtrl().restaurarUsuario(usuario);
	}

	@Override
	public ArrayList<UserDT> getUsuarios(boolean status, int nropagina, int cantPerPagina) {
		return Factory.getModelCtrl().getUsuarios(status, nropagina, cantPerPagina);
	}

	@Override
	public int countUsers(boolean status) {
		return Factory.getModelCtrl().countUsers(status);
	}

	@Override
	public ArrayList<IntegranteDT> getIntegrantes(EnumEstadosItem status, int nropagina, int cantPerPagina, IntegranteDT filter) {
		return Factory.getModelCtrl().getIntegrantes(status, nropagina, cantPerPagina,filter);
	}

	@Override
	public boolean crearIntegrante(IntegranteDT integrante) {
		return Factory.getModelCtrl().crearIntegrante(integrante);
	}

	@Override
	public boolean integranteBaja(IntegranteDT integrante) {
		return Factory.getModelCtrl().integranteBaja(integrante);
	}

	@Override
	public boolean editarIntegrante(IntegranteDT integrante) {
		return Factory.getModelCtrl().editarIntegrante(integrante);
	}

	@Override
	public boolean restaurarIntegrante(IntegranteDT integrante) {
		return Factory.getModelCtrl().restaurarIntegrante(integrante);
	}

	@Override
	public boolean integranteDelete(IntegranteDT integrante) {
		return Factory.getModelCtrl().integranteDelete(integrante);
	}

	@Override
	public int countIntegrantes(EnumEstadosItem status, IntegranteDT filter) {
		return Factory.getModelCtrl().countIntegrantes(status,filter);
	}

	@Override
	public boolean createEquipo(EquipoDT equipo) {
		return Factory.getModelCtrl().createEquipo(equipo);
	}

	@Override
	public ArrayList<EquipoDT> getEquipos(EnumEstadosItem status, int nropagina, int cantPerPagina, Object filter) {
		return Factory.getModelCtrl().getEquipos(status, nropagina, cantPerPagina,filter);
	}

	@Override
	public boolean restaurarEquipo(EquipoDT equipo) {
		return Factory.getModelCtrl().restaurarEquipo(equipo);
	}

	@Override
	public boolean equipoBaja(EquipoDT equipo) {
		return Factory.getModelCtrl().equipoBaja(equipo);
	}

	@Override
	public boolean equipoDelete(EquipoDT equipo) {
		return Factory.getModelCtrl().equipoDelete(equipo);
	}

	@Override
	public int countEquipos(EnumEstadosItem status, EquipoDT filter) {
		return Factory.getModelCtrl().countEquipos(status,filter);
	}

	@Override
	public ArrayList<IntegranteDT> getIntegranteSinEquipo() {
		return Factory.getModelCtrl().getIntegranteSinEquipo();
	}

	@Override
	public ArrayList<IntegranteDT> getIntegranteEquipo(EquipoDT equipo) {
		return Factory.getModelCtrl().getIntegranteEquipo(equipo);
	}

	@Override
	public boolean agregarIntegranteEquipo(IntegranteDT integrante, EquipoDT equipo, boolean agregar) {
		return Factory.getModelCtrl().agregarIntegranteEquipo(integrante,equipo,agregar);
	}
}
