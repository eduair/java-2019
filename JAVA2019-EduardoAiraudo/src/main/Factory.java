package main;

import interfaces.ILogicController;
import interfaces.IModelController;
import interfaces.IViewController;
import logic.LogicController;
import model.ModelController;
import view.ViewController;

public class Factory {

	public static ILogicController getLogicCtrl() {
		return LogicController.getInstance();
	}
	
	public static IViewController getViewCtrl() {
		return ViewController.getInstance();
	}
	
	public static IModelController getModelCtrl() {
		return ModelController.getInstance();
	}
}
