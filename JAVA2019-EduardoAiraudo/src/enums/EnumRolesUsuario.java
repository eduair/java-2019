package enums;

public enum EnumRolesUsuario {
	ADMINISTRADOR("Administrador"), GESTOR_EQUIPOS("Gestor de Equipos"), INTEGRANTE("Integrante");
	private final String display;
	private EnumRolesUsuario(String s) {
        display = s;
    }
 
    @Override
    public String toString() {
        return display;
    }
}
