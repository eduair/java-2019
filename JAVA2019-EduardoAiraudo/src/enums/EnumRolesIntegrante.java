package enums;

public enum EnumRolesIntegrante {
	TODOS("Todos"),DIRECTOR_TECNICO("Director T�cnico"), JUGADOR("Jugador");
	private final String display;
	private EnumRolesIntegrante(String s) {
        display = s;
    }
 
    @Override
    public String toString() {
        return display;
    }
}
