package enums;

public enum EnumEstadosItem {
	ACTIVO("Activo"),BAJA_SOLICITADA("Baja Solicitada"), BAJA_LOGICA("Baja l�gica");
	private final String display;
	private EnumEstadosItem(String s) {
        display = s;
    }
 
    @Override
    public String toString() {
        return display;
    }
}
