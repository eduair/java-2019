package model.interfaces;

import java.util.ArrayList;

import datatypes.RolesDT;
import datatypes.UserDT;

public interface ICrudRoles {
	ArrayList<RolesDT> read();
}
