package model.interfaces;

import java.util.ArrayList;

import datatypes.UserDT;

public interface ICrudPartido {
	boolean create (UserDT user);
	boolean update (UserDT user);
	boolean delete (UserDT user);
	ArrayList<UserDT> read();
	UserDT readOne(UserDT user);
}
