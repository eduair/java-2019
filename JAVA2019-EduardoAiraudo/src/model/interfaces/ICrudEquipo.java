package model.interfaces;

import java.util.ArrayList;

import datatypes.EquipoDT;
import datatypes.IntegranteDT;
import enums.EnumEstadosItem;

public interface ICrudEquipo {
	boolean create (EquipoDT equipo);
	boolean update (EquipoDT equipo);
	boolean delete (EquipoDT equipo);
	ArrayList<EquipoDT> read(EnumEstadosItem status, int nropagina, int cantPerPagina, Object filter);
	EquipoDT readOne(EquipoDT equipo);
	boolean changeStatus(EquipoDT equipo, EnumEstadosItem activo);
	int countEquipos(EnumEstadosItem status, EquipoDT filter);
	ArrayList<IntegranteDT> getIntegrantes(EquipoDT equipo);
	boolean agregarIntegrante(IntegranteDT integrante, EquipoDT equipo, boolean agregar);
}
