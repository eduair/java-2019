package model.interfaces;

import java.util.ArrayList;

import datatypes.IntegranteDT;
import enums.EnumEstadosItem;

public interface ICrudIntegrantes {
	boolean create (IntegranteDT integrante);
	boolean update (IntegranteDT integrante);
	boolean delete (IntegranteDT integrante);
	ArrayList<IntegranteDT> read(EnumEstadosItem status, int nropagina, int cantPerPagina, IntegranteDT filter);
	IntegranteDT readOne(IntegranteDT integrante);
	boolean validateIntegrante(IntegranteDT integrante);
	ArrayList<IntegranteDT> read(boolean b, int nropagina, int cantPerPagina);
	boolean changeStatus(IntegranteDT integrante, EnumEstadosItem activo);
	int countIntegrantes(EnumEstadosItem status, IntegranteDT filter);
	ArrayList<IntegranteDT> getIntegranteSinEquipo();
}
