package model.interfaces;

import java.util.ArrayList;

import datatypes.UserDT;

public interface ICrudUsers {
	boolean create (UserDT user);
	boolean update (UserDT user);
	boolean delete (UserDT user);
	ArrayList<UserDT> read();
	UserDT readOne(UserDT user);
	boolean validateUser(UserDT user);
	ArrayList<UserDT> read(boolean b, int nropagina, int cantPerPagina);
	boolean changeStatus(UserDT user, boolean status);
	int countUsers(boolean status);
}
