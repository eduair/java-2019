package model;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import datatypes.EquipoDT;
import datatypes.IntegranteDT;
import datatypes.UserDT;
import enums.EnumEstadosItem;
import enums.EnumRolesIntegrante;
import interfaces.IModelController;
import model.interfaces.ICrudUsers;
import model.interfaces.ICrudEquipo;
import model.interfaces.ICrudIntegrantes;

public class ModelController implements IModelController {
	private ConectorBD _conexion;
	private static ModelController _instance;
	public static ModelController getInstance()
	{
		if(_instance == null)
		{
			_instance = new ModelController();
		}	
		return _instance;
	}
	
	public Map<Integer, EnumEstadosItem> getEstadoById() {
		Map<Integer, EnumEstadosItem> map = new HashMap<Integer, EnumEstadosItem>();
		map.put(1, EnumEstadosItem.ACTIVO);
		map.put(2, EnumEstadosItem.BAJA_SOLICITADA);
		map.put(3, EnumEstadosItem.BAJA_LOGICA);
		return map;
	}
	
	public Map<EnumEstadosItem, Integer> getEstadosByEnum() {
		Map<EnumEstadosItem, Integer> map = new HashMap<EnumEstadosItem, Integer>();
		map.put(EnumEstadosItem.ACTIVO,1);
		map.put( EnumEstadosItem.BAJA_SOLICITADA,2);
		map.put(EnumEstadosItem.BAJA_LOGICA,3);
		return map;
	}
	
	public Map<Integer, EnumRolesIntegrante> getRolesById() {
		Map<Integer, EnumRolesIntegrante> map = new HashMap<Integer, EnumRolesIntegrante>();
		map.put(2, EnumRolesIntegrante.DIRECTOR_TECNICO);
		map.put(1, EnumRolesIntegrante.JUGADOR);
		return map;
	}
	
	public Map<EnumRolesIntegrante, Integer> getRolesByEnum() {
		Map<EnumRolesIntegrante, Integer> map = new HashMap<EnumRolesIntegrante, Integer>();
		map.put(EnumRolesIntegrante.DIRECTOR_TECNICO,2);
		map.put(EnumRolesIntegrante.JUGADOR,1);
		return map;
	}
	
	public int getIntRole(EnumRolesIntegrante role) {
		Map<EnumRolesIntegrante, Integer> map_roles = this.getRolesByEnum();
		return map_roles.get(role);
	}
	
	public int getIntEstado(EnumEstadosItem estado) {
		Map<EnumEstadosItem, Integer> map_roles = this.getEstadosByEnum();
		return map_roles.get(estado);
	}
	
	@Override
	public void conectarABaseDatos() {
		_conexion = new ConectorBD();
	}

	@Override
	public boolean validateUser(UserDT usuario) {
		ICrudUsers crudUser = new CRUDUser(_conexion);
		return crudUser.validateUser(usuario);
	}

	@Override
	public boolean loginUser(UserDT usuario) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean editarUsuario(UserDT usuario) {
		ICrudUsers crudUser = new CRUDUser(_conexion);
		return crudUser.update(usuario);
	}

	@Override
	public boolean crearUsuario(UserDT usuario) {
		ICrudUsers crudUser = new CRUDUser(_conexion);
		return crudUser.create(usuario);
	}

	@Override
	public boolean usuarioBaja(UserDT usuario) {
		ICrudUsers crudUser = new CRUDUser(_conexion);
		return crudUser.changeStatus(usuario,false);
	}

	@Override
	public boolean usuarioDelete(UserDT usuario) {
		ICrudUsers crudUser = new CRUDUser(_conexion);
		return crudUser.delete(usuario);
	}

	@Override
	public boolean restaurarUsuario(UserDT usuario) {
		ICrudUsers crudUser = new CRUDUser(_conexion);
		return crudUser.changeStatus(usuario,true);
	}

	@Override
	public ArrayList<UserDT> getUsuarios(boolean status, int nropagina, int cantPerPagina) {
		ICrudUsers crudUser = new CRUDUser(_conexion);
		return crudUser.read(status,nropagina, cantPerPagina);
	}

	@Override
	public int countUsers(boolean status) {
		ICrudUsers crudUser = new CRUDUser(_conexion);
		return crudUser.countUsers(status);
	}

	@Override
	public ArrayList<IntegranteDT> getIntegrantes(EnumEstadosItem status, int nropagina, int cantPerPagina, IntegranteDT filter) {
		ICrudIntegrantes crudIntegrante = new CRUDIntegrante(_conexion);
		return crudIntegrante.read(status,nropagina, cantPerPagina,filter);
	}

	@Override
	public boolean crearIntegrante(IntegranteDT integrante) {
		ICrudIntegrantes crudIntegrante = new CRUDIntegrante(_conexion);
		return crudIntegrante.create(integrante);
	}

	@Override
	public boolean integranteBaja(IntegranteDT integrante) {
		ICrudIntegrantes crudIntegrante = new CRUDIntegrante(_conexion);
		return crudIntegrante.changeStatus(integrante,EnumEstadosItem.BAJA_LOGICA);
	}

	@Override
	public boolean editarIntegrante(IntegranteDT integrante) {
		ICrudIntegrantes crudIntegrante = new CRUDIntegrante(_conexion);
		return crudIntegrante.update(integrante);
	}

	@Override
	public boolean restaurarIntegrante(IntegranteDT integrante) {
		ICrudIntegrantes crudIntegrante = new CRUDIntegrante(_conexion);
		return crudIntegrante.changeStatus(integrante,EnumEstadosItem.ACTIVO);
	}

	@Override
	public boolean integranteDelete(IntegranteDT integrante) {
		ICrudIntegrantes crudIntegrante = new CRUDIntegrante(_conexion);
		return crudIntegrante.delete(integrante);
	}

	@Override
	public int countIntegrantes(EnumEstadosItem status, IntegranteDT filter) {
		ICrudIntegrantes crudIntegrante = new CRUDIntegrante(_conexion);
		return crudIntegrante.countIntegrantes(status,filter);
	}

	@Override
	public UserDT getUser(UserDT usuario) {
		ICrudUsers crudUser = new CRUDUser(_conexion);
		return crudUser.readOne(usuario);
	}

	@Override
	public boolean createEquipo(EquipoDT equipo) {
		ICrudEquipo crudEquipos = new CRUDEquipos(_conexion);
		return crudEquipos.create(equipo);
	}

	@Override
	public ArrayList<EquipoDT> getEquipos(EnumEstadosItem status, int nropagina, int cantPerPagina, Object filter) {
		ICrudEquipo crudEquipos = new CRUDEquipos(_conexion);
		return crudEquipos.read(status,nropagina, cantPerPagina,filter);
	}

	@Override
	public boolean restaurarEquipo(EquipoDT equipo) {
		ICrudEquipo crudEquipos = new CRUDEquipos(_conexion);
		return crudEquipos.changeStatus(equipo,EnumEstadosItem.ACTIVO);
	}

	@Override
	public boolean equipoBaja(EquipoDT equipo) {
		ICrudEquipo crudEquipos = new CRUDEquipos(_conexion);
		return crudEquipos.changeStatus(equipo,EnumEstadosItem.BAJA_LOGICA);
	}

	@Override
	public boolean equipoDelete(EquipoDT equipo) {
		ICrudEquipo crudEquipos = new CRUDEquipos(_conexion);
		return crudEquipos.delete(equipo);
	}

	@Override
	public int countEquipos(EnumEstadosItem status, EquipoDT filter) {
		ICrudEquipo crudEquipos = new CRUDEquipos(_conexion);
		return crudEquipos.countEquipos(status,filter);
	}

	@Override
	public ArrayList<IntegranteDT> getIntegranteSinEquipo() {
		ICrudIntegrantes crudIntegrante = new CRUDIntegrante(_conexion);
		return crudIntegrante.getIntegranteSinEquipo();
	}

	@Override
	public ArrayList<IntegranteDT> getIntegranteEquipo(EquipoDT equipo) {
		ICrudEquipo crudEquipos = new CRUDEquipos(_conexion);
		return crudEquipos.getIntegrantes(equipo);
	}

	@Override
	public boolean agregarIntegranteEquipo(IntegranteDT integrante, EquipoDT equipo, boolean agregar) {
		ICrudEquipo crudEquipos = new CRUDEquipos(_conexion);
		return crudEquipos.agregarIntegrante(integrante,equipo,agregar);
	}
}
