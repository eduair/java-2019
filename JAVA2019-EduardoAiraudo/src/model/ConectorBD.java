package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import datatypes.UserDT;

public class ConectorBD {
	
	private Connection _connection;

	public ConectorBD()
	{
		try
		{
			String database = "proyecto_java";
			_connection = DriverManager.getConnection("jdbc:mysql://localhost/"+database+"?useSSL=false&serverTimezone=UTC&allowMultiQueries=true", "root", "");
			// allowMultiQueries=true: indica que se permiten m�ltiples consultas en un mismo "impacto" a la base de datos.
			// serverTimezone=UTC: Indica que se usar� el tiempo universal coordinado.
			// useSSL=false: Indica que no se usar� el protocolo de validaci�n SSL para mayor seguridad. Lo desactivamos porque usamos la conexi�n local y queremos evitar las advertencias que salen si no lo hacemos.
			if(_connection != null)
			{
				System.out.println("Conexi�n exitosa mismo.");
			}
			else
			{
				System.out.println("Conexi�n fallida.");
			}
		}
		catch (SQLException e) { 
			System.out.println("Conexi�n fallida con excepci�n capturada.");
			e.printStackTrace(); 
		}

	}
	
	/**
	 * @param queries : string array with SQL (INSERT, UPDATE) queries
	 * @return Success SQL Query = true else false
	 */
	public boolean multipleUpdateInsert(String [] queries) {
		try
		{
			_connection.setAutoCommit(false);
			
			for (int i = 0; i < queries.length; i++) {
				Statement stmt = _connection.createStatement();
				String query = queries[i];
				System.out.println(query);
				stmt.execute(query);
			}
			
			_connection.commit();
			_connection.setAutoCommit(true);
			
			return true;
		}
		catch(SQLException e)
		{
			try {
				_connection.rollback();
				_connection.setAutoCommit(true);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * @param query validate a select query (check if exists)
	 * @return True is SELECT is valid
	 */
	public boolean validationQuery(String query) {
		System.out.println("QUERY: " + query);
		try
		{
			Statement stmt = _connection.createStatement();
			
			ResultSet rs = stmt.executeQuery(query);
			
			//return rs.next();
			if(rs.next())
			{
				return true;
			}
			else
			{
				return false;
			}
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public int CountQuery(String table, String conditional) {
		String query = "SELECT COUNT(*) as cantidad FROM " + table + " " + conditional + ";";
		try
		{
			Statement stmt = _connection.createStatement();
						
			ResultSet rs = stmt.executeQuery(query);
			
			//return rs.next();
			if(rs.next())
			{
				return rs.getInt("cantidad");
			}
			else
			{
				return 0;
			}
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return 0;
		} 
	}
	
	/**
	 * @param query: SELECT query
	 * @return ResultSet of the query for the first found ResultSet (Only 1 entry)
	 */
	public ResultSet selectionQuery(String query) {
		try
		{
			Statement stmt = _connection.createStatement();
						
			ResultSet rs = stmt.executeQuery(query);
			
			//return rs.next();
			if(rs.next())
			{
				return rs;
			}
			else
			{
				return null;
			}
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public ResultSet multipleSelectionQuery(String query) {
		try
		{
			Statement stmt = _connection.createStatement();
						
			ResultSet rs = stmt.executeQuery(query);
			//return rs.next();
			return rs;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}

}