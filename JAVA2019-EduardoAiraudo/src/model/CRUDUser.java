package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import datatypes.UserDT;
import enums.EnumRolesUsuario;
import model.interfaces.ICrudUsers;

public class CRUDUser implements ICrudUsers {
	private ConectorBD _conexion;
	
	private Map<Integer, EnumRolesUsuario> getRolesById() {
		Map<Integer, EnumRolesUsuario> map = new HashMap<Integer, EnumRolesUsuario>();
		map.put(1, EnumRolesUsuario.ADMINISTRADOR);
		map.put(2, EnumRolesUsuario.GESTOR_EQUIPOS);
		map.put(3, EnumRolesUsuario.INTEGRANTE);
		return map;
	}
	
	private Map<EnumRolesUsuario,Integer> getRolesByEnum() {
		Map<EnumRolesUsuario, Integer> map = new HashMap<EnumRolesUsuario, Integer>();
		map.put(EnumRolesUsuario.ADMINISTRADOR, 1);
		map.put(EnumRolesUsuario.GESTOR_EQUIPOS, 2);
		map.put(EnumRolesUsuario.INTEGRANTE, 3);
		return map;
	}
	
	private int getIntRole(EnumRolesUsuario role) {
		Map<EnumRolesUsuario, Integer> map_roles = this.getRolesByEnum();
		return map_roles.get(role);
	}
	
	public CRUDUser(ConectorBD conexion) {
		this._conexion = conexion;
	}

	@Override
	public boolean create(UserDT user) {
		if(this.userExists(user)) {
			return false;
		} else {
			int rol = getIntRole(user.getRol());
			String[] queries = new String[1];
			queries[0] = "INSERT INTO usuarios (email, pass, rol, status) VALUES ('"+user.getEmail()+"', '"+user.getClave()+"', '"+rol+"', '1');";
			return _conexion.multipleUpdateInsert(queries);
		}
	}

	@Override
	public boolean update(UserDT user) {
		Map<EnumRolesUsuario, Integer> map_roles = this.getRolesByEnum();
		int rol = map_roles.get(user.getRol());
		String[] queries = new String[1];
		queries[0] = "UPDATE usuarios SET pass = '"+user.getClave()+"', rol = '"+rol+"' WHERE email = '"+user.getEmail()+"'";
		return _conexion.multipleUpdateInsert(queries);
	}

	@Override
	public boolean delete(UserDT user) {
		String[] queries = new String[1];
		queries[0] = "DELETE FROM usuarios where email = '"+user.getEmail()+"'";
		return _conexion.multipleUpdateInsert(queries);
	}
	
	private int getStatus(boolean status) {
		int statusValue;
		if (status) {
			statusValue = 1;
		} else {
			statusValue = 0;
		}
		return statusValue;
	}

	@Override
	public ArrayList<UserDT> read(boolean status, int nropagina, int cantiPerPagina) {
		int statusValue = getStatus(status);
		int limit = (nropagina-1)*cantiPerPagina;
		String query = "SELECT * FROM usuarios WHERE status = '" + statusValue + "' AND rol != '"+getIntRole(EnumRolesUsuario.ADMINISTRADOR)+"'";
		query+= " LIMIT " + limit + "," + cantiPerPagina;
		System.out.println(query);
		ResultSet rs = _conexion.multipleSelectionQuery(query);
		ArrayList<UserDT> userList = new ArrayList<UserDT>();
		try {
			while(rs.next())
			{
				String email = rs.getString("email");
				String clave = rs.getString("pass");
				int rol_int = rs.getInt("rol");
				Map<Integer, EnumRolesUsuario> map_roles = this.getRolesById();
				EnumRolesUsuario rol = map_roles.get(rol_int);
				UserDT user = new UserDT(email,clave,rol);
				userList.add(user);
			}
			return userList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private boolean userExists(UserDT user) {
		String query = "SELECT * FROM usuarios WHERE email = '"+user.getEmail()+"'";
		ResultSet rs = _conexion.selectionQuery(query);
		if (rs != null) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public UserDT readOne(UserDT user) {
		String query = "SELECT * FROM usuarios WHERE email = '"+user.getEmail()+"'";
		ResultSet rs = _conexion.selectionQuery(query);
		String email;
		try {
			email = rs.getString("email");
			String clave = rs.getString("pass");
			int rol_int = rs.getInt("rol");
			Map<Integer, EnumRolesUsuario> map_roles = this.getRolesById();
			EnumRolesUsuario rol = map_roles.get(rol_int);
			return new UserDT(email,clave,rol);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean validateUser(UserDT user) {
		String query = "SELECT * FROM usuarios WHERE email = '"+user.getEmail()+"' AND pass = '"+user.getClave()+"';";
		return _conexion.validationQuery(query);
	}

	@Override
	public ArrayList<UserDT> read() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean changeStatus(UserDT user, boolean status) {
		String[] queries = new String[1];
		int statusValue = getStatus(status);
		queries[0] = "UPDATE usuarios SET status = '"+statusValue+"' WHERE email = '"+user.getEmail()+"'";
		return _conexion.multipleUpdateInsert(queries);
	}

	@Override
	public int countUsers(boolean status) {
		String table = "usuarios";
		int statusValue = getStatus(status);
		String conditionales = " WHERE status = '" + statusValue +"' AND rol != '" + getIntRole(EnumRolesUsuario.ADMINISTRADOR) + "'";
		return _conexion.CountQuery(table, conditionales);
	}

}
