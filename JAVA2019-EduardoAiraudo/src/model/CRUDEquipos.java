package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map;

import datatypes.EquipoDT;
import datatypes.IntegranteDT;
import datatypes.UserDT;
import enums.EnumEstadosItem;
import enums.EnumRolesIntegrante;
import main.Factory;
import model.interfaces.ICrudEquipo;

public class CRUDEquipos implements ICrudEquipo {
	
	private ConectorBD _conexion;
	
	public CRUDEquipos(ConectorBD conexion) {
		this._conexion = conexion;
	}

	@Override
	public boolean create(EquipoDT equipo) {
		String date = Factory.getLogicCtrl().getStringFromLocalDate(equipo.get_creation_date());
		String[] queries = new String[1];
		queries[0] = "INSERT INTO equipos (name, adress, phone, creation_date) VALUES ('"+equipo.get_name()+"', '"+equipo.get_adress()+"', '"+equipo.get_phone()+"', '"+date+"');";
		return _conexion.multipleUpdateInsert(queries);
	}

	@Override
	public boolean update(EquipoDT equipo) {
		String[] queries = new String[1];
		queries[0] = "UPDATE equipos SET name = '"+equipo.get_name()+"', adress = '"+equipo.get_adress()+"', phone = '"+equipo.get_phone()+"' WHERE id = '"+equipo.get_id()+"'";
		return _conexion.multipleUpdateInsert(queries);
	}

	@Override
	public boolean delete(EquipoDT equipo) {
		String[] queries = new String[1];
		queries[0] = "DELETE FROM equipos where id = '"+equipo.get_id()+"'";
		return _conexion.multipleUpdateInsert(queries);
	}

	@Override
	public EquipoDT readOne(EquipoDT equipo) {
		String query = "SELECT * FROM equipos WHERE ci = '"+equipo.get_id()+"'";;
		ResultSet rs = _conexion.selectionQuery(query);
		try {
			int id = rs.getInt("id");
			String name = rs.getString("name");
			String adress = rs.getString("adress");
			String phone = rs.getString("phone");
			int statusEquipo = rs.getInt("status");
			Map<Integer, EnumEstadosItem> map_estados = Factory.getModelCtrl().getEstadoById();
			EnumEstadosItem estado = map_estados.get(statusEquipo);		
					
			LocalDate date = Factory.getLogicCtrl().convertToLocalDate(rs.getDate("creation_date"));
			
			return new EquipoDT(id,name,adress,phone,date,estado);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ArrayList<EquipoDT> read(EnumEstadosItem status, int nropagina, int cantiPerPagina, Object filtro) {
		int statusValue = Factory.getModelCtrl().getIntEstado(status);
		int limit = (nropagina-1)*cantiPerPagina;
		String query = "SELECT * FROM equipos WHERE ";
		if(status == EnumEstadosItem.ACTIVO) {
			query+= " (status = '" + statusValue +"' OR status = '" + Factory.getModelCtrl().getIntEstado(EnumEstadosItem.BAJA_SOLICITADA) + "')";
		} else {
			query+= " status = '" + statusValue +"'";
		}
		/*String filtros = generateFiltros(filtro);
		if(filtros != "") {
			query += " AND " + filtros;
		}*/
		query+= " LIMIT " + limit + "," + cantiPerPagina;
		System.out.println(query);
		ResultSet rs = _conexion.multipleSelectionQuery(query);
		ArrayList<EquipoDT> equipoList = new ArrayList<EquipoDT>();
		try {
			while(rs.next())
			{
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String adress = rs.getString("adress");
				String phone = rs.getString("phone");
				int statusEquipo = rs.getInt("status");
				Map<Integer, EnumEstadosItem> map_estados = Factory.getModelCtrl().getEstadoById();
				EnumEstadosItem estado = map_estados.get(statusEquipo);		
						
				LocalDate date = Factory.getLogicCtrl().convertToLocalDate(rs.getDate("creation_date"));
				
				EquipoDT equipo = new EquipoDT(id,name,adress,phone,date,estado);
				equipoList.add(equipo);
			}
			return equipoList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean changeStatus(EquipoDT equipo, EnumEstadosItem status) {
		String[] queries = new String[1];
		int statusValue = Factory.getModelCtrl().getIntEstado(status);
		queries[0] = "UPDATE equipos SET status = '"+statusValue+"' WHERE id = '"+equipo.get_id()+"'";
		return _conexion.multipleUpdateInsert(queries);
	}

	@Override
	public int countEquipos(EnumEstadosItem status, EquipoDT filtro) {
		String table = "equipos";
		int statusValue = Factory.getModelCtrl().getIntEstado(status);
		String conditionales = " WHERE";
		if(status == EnumEstadosItem.ACTIVO) {
			conditionales+= " (status = '" + statusValue +"' OR status = '" + Factory.getModelCtrl().getIntEstado(EnumEstadosItem.BAJA_SOLICITADA) + "')";
		} else {
			conditionales+= " status = '" + statusValue +"'";
		}
		/*String filtros = generateFiltros(filtro);
		if(filtros != "") {
			conditionales += " AND " + generateFiltros(filtro) + ";";
		}*/
		return _conexion.CountQuery(table, conditionales);
	}

	@Override
	public ArrayList<IntegranteDT> getIntegrantes(EquipoDT equipo) {
		String query = "SELECT ci,name,rol_integrante,status FROM `integrantes` RIGHT JOIN integrantes_equipo ON integrantes.ci = integrantes_equipo.id_integrante WHERE id_equipo = '"+equipo.get_id()+"' and integrantes.status != 3 and integrantes_equipo.actual = 1";
		ResultSet rs = _conexion.multipleSelectionQuery(query);
		ArrayList<IntegranteDT> integranteList = new ArrayList<IntegranteDT>();
		try {
			while(rs.next())
			{
				int cedula = rs.getInt("ci");
				String name = rs.getString("name");
				int rol_int = rs.getInt("rol_integrante");
				Map<Integer, EnumRolesIntegrante> map_roles = Factory.getModelCtrl().getRolesById();
				EnumRolesIntegrante rol = map_roles.get(rol_int);
				
				int statusIntegrante = rs.getInt("status");
				Map<Integer, EnumEstadosItem> map_estados = Factory.getModelCtrl().getEstadoById();
				EnumEstadosItem estado = map_estados.get(statusIntegrante);
				
				IntegranteDT integrante = new IntegranteDT(cedula,name,rol,estado);
				integranteList.add(integrante);
			}
			return integranteList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean agregarIntegrante(IntegranteDT integrante, EquipoDT equipo, boolean agregar) {
		String[] queries = null;
		String leaveEquipos = "UPDATE integrantes_equipo SET actual = 0 where id_integrante = '"+integrante.get_cedula()+"'";
		if(agregar) {
			queries = new String[2];
			queries[0] = leaveEquipos;
			queries[1] = "INSERT INTO integrantes_equipo (id_integrante, id_equipo) VALUES ('"+integrante.get_cedula()+"', '"+equipo.get_id()+"');";
		} else {
			queries = new String[1];
			queries[0] = leaveEquipos;
		}
		return _conexion.multipleUpdateInsert(queries);
	}


}
