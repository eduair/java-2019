package model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import datatypes.EquipoDT;
import datatypes.IntegranteDT;
import enums.EnumEstadosItem;
import enums.EnumRolesIntegrante;
import enums.EnumRolesUsuario;
import main.Factory;
import model.interfaces.ICrudIntegrantes;

public class CRUDIntegrante implements ICrudIntegrantes {
	private ConectorBD _conexion;
	
	public CRUDIntegrante(ConectorBD conexion) {
		this._conexion = conexion;
	}

	@Override
	public boolean create(IntegranteDT integrante) {
		if(this.integranteExists(integrante)) {
			return false;
		} else {
			int rol = Factory.getModelCtrl().getIntRole(integrante.get_rol());
			String[] queries = new String[1];
			queries[0] = "INSERT INTO integrantes (ci, name, rol_integrante, status) VALUES ('"+integrante.get_cedula()+"', '"+integrante.get_name()+"', '"+rol+"', '1');";
			return _conexion.multipleUpdateInsert(queries);
		}
	}

	@Override
	public boolean update(IntegranteDT integrante) {
		Map<EnumRolesIntegrante, Integer> map_roles = Factory.getModelCtrl().getRolesByEnum();
		int rol = map_roles.get(integrante.get_rol());
		String[] queries = new String[1];
		queries[0] = "UPDATE integrantes SET name = '"+integrante.get_name()+"', rol_integrante = '"+rol+"' WHERE ci = '"+integrante.get_cedula()+"'";
		return _conexion.multipleUpdateInsert(queries);
	}

	@Override
	public boolean delete(IntegranteDT integrante) {
		String[] queries = new String[2];
		queries[0] = "DELETE from integrantes_equipo where id_integrante = '"+integrante.get_cedula()+"'";
		queries[1] = "DELETE FROM integrantes where ci = '"+integrante.get_cedula()+"'";
		return _conexion.multipleUpdateInsert(queries);
	}
	
	private int getStatus(boolean status) {
		int statusValue;
		if (status) {
			statusValue = 1;
		} else {
			statusValue = 0;
		}
		return statusValue;
	}

	@Override
	public ArrayList<IntegranteDT> read(EnumEstadosItem status, int nropagina, int cantiPerPagina, IntegranteDT filtro) {
		int statusValue = Factory.getModelCtrl().getIntEstado(status);
		int limit = (nropagina-1)*cantiPerPagina;
		String query = "SELECT * FROM integrantes WHERE ";
		if(status == EnumEstadosItem.ACTIVO) {
			query+= " (status = '" + statusValue +"' OR status = '" + Factory.getModelCtrl().getIntEstado(EnumEstadosItem.BAJA_SOLICITADA) + "')";
		} else {
			query+= " status = '" + statusValue +"'";
		}
		String filtros = generateFiltros(filtro);
		if(filtros != "") {
			query += " AND " + filtros;
		}
		query+= " LIMIT " + limit + "," + cantiPerPagina;
		System.out.println(query);
		ResultSet rs = _conexion.multipleSelectionQuery(query);
		ArrayList<IntegranteDT> integranteList = new ArrayList<IntegranteDT>();
		try {
			while(rs.next())
			{
				int cedula = rs.getInt("ci");
				String name = rs.getString("name");
				int rol_int = rs.getInt("rol_integrante");
				Map<Integer, EnumRolesIntegrante> map_roles = Factory.getModelCtrl().getRolesById();
				EnumRolesIntegrante rol = map_roles.get(rol_int);
				
				int statusIntegrante = rs.getInt("status");
				Map<Integer, EnumEstadosItem> map_estados = Factory.getModelCtrl().getEstadoById();
				EnumEstadosItem estado = map_estados.get(statusIntegrante);
				
				IntegranteDT integrante = new IntegranteDT(cedula,name,rol,estado);
				integranteList.add(integrante);
			}
			return integranteList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private String generateFiltros(IntegranteDT filtro) {
		String query = "";
		if(filtro != null) {
			String name = filtro.get_name();
			int cedula = filtro.get_cedula();
			String cedula_str = "";
			EnumRolesIntegrante rol = filtro.get_rol();
			int role = 0;
			if(rol != EnumRolesIntegrante.TODOS) { 
				role = Factory.getModelCtrl().getIntRole(rol);
			}
			if(cedula != -1) {
				cedula_str = Integer.toString(cedula);
			}
			query = "ci LIKE '%"+cedula_str+"%' AND name LIKE '%"+name+"%'";
			if(role != 0) {
				query += " AND rol_integrante = '"+role+"'";		
			}
		}
		return query;
	}

	private boolean integranteExists(IntegranteDT integrante) {
		String query = "SELECT * FROM integrantes WHERE ci = '"+integrante.get_cedula()+"'";
		ResultSet rs = _conexion.selectionQuery(query);
		if (rs != null) {
			return true;
		} else {
			return false;
		}
	}
	
	private EquipoDT getEquipo(ResultSet rs) {
		EquipoDT equipo = null;
		try {
			int id = rs.getInt("id_equipo");
			String name = rs.getString ("name_equipo");
			String address = rs.getString ("address_equipo");
			String phone = rs.getString ("phone_equipo");
			Date date = rs.getDate("date_equipo");
			LocalDate creation_date = Factory.getLogicCtrl().convertToLocalDate(date);
			equipo = new EquipoDT(id,name,address,phone,creation_date, EnumEstadosItem.ACTIVO);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return equipo;
	}
	
	@Override
	public IntegranteDT readOne(IntegranteDT integrante) {
		String query = "SELECT * FROM integrantes WHERE ci = '"+integrante.get_cedula()+"'";;
		ResultSet rs = _conexion.selectionQuery(query);
		String email;
		try {
			int cedula = rs.getInt("ci");
			String name = rs.getString("name");
			int rol_int = rs.getInt("rol_integrante");
			
			int statusIntegrante = rs.getInt("status");
			Map<Integer, EnumEstadosItem> map_estados = Factory.getModelCtrl().getEstadoById();
			EnumEstadosItem estado = map_estados.get(statusIntegrante);
			
			
			Map<Integer, EnumRolesIntegrante> map_roles = Factory.getModelCtrl().getRolesById();
			EnumRolesIntegrante rol = map_roles.get(rol_int);
			EquipoDT equipo = getEquipo(rs);

			return new IntegranteDT(cedula,name,rol,estado);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean validateIntegrante(IntegranteDT integrante) {
		String query = "SELECT * FROM integrantes WHERE ci = '"+integrante.get_cedula()+"' AND pass = '"+integrante.get_name()+"';";
		return _conexion.validationQuery(query);
	}

	@Override
	public boolean changeStatus(IntegranteDT integrante, EnumEstadosItem status) {
		String[] queries = new String[1];
		int statusValue = Factory.getModelCtrl().getIntEstado(status);
		queries[0] = "UPDATE integrantes SET status = '"+statusValue+"' WHERE ci = '"+integrante.get_cedula()+"'";
		return _conexion.multipleUpdateInsert(queries);
	}

	@Override
	public int countIntegrantes(EnumEstadosItem status, IntegranteDT filtro) {
		String table = "integrantes";
		int statusValue = Factory.getModelCtrl().getIntEstado(status);
		String conditionales = " WHERE";
		if(status == EnumEstadosItem.ACTIVO) {
			conditionales+= " (status = '" + statusValue +"' OR status = '" + Factory.getModelCtrl().getIntEstado(EnumEstadosItem.BAJA_SOLICITADA) + "')";
		} else {
			conditionales+= " status = '" + statusValue +"'";
		}
		String filtros = generateFiltros(filtro);
		if(filtros != "") {
			conditionales += " AND " + generateFiltros(filtro) + ";";
		}
		return _conexion.CountQuery(table, conditionales);
	}

	@Override
	public ArrayList<IntegranteDT> read(boolean b, int nropagina, int cantPerPagina) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<IntegranteDT> getIntegranteSinEquipo() {
		String query = "SELECT * FROM integrantes LEFT JOIN integrantes_equipo ON integrantes.ci = integrantes_equipo.id_integrante" + 
				"WHERE NOT (integrantes_equipo.id_equipo != null AND integrantes_equipo.id_integrante IN (SELECT actual FROM integrantes_equipo where integrantes_equipo.actual = 1)) AND integrantes.status != 3";
		ResultSet rs = _conexion.multipleSelectionQuery(query);
		ArrayList<IntegranteDT> integranteList = new ArrayList<IntegranteDT>();
		try {
			while(rs.next())
			{
				int cedula = rs.getInt("ci");
				String name = rs.getString("name");
				int rol_int = rs.getInt("rol_integrante");
				Map<Integer, EnumRolesIntegrante> map_roles = Factory.getModelCtrl().getRolesById();
				EnumRolesIntegrante rol = map_roles.get(rol_int);
				
				int statusIntegrante = rs.getInt("status");
				Map<Integer, EnumEstadosItem> map_estados = Factory.getModelCtrl().getEstadoById();
				EnumEstadosItem estado = map_estados.get(statusIntegrante);
				
				IntegranteDT integrante = new IntegranteDT(cedula,name,rol,estado);
				integranteList.add(integrante);
			}
			return integranteList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
