package datatypes;

import enums.EnumEstadosItem;
import enums.EnumRolesIntegrante;

public class IntegranteDT {
	private String _name;
	private EnumRolesIntegrante _rol;
	private int _cedulaIdentidad;
	private EquipoDT _equipo;
	private EnumEstadosItem _status;
	public IntegranteDT(int cedula, String name, EnumRolesIntegrante rol, EquipoDT equipo, EnumEstadosItem status) {
		this._name = name;
		this._cedulaIdentidad = cedula;
		this._rol = rol;
		this._status = status;
		this._equipo = equipo;
	}
	
	public IntegranteDT(int cedula, String name, EnumRolesIntegrante rol, EnumEstadosItem status) {
		this(cedula, name,rol, null, status);
	}
	
	public EquipoDT get_equipo() {
		return _equipo;
	}
	
	public EnumEstadosItem get_status() {
		return _status;
	}
	
	public int get_cedula() {
		return _cedulaIdentidad;
	}

	
	public String get_name() {
		return _name;
	}

	public EnumRolesIntegrante get_rol() {
		return _rol;
	}
}
