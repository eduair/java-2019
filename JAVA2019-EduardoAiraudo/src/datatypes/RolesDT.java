package datatypes;

import enums.EnumRolesUsuario;

public class RolesDT {
	private int id;
	private EnumRolesUsuario name;
	
	public RolesDT(int id, EnumRolesUsuario name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}

	public EnumRolesUsuario getRole() {
		return name;
	}
	
	public String getName() {
		String nombre_rol = null;
		switch (name) {
		case ADMINISTRADOR:
			nombre_rol = "ADMINISTRADOR";
			break;
		case GESTOR_EQUIPOS:
			nombre_rol = "GESTOR_EQUIPOS";
			break;
		case INTEGRANTE:
			nombre_rol = "INTEGRANTE";
			break;
		default:
			break;
		}
		return nombre_rol;
	}
}
