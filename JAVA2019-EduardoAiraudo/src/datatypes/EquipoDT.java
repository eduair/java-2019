package datatypes;

import java.time.LocalDate;

import enums.EnumEstadosItem;

public class EquipoDT {
	
	private int _id;
	private String _name;
	private String _adress;
	private String _phone;
	private LocalDate _creation_date; 
	private EnumEstadosItem _status;
	
	/**
	 * @param id Id equipo
	 * @param name Nombre equipo
	 * @param adress Direcci�n, Ubicaci�n de Equipo
	 * @param phone Tel�fono
	 * @param creation_date Fecha de fundaci�n
	 */
	public EquipoDT(int id, String name, String adress, String phone,LocalDate creation_date, EnumEstadosItem status) {
		this._id = id;
		this._name = name;
		this._adress = adress;
		this._phone = phone;
		this._creation_date = creation_date;
		this._status = status;
	}
	
	public EquipoDT(String name, String adress, String phone,LocalDate creation_date) {
		this(0, name, adress, phone,creation_date,EnumEstadosItem.ACTIVO);
	}

	public int get_id() {
		return _id;
	}
	
	public EnumEstadosItem get_status() {
		return _status;
	}
	

	public String get_name() {
		return _name;
	}

	public String get_adress() {
		return _adress;
	}

	public String get_phone() {
		return _phone;
	}

	public LocalDate get_creation_date() {
		return _creation_date;
	}
}

//PARA DESPUES
/* SELECT ci,integrantes.name,equipos.name as equipo FROM integrantes LEFT JOIN integrantes_equipo ON integrantes_equipo.id_integrante = integrantes.ci
LEFT JOIN equipos ON integrantes_equipo.id_equipo = equipos.id */
