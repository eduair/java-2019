package datatypes;

import enums.EnumRolesUsuario;

public class UserDT {
	private String email;
	private String clave;
	private EnumRolesUsuario _rol;
	
	public UserDT(String email, String clave, EnumRolesUsuario rol) {
		super();
		this.email = email;
		this.clave = clave;
		this._rol = rol;
	}
	
	public UserDT(String email, String clave) {
		this(email,clave,null);
	}
	
	public String getEmail() {
		return email;
	}

	public String getClave() {
		return clave;
	}
	
	public EnumRolesUsuario getRol() {
		return _rol;
	}

	@Override
	public boolean equals(Object usuObj) {
		UserDT usu =  (UserDT)usuObj;
		return usu.getEmail().equals(this.email)
				&& usu.getClave().equals(this.clave);
	}
	
}
