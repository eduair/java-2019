package datatypes;

import enums.EnumEstadosPartido;

public class PartidoDT {
	private static int id;
	private String equipoLocal;
	private String equipoVisitante;
	private int golesLocal;
	private int golesVisitante;
	private EnumEstadosPartido estado;
	
	public PartidoDT(int id, String equipoLocal, String equipoVisitante, EnumEstadosPartido estado, int golesLocal, int golesVisitante) {
		super();
		this.id = id;
		this.equipoLocal = equipoLocal;
		this.equipoVisitante = equipoVisitante;
		this.estado = estado;
		this.golesLocal = golesLocal;
		this.golesVisitante = golesVisitante;
		
	}

	public static int getId() {
		return id;
	}

	public String getEquipoLocal() {
		return equipoLocal;
	}

	public String getEquipoVisitante() {
		return equipoVisitante;
	}

	public int getGolesLocal() {
		return golesLocal;
	}

	public int getGolesVisitante() {
		return golesVisitante;
	}

	public EnumEstadosPartido getEstado() {
		return estado;
	}
	
	
}
