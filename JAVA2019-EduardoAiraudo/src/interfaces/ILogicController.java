package interfaces;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

import datatypes.EquipoDT;
import datatypes.IntegranteDT;
import datatypes.PartidoDT;
import datatypes.UserDT;
import enums.EnumCrearPartido;
import enums.EnumEstadosItem;
import logic.LogicController;

public interface ILogicController {

	EnumCrearPartido crearPartidoPedido(PartidoDT partido);

	boolean validateEmail(String emailStr);

	boolean login(UserDT usuario);

	UserDT getUsuarioLogueado(UserDT usuario);
	
	public LocalDate convertToLocalDate(Date date);
	public String getStringFromLocalDate(LocalDate date);
	public Date convertToDateViaInstant(LocalDate dateToConvert);
	 

	void logout();

	UserDT getUsuarioLogueado();

	boolean editarUsuario(UserDT usuario);

	boolean crearUsuario(UserDT usuario);

	ArrayList<UserDT> getUsuarios(boolean status, int nropagina, int cantPerPagina);

	boolean usuarioBaja(UserDT usuario);

	boolean usuarioDelete(UserDT usuario);

	boolean restaurarUsuario(UserDT usuario);
	
	int countUsers(boolean status);

	ArrayList<IntegranteDT> getIntegrantes(EnumEstadosItem status, int nropagina, int cantPerPagina, IntegranteDT filter);

	boolean crearIntegrante(IntegranteDT integrante);

	boolean integranteBaja(IntegranteDT integrante);

	boolean editarIntegrante(IntegranteDT integrante);

	boolean integranteDelete(IntegranteDT integrante);

	boolean restaurarIntegrante(IntegranteDT integrantes);

	int countIntegrantes(EnumEstadosItem _status, IntegranteDT filter);

	boolean createEquipo(EquipoDT equipo);

	ArrayList<EquipoDT> getEquipos(EnumEstadosItem bajaLogica, int page, int cANT_PER_PAGINA, Object object);

	boolean restaurarEquipo(EquipoDT equipo);

	boolean equipoBaja(EquipoDT equipo);

	boolean equipoDelete(EquipoDT equipo);

	int countEquipos(EnumEstadosItem _status, EquipoDT filter);

	ArrayList<IntegranteDT> getIntegranteSinEquipo();

	ArrayList<IntegranteDT> getIntegranteEquipo(EquipoDT equipo);

	boolean agregarIntegranteEquipo(IntegranteDT integrante, EquipoDT equipo, boolean agregar);
	
}
