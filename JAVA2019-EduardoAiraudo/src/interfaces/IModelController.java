package interfaces;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map;

import datatypes.EquipoDT;
import datatypes.IntegranteDT;
import datatypes.UserDT;
import enums.EnumEstadosItem;
import enums.EnumRolesIntegrante;

public interface IModelController {

	void conectarABaseDatos();

	boolean validateUser(UserDT usuario);

	boolean loginUser(UserDT usuario);

	UserDT getUser(UserDT usuario);

	boolean editarUsuario(UserDT usuario);

	boolean crearUsuario(UserDT usuario);

	ArrayList<UserDT> getUsuarios(boolean status, int nropagina, int cantPerPagina);

	boolean usuarioBaja(UserDT usuario);

	boolean usuarioDelete(UserDT usuario);

	boolean restaurarUsuario(UserDT usuario);

	int countUsers(boolean status);

	ArrayList<IntegranteDT> getIntegrantes(EnumEstadosItem status, int nropagina, int cantPerPagina, IntegranteDT filter);

	boolean crearIntegrante(IntegranteDT integrante);

	boolean integranteBaja(IntegranteDT integrante);

	boolean editarIntegrante(IntegranteDT integrante);

	boolean restaurarIntegrante(IntegranteDT integrante);

	boolean integranteDelete(IntegranteDT integrante);

	int countIntegrantes(EnumEstadosItem status, IntegranteDT filter);

	boolean createEquipo(EquipoDT equipo);

	ArrayList<EquipoDT> getEquipos(EnumEstadosItem status, int nropagina, int cantPerPagina, Object filter);

	boolean restaurarEquipo(EquipoDT equipo);

	boolean equipoBaja(EquipoDT equipo);

	boolean equipoDelete(EquipoDT equipo);

	int countEquipos(EnumEstadosItem status, EquipoDT filter);

	int getIntEstado(EnumEstadosItem status);

	Map<Integer, EnumEstadosItem> getEstadoById();

	ArrayList<IntegranteDT> getIntegranteSinEquipo();

	ArrayList<IntegranteDT> getIntegranteEquipo(EquipoDT equipo);

	Map<Integer, EnumRolesIntegrante> getRolesById();

	Map<EnumRolesIntegrante, Integer> getRolesByEnum();

	int getIntRole(EnumRolesIntegrante rol);

	boolean agregarIntegranteEquipo(IntegranteDT integrante, EquipoDT equipo, boolean agregar);
	
}
