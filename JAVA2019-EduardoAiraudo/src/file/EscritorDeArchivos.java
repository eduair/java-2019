package file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import view.ViewController;

public class EscritorDeArchivos {
	private static EscritorDeArchivos _instance;
	
	public static EscritorDeArchivos getInstance()
	{
		if(_instance == null)
		{
			_instance = new EscritorDeArchivos();
		}	
		return _instance;
	}
	
	private EscritorDeArchivos()
	{
		System.out.println("Creando instancia de ViewController");
	}

	public void crearArchivo(String uri) {
		String path = uri;
		File archivo = new File(path);
		if(!archivo.exists()) {
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(archivo));
				bw.write("");
			} catch(IOException e) {
				e.printStackTrace();
				try {
					if(bw != null) {
						bw.close();	
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		} else {
			System.out.println("Ya existe");
		}
		
	}

	public void escribirEnArchivo(String uri, String contenido) {
		String path = uri;
		File archivo = new File(path);
		BufferedWriter bw = null;
		try {
			bw = 
					new BufferedWriter(new FileWriter(archivo));
			bw.write(contenido);
			bw.close();
		} catch(IOException e) {
			try {
				if(bw != null)  {
					bw.close();
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		
	}

}
