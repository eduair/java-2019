package file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LectorDeArchivos {
	private static LectorDeArchivos _instance;
	
	public static LectorDeArchivos getInstance()
	{
		if(_instance == null)
		{
			_instance = new LectorDeArchivos();
		}	
		return _instance;
	}
	
	private LectorDeArchivos()
	{
		System.out.println("Creando instancia de ViewController");
	}

	public String leerDeArchivo(String uri) {
		String datosLegibles = "";
		BufferedReader bufferedReader;
		String linea = "";
		int númeroDeLínea = 0;
		File archivo = new File(uri); 
		try  {
			bufferedReader = new BufferedReader( new FileReader(archivo));
			while(linea != null) {
				datosLegibles += "\n" + linea;
				númeroDeLínea++;
				linea = bufferedReader.readLine();
			}
			bufferedReader.close();
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
		return datosLegibles;
	}
	
	public boolean esArchivo(String uri) {
		File archivo = new File(uri);
		return archivo.isFile();
	}
	
	public boolean esDirectorio(String uri) {
		File archivo = new File(uri);
		return archivo.isDirectory();
	}

}
