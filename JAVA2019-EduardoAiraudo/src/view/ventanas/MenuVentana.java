package view.ventanas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import view.ViewController;

public class MenuVentana {
	private JMenuBar _menubar;
	
	public MenuVentana(JMenuBar menuBar) {
		this._menubar = menuBar;
	}
	
	public JMenuBar getMenuBar() {
		return this._menubar;
	}

	/**
	 * @param verBajasSolicitadas
	 * True = Mostrar menú de bajas solicitadas para usuarios
	 */
	public void mostrarMenuGestionUsuario(boolean verBajasSolicitadas) {
		String GESTION_MENU = "Gestion Usuarios";
		String CREAR_USUARIO = "Crear";
		String LISTAR_USUARIOS = "Usuarios";
		
		JMenu gestionMenu = gJ(GESTION_MENU);
		JMenuItem crearUsuario = gI(CREAR_USUARIO,gestionMenu);
		JMenuItem listarUsuarios = gI(LISTAR_USUARIOS,gestionMenu);
		
		if(verBajasSolicitadas) {
			String BAJAS_USUARIO = "Bajas Lógicas";
			JMenuItem bajasUsuario = gI(BAJAS_USUARIO,gestionMenu);
			
			bajasUsuario.addActionListener(new ActionListener() {	
				int statusUsuario = 0; // ESTADO USUARIO - 0 BAJA, 1 ACTIVO
				@Override
				public void actionPerformed(ActionEvent arg0) {
					ViewController.getInstance().mostrarBajasSolicitadasUsuario(1);
				}
			});
			
		}
		
		crearUsuario.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().mostrarCrearUsuario();
			}
		});
		
		listarUsuarios.addActionListener(new ActionListener() {
			int statusUsuario = 1; // ESTADO USUARIO - 0 BAJA, 1 ACTIVO
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().mostrarPanelUsuarios(1);
			}
		});
	}
	
	
	public void mostrarMenuGestionarEquipos() {
		String EQUIPOS = "Equipos";
		String CREAR_EQUPO = "Crear";
		String BORRAR_EQUIPO = "Borrar";
		String MODIFICAR_EQUIPO = "Modificar";
		String LISTAR_EQUIPOS = "Listar equipos";
		JMenu equiposMenu = gJ(EQUIPOS);
		JMenuItem crearEquipo = gI(CREAR_EQUPO,equiposMenu);
		JMenuItem borrarEquipo = gI(BORRAR_EQUIPO,equiposMenu);
		JMenuItem modificarEquipo = gI(MODIFICAR_EQUIPO,equiposMenu);
		JMenuItem listarEquipos = gI(LISTAR_EQUIPOS,equiposMenu);

		crearEquipo.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().mostrarCrearEquipo();
			}
		});
		
		listarEquipos.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().mostrarPanelEquipos(1);
			}
		});
	}
	
	public void mostrarGestionarIntegrantes(boolean verBajasSolicitadas) {
		String GESTION_MENU = "Gestion Integrantes";
		String CREAR_INTEGRANTE = "Crear";
		String LISTAR_INTEGRANTES = "Integrantes";
		
		JMenu gestionMenu = gJ(GESTION_MENU);
		JMenuItem crearIntegrante = gI(CREAR_INTEGRANTE,gestionMenu);
		JMenuItem listarIntegrantes = gI(LISTAR_INTEGRANTES,gestionMenu);
		
		if(verBajasSolicitadas) {
			String BAJAS_INTEGRANTES = "Bajas Lógicas";
			JMenuItem bajasIntegrantes = gI(BAJAS_INTEGRANTES,gestionMenu);
			bajasIntegrantes.addActionListener(new ActionListener() {	
				@Override
				public void actionPerformed(ActionEvent arg0) {
					ViewController.getInstance().mostrarBajasLógicasIntegrante(1);
				}
			});
			
		}
		
		crearIntegrante.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().mostrarCrearIntegrante();
			}
		});
		
		listarIntegrantes.addActionListener(new ActionListener() {
			int statusIntegrantes = 1; // ESTADO INTEGRANTE - 0 BAJA, 1 ACTIVO
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().mostrarPanelIntegrantes(1);
			}
		});
	}
	
	
	public void mostrarMenuGestionarCampeonatos() {
		String CAMPEONATOS = "Campeonatos";
		String CREAR_CAMPEONATOS = "Crear";
		String BORRAR_CAMPEONATOS = "Borrar";
		String MODIFICAR_CAMPEONATOS = "Modificar";
		JMenu campeonatosMenu = gJ(CAMPEONATOS);
		JMenuItem crearCampeonato = gI(CREAR_CAMPEONATOS,campeonatosMenu);
		JMenuItem borrarCampeonato = gI(BORRAR_CAMPEONATOS,campeonatosMenu);
		JMenuItem modificarCampeonato = gI(MODIFICAR_CAMPEONATOS,campeonatosMenu);
	}
	
	public void mostrarPerfil() {
		String PERFIL = "Perfil";
		String MODIFICAR_PERFIL = "Modificar Perfil";
		String LOGOUT = "Cerrar Sesión";
		JMenu perfilMenu = gJ(PERFIL);
		JMenuItem editarPerfil = gI(MODIFICAR_PERFIL,perfilMenu);
		JMenuItem logoutPerfil = gI(LOGOUT,perfilMenu);
		logoutPerfil.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().logout();
				
			}
		});
		
		editarPerfil.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().perfil();
			}
		});
	}
	
	// JMENU FUNCTIONS
	
	/**
	 * Generate JMenu
	 * @param name
	 * @return JMenu created
	 */
	private JMenu gJ(String name) {
		JMenu newMenu = new JMenu(name);
		_menubar.add(newMenu);
		return newMenu;
	}
	
	/**
	 * Generate JMenuItem
	 * @param name
	 * @return JMenuItem created and ads it to JMenu
	 */
	private JMenuItem gI(String name, JMenu menu) {
		JMenuItem toReturn = new JMenuItem(name);
		menu.add(toReturn);
		return toReturn;
	}

	public void ocultarMenu() {
		_menubar.removeAll();
	}
}
