package view.ventanas;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import datatypes.IntegranteDT;
import enums.EnumEstadosItem;
import enums.EnumRolesIntegrante;
import view.ViewController;
import view.styles.TextPrompt;

import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JComboBox;

public class PanelIntegrantes extends JPanel {
	
	private DefaultTableModel tableModel = new DefaultTableModel() {

	    @Override
	    public boolean isCellEditable(int row, int column) {
	        //all cells false
	        return false;
	    }
	};
	
	private ArrayList<IntegranteDT> _integrantes = new ArrayList<IntegranteDT>();
	private IntegranteDT _integranteSelected;
	private JTable table;
	private EnumEstadosItem _status;
    private String ci_selected;
    private int cedula_Integrante_selected;
    private ArrayList<IntegranteDT> integrantes;
	private JLabel lblCrearIntegrante;
	private JButton eliminarIntegrante;
	private JButton btnEditarIntegrante;
	private JPanel panel;
	private JButton btnCrearIntegrante = new JButton("Crear Integrante");
	private JButton btnRestaurarIntegrante;
	private JButton changeTab;
	private JPanel panel_1;
	private JPanel panel_2;
	private JButton btnAnterior;
	private JLabel lblNroPagina;
	private JButton btnSiguiente;
	private int currentPage;
	private JPanel panel_3;
	private JTextField filterCI;
	private JTextField filterName;
	private JComboBox<EnumRolesIntegrante> rolSelect;
	private IntegranteDT _filter;
	private JButton btnFiltro;


	/**
	 * 
	 */
	public PanelIntegrantes() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		panel_1 = new JPanel();
		setPreferredSize(new Dimension(700, 500));
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		add(panel_1);
		lblCrearIntegrante = new JLabel("Cargando...");
		lblCrearIntegrante.setPreferredSize(new Dimension(350, 40));
		panel_1.add(lblCrearIntegrante);
		lblCrearIntegrante.setHorizontalAlignment(SwingConstants.LEFT);
		lblCrearIntegrante.setFont(new Font("Tekton Pro Cond", Font.PLAIN, 30));
		
		changeTab = new JButton("Cargando..");
		changeTab.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(_status == EnumEstadosItem.ACTIVO) {
					ViewController.getInstance().mostrarBajasL�gicasIntegrante(1);
				} else {
					ViewController.getInstance().mostrarPanelIntegrantes(1);
				}
			}
		});
		changeTab.setPreferredSize(new Dimension(200, 30));
		panel_1.add(changeTab);
		
		panel_3 = new JPanel();
		add(panel_3);
		
		filterCI = new JTextField();
		TextPrompt placeholderCI = new TextPrompt("C�dula", filterCI);
		panel_3.add(filterCI);
		filterCI.setColumns(10);
		
		filterName = new JTextField();
		TextPrompt placeholderName = new TextPrompt("Nombre", filterName);
		panel_3.add(filterName);
		filterName.setColumns(10);
		
		rolSelect = new JComboBox<EnumRolesIntegrante>();
		addRolesToSelect();
		panel_3.add(rolSelect);
		
		btnFiltro = new JButton("Filtrar");
		panel_3.add(btnFiltro);
		btnFiltro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ViewController vw = ViewController.getInstance();
				int filter_ci = -1;
				String ci_text = filterCI.getText();
				boolean error = false;
				System.out.println("CEDULA TEXT:: " + ci_text);
				if(ci_text.length() > 0) {
					try {
						filter_ci = Integer.valueOf(ci_text);
					} catch (Exception e) {
						error = true;
						vw.showAlert("C�dula de identidad debe ser n�merico");
						filterCI.setText("");
					}
				}
				if(!error) {
					String name = filterName.getText();
					EnumRolesIntegrante rol = (EnumRolesIntegrante)rolSelect.getSelectedItem(); 
					_filter = new IntegranteDT(filter_ci,name,rol,_status);
					if(_filter != null) {
						vw.aplicarFiltroIntegrante(_status,currentPage, _filter);
					}
				}
			}
		});
		table = new JTable();
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setPreferredSize(new Dimension(600, 400));
		add(scrollPane);
		
		String[] headers = {"C�dula", "Nombre", "Rol", "Estado"};
		tableModel.setColumnIdentifiers(headers);
		table.setModel(tableModel);
		
		panel = new JPanel();
		add(panel);
		
		panel_2 = new JPanel();
		add(panel_2);
		
		btnAnterior = new JButton("Anterior");
		btnAnterior.setEnabled(false);
		
		btnAnterior.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSiguiente.setEnabled(true);
				currentPage = Integer.parseInt(lblNroPagina.getText());
				currentPage = currentPage-1;
				ViewController.getInstance().cambiarPagina("panelIntegrantes",_status, currentPage,_filter);
				lblNroPagina.setText("" + currentPage);
				checkBtnSiguiente();
				if(currentPage == 1) {
					btnAnterior.setEnabled(false);
				}
			}
		});
		panel_2.add(btnAnterior);
		
		lblNroPagina = new JLabel("1");
		currentPage = Integer.parseInt(lblNroPagina.getText());
		panel_2.add(lblNroPagina);
		
		btnSiguiente = new JButton("Siguiente");
		btnSiguiente.setEnabled(false);
		btnSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentPage = Integer.parseInt(lblNroPagina.getText());
				currentPage = currentPage+1;
				ViewController.getInstance().cambiarPagina("panelIntegrantes",_status, currentPage,_filter);
				btnAnterior.setEnabled(true);
				checkBtnSiguiente();
				lblNroPagina.setText("" + currentPage);
			}
		});
		panel_2.add(btnSiguiente);
		btnEditarIntegrante = new JButton("Editar Integrante");
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent e) {
	            // do some actions here, for example
	            // print first column value from selected row
				if (!e.getValueIsAdjusting() && table.getSelectedRow() != -1) {
					_integranteSelected = _integrantes.get(table.getSelectedRow());
					cedula_Integrante_selected = _integranteSelected.get_cedula();
				}
	        }
	    });
		
		
		btnEditarIntegrante.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(cedula_Integrante_selected == 0) {
					ViewController.getInstance().showAlert("No se ha seleccionado Integrante");
				} else {
					ViewController.getInstance().infoIntegrante(_integranteSelected);
				}
			}
		});
		
		btnCrearIntegrante.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().mostrarCrearIntegrante();
			}
		});
		eliminarIntegrante = new JButton("Cargando..");
		btnRestaurarIntegrante = new JButton("Restaurar");
		
		
		btnRestaurarIntegrante.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(cedula_Integrante_selected == 0) {
					ViewController.getInstance().showAlert("No se ha seleccionado Integrante");
				} else {
					currentPage = Integer.parseInt(lblNroPagina.getText());
					ViewController.getInstance().restaurar(_integranteSelected, currentPage,_filter);
				}
			}
		});
		
		eliminarIntegrante.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(cedula_Integrante_selected == 0) {
					ViewController.getInstance().showAlert("No se ha seleccionado Integrante");
				} else {
					String textDialog;
					if(_status == EnumEstadosItem.ACTIVO) {
						textDialog = "�Seguro que deseas dar de baja a " + cedula_Integrante_selected;
					} else {
						textDialog = "�Seguro que deseas eliminar permanentemente a " + cedula_Integrante_selected;
					}
					int input = JOptionPane.showConfirmDialog(null, textDialog);
					switch (input) {
					case 0:
						currentPage = Integer.parseInt(lblNroPagina.getText());
						if(_status == EnumEstadosItem.ACTIVO) {
							ViewController.getInstance().IntegranteBaja(_integranteSelected,currentPage,_filter);
						} else {
							ViewController.getInstance().IntegranteDelete(_integranteSelected,currentPage,_filter);
						}
						break;
					case 1:
						System.out.println("No");
						break;
					case 2:
						System.out.println("Cancelar");
						break;
					default:
						break;
					}
				}
			}
		});
	}
	
	private void cleanTable() {
		int index = 0;
		table.clearSelection();
		while(tableModel.getRowCount() > 0) {
			tableModel.removeRow(index);
		}
	}
	
	public void mostrarInfoIntegrantes(ArrayList<IntegranteDT> integrantes,EnumEstadosItem status, boolean recargarBtns)
	{
		this.cleanTable();
		table.getSelectionModel().clearSelection();
		this._status = status;
		String textTitle;
		String textBtn;
		if (recargarBtns) {
			if(status == EnumEstadosItem.ACTIVO) {
				panel.add(btnCrearIntegrante);
				textTitle = "Integrantes Activos";
				textBtn = "Dar de Baja";
				changeTab.setText("Ir a Bajas L�gicas");
				panel.add(btnEditarIntegrante);
			} else {
				panel.add(btnRestaurarIntegrante);
				changeTab.setText("Ir a Integrantes Activos");
				textTitle = "Integrantes con Baja L�gica";
				textBtn = "Eliminar Permanentemente";
			};
			panel.add(eliminarIntegrante);
			eliminarIntegrante.setText(textBtn);
			lblCrearIntegrante.setText(textTitle);
		}
		this._integrantes = integrantes;
		for (int i = 0; i < integrantes.size(); i++) {
			IntegranteDT Integrante = _integrantes.get(i);
			String[] personaInfo;
			personaInfo = new String[4];
			personaInfo[0] = String.valueOf(Integrante.get_cedula());
			System.out.println("RECARGANDO:: " +  personaInfo[0]);
			personaInfo[1] = Integrante.get_name().toString();
			personaInfo[2] = Integrante.get_rol().toString();
			personaInfo[3] = Integrante.get_status().toString();
			tableModel.addRow(personaInfo);
		}
		checkBtnSiguiente();
	}
	
	private void checkBtnSiguiente() {
		double amountIntegrantes = ViewController.getInstance().countIntegrantes(_status,_filter);
		System.out.println(amountIntegrantes);
		int maxPage = (int) Math.ceil(amountIntegrantes / ViewController.getInstance().getPages());
		System.out.println("MAX PAGE::" + maxPage + " CURRENT PAGE::" + currentPage);
		if(currentPage >= maxPage) {
			btnSiguiente.setEnabled(false);
		} else {
			btnSiguiente.setEnabled(true);
		}
	}
	
	private void addRolesToSelect() {
		rolSelect.setModel(new DefaultComboBoxModel<>(EnumRolesIntegrante.values()));
	}
}
