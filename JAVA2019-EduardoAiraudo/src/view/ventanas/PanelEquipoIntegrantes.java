package view.ventanas;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import datatypes.EquipoDT;
import datatypes.IntegranteDT;
import view.ViewController;

import javax.swing.JLabel;
import javax.swing.BoxLayout;

import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import java.awt.Font;
import java.util.ArrayList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PanelEquipoIntegrantes extends JPanel {
	
	private IntegranteDT _integranteAgregar;
	private IntegranteDT _integranteRemover;
	
	
	private DefaultTableModel tableModel = new DefaultTableModel() {

	    @Override
	    public boolean isCellEditable(int row, int column) {
	        //all cells false
	        return false;
	    }
	};
	

	private DefaultTableModel tableMIntEquipo = new DefaultTableModel() {

	    @Override
	    public boolean isCellEditable(int row, int column) {
	        //all cells false
	        return false;
	    }
	};
	
	
	private JTable tableIntegrantes;
	private JTable tableIntegrantesEquipo;


	private ArrayList<IntegranteDT> _integrantes;


	private ArrayList<IntegranteDT> _integrantesEquipo;
	private EquipoDT _equipo;


	public PanelEquipoIntegrantes(EquipoDT equipo) {
		this._equipo = equipo;
		String[] headers = {"C�dula", "Nombre", "Rol"};
		tableModel.setColumnIdentifiers(headers);
		System.out.println("PASO");
		tableMIntEquipo.setColumnIdentifiers(headers);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		JPanel panel_2 = new JPanel();
		add(panel_2);
		
		JPanel panel = new JPanel();
		panel_2.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setPreferredSize(new Dimension(330, 400));
		JLabel Integrantes = new JLabel("Integrantes");
		Integrantes.setFont(new Font("Tahoma", Font.PLAIN, 16));
		Integrantes.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(Integrantes);
		
		
		tableIntegrantes = new JTable();
		tableIntegrantes.setModel(tableModel);
		
		JScrollPane scrollPane = new JScrollPane(tableIntegrantes);
		panel.add(scrollPane);
		
		JButton btnAgregarEquipo = new JButton("Agregar Equipo");
		panel.add(btnAgregarEquipo);
		
		JPanel panel_1 = new JPanel();
		panel_2.add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));
		panel_1.setPreferredSize(new Dimension(330, 400));
		JLabel lblIntegrantesEquipo = new JLabel("Integrantes Equipo");
		lblIntegrantesEquipo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel_1.add(lblIntegrantesEquipo);
		
		tableIntegrantesEquipo = new JTable();
		tableIntegrantesEquipo.setModel(tableMIntEquipo);
		
		JScrollPane scrollPane_1 = new JScrollPane(tableIntegrantesEquipo);
		panel_1.add(scrollPane_1);
		
		JButton btnRemoverEquipo = new JButton("Remover Equipo");
		panel_1.add(btnRemoverEquipo);
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		
		JButton btnVolver = new JButton("Volver a Equipos");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().mostrarPanelEquipos(1);
			}
		});
		panel_3.add(btnVolver);
		
		btnAgregarEquipo.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				if(_integranteAgregar == null) {
					ViewController.getInstance().showAlert("No se ha seleccionado Integrante a Agregar");
				} else {
					ViewController.getInstance().agregarIntegranteEquipo(_integranteAgregar,_equipo, true);
				}
			}
		});
		
		btnRemoverEquipo.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				if(_integranteRemover == null) {
					ViewController.getInstance().showAlert("No se ha seleccionado Integrante a Remover");
				} else {
					ViewController.getInstance().agregarIntegranteEquipo(_integranteRemover,_equipo, false);
				}
			}
		});
		this.selectionEventTable();

	}
	
	private void cleanTable(JTable table, DefaultTableModel tableModel) {
		int index = 0;
		table.clearSelection();
		while(tableModel.getRowCount() > 0) {
			tableModel.removeRow(index);
		}
	}
	
	public void loadIntegrantesSinEquipo(ArrayList<IntegranteDT> integrantes) {
		this._integrantes = integrantes;
		this.cleanTable(tableIntegrantes, tableModel);
		for (int i = 0; i < integrantes.size(); i++) {
			IntegranteDT Integrante = _integrantes.get(i);
			String[] personaInfo;
			personaInfo = new String[3];
			personaInfo[0] = String.valueOf(Integrante.get_cedula());
			System.out.println("RECARGANDO:: " +  personaInfo[0]);
			personaInfo[1] = Integrante.get_name().toString();
			personaInfo[2] = Integrante.get_rol().toString();
			tableModel.addRow(personaInfo);
		}
		tableIntegrantes.setModel(tableModel);
	}
	
	public void loadIntegrantesEquipo(ArrayList<IntegranteDT> integrantes) {
		this._integrantesEquipo = integrantes;
		this.cleanTable(tableIntegrantesEquipo, tableMIntEquipo);
		for (int i = 0; i < integrantes.size(); i++) {
			IntegranteDT Integrante = _integrantesEquipo.get(i);
			String[] personaInfo;
			personaInfo = new String[3];
			personaInfo[0] = String.valueOf(Integrante.get_cedula());
			System.out.println("RECARGANDO:: " +  personaInfo[0]);
			personaInfo[1] = Integrante.get_name().toString();
			personaInfo[2] = Integrante.get_rol().toString();
			tableMIntEquipo.addRow(personaInfo);
		}
		tableIntegrantesEquipo.setModel(tableMIntEquipo);
	}
	
	public void selectionEventTable() {
		tableIntegrantes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableIntegrantes.getSelectionModel().addListSelectionListener(new ListSelectionListener(){

			public void valueChanged(ListSelectionEvent e) {
	            // do some actions here, for example
	            // print first column value from selected row
				if (!e.getValueIsAdjusting() && tableIntegrantes.getSelectedRow() != -1) {
					_integranteAgregar = _integrantes.get(tableIntegrantes.getSelectedRow());
				}
	        }
	    });
		
		tableIntegrantesEquipo.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableIntegrantesEquipo.getSelectionModel().addListSelectionListener(new ListSelectionListener(){

			public void valueChanged(ListSelectionEvent e) {
	            // do some actions here, for example
	            // print first column value from selected row
				if (!e.getValueIsAdjusting() && tableIntegrantesEquipo.getSelectedRow() != -1) {
					_integranteRemover = _integrantesEquipo.get(tableIntegrantesEquipo.getSelectedRow());
				}
	        }
	    });
	}

}
