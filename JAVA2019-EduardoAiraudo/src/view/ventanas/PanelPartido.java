package view.ventanas;

import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

import datatypes.PartidoDT;
import enums.EnumCrearPartido;
import enums.EnumEstadosPartido;
import view.ViewController;

import java.awt.FlowLayout;
import java.awt.Dimension;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PanelPartido extends JPanel {
	private JTextField textField;
	private JTextField textField_1;
	public PanelPartido() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JPanel panel = new JPanel();
		add(panel);
		
		JLabel label = new JLabel("Gestor de equipos - Eduardo Airaudo");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Dialog", Font.BOLD, 18));
		panel.add(label);
		
		JPanel panel_1 = new JPanel();
		add(panel_1);
		
		JLabel lblCreatePartido = new JLabel("Crear Partido");
		lblCreatePartido.setHorizontalAlignment(SwingConstants.CENTER);
		lblCreatePartido.setFont(new Font("Dialog", Font.BOLD, 40));
		panel_1.add(lblCreatePartido);
		
		JPanel panel_2 = new JPanel();
		add(panel_2);
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 15));
		
		JLabel label_2 = new JLabel("Local");
		label_2.setPreferredSize(new Dimension(120, 25));
		label_2.setHorizontalAlignment(SwingConstants.RIGHT);
		label_2.setFont(new Font("Segoe UI", Font.PLAIN, 22));
		panel_2.add(label_2);
		
		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.LEFT);
		textField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField.setColumns(15);
		panel_2.add(textField);
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_4 = new JPanel();
		add(panel_4);
		panel_4.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 15));
		
		JLabel lblVisitante = new JLabel("Visitante");
		lblVisitante.setPreferredSize(new Dimension(120, 25));
		lblVisitante.setHorizontalAlignment(SwingConstants.RIGHT);
		lblVisitante.setFont(new Font("Segoe UI", Font.PLAIN, 22));
		panel_4.add(lblVisitante);
		
		textField_1 = new JTextField();
		textField_1.setHorizontalAlignment(SwingConstants.LEFT);
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField_1.setColumns(15);
		panel_4.add(textField_1);
		
		JPanel panel_5 = new JPanel();
		add(panel_5);
		panel_5.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 20));
		
		JButton btnCrearPartido = new JButton("CREAR PARTIDO");
		btnCrearPartido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PartidoDT partido = new PartidoDT(-1,"Equipo 1","Equipo 2", EnumEstadosPartido.ESTADO_PENDIENTE, 1, 2);
				EnumCrearPartido resCrearPartido = ViewController.getInstance().onCreatePartido(partido);
				switch (resCrearPartido) {
				case GOLES_INVALIDO:
					ViewController.getInstance().showAlert("N�MERO INV�LIDO DE GOLES");
					break;
				case TRUE:
					ViewController.getInstance().showAlert("PARTIDO CREADO EXITOSAMENTE");
					break;
				default:
					break;
				}
			}
		});
		btnCrearPartido.setFont(new Font("Segoe UI", Font.BOLD, 23));
		panel_5.add(btnCrearPartido);
	}
	
}
