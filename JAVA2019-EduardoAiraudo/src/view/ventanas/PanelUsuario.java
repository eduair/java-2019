package view.ventanas;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

import datatypes.UserDT;
import enums.EnumRolesUsuario;
import view.ViewController;

import java.awt.FlowLayout;
import java.awt.Dimension;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

// Manipulaci�n fechas JAVA LocalDate tipo de datos
// JCalendar

public class PanelUsuario extends JPanel {
	private JTextField emailField;
	private JPasswordField passwordField;
	private JComboBox rolSelect;
	private UserDT _usuario;
	private boolean crear_usuario;
	private boolean _perfil;
	
	public PanelUsuario(UserDT usuario, boolean perfil) {
		this._usuario = usuario;
		this._perfil = perfil;
		String textBtn = "Guardar";
		String textTitle;
		if(_perfil) {
			textTitle = "Mi Perfil";
		} else if (!perfil && _usuario == null) {
			textTitle = "Crear";
		} else {
			textTitle = "Guardar";
		}
		if(_usuario != null) {
			emailField = new JTextField(_usuario.getEmail());
			passwordField = new JPasswordField(_usuario.getClave());
			emailField.setEnabled(false);
		} else {
			textBtn = "CREAR";
			emailField = new JTextField();
			passwordField = new JPasswordField();
		}

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JPanel panel = new JPanel();
		add(panel);
		
		JLabel label = new JLabel("Gestor de equipos - Eduardo Airaudo");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Dialog", Font.BOLD, 18));
		panel.add(label);
		
		JPanel panel_1 = new JPanel();
		add(panel_1);
		
		JLabel lblUsuarios = new JLabel(textTitle);
		lblUsuarios.setHorizontalAlignment(SwingConstants.CENTER);
		lblUsuarios.setFont(new Font("Dialog", Font.BOLD, 40));
		panel_1.add(lblUsuarios);
		
		JPanel panel_2 = new JPanel();
		add(panel_2);
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 15));
		
		JLabel label_2 = new JLabel("Email");
		label_2.setPreferredSize(new Dimension(120, 25));
		label_2.setHorizontalAlignment(SwingConstants.RIGHT);
		label_2.setFont(new Font("Segoe UI", Font.PLAIN, 22));
		panel_2.add(label_2);
		
		emailField.setHorizontalAlignment(SwingConstants.LEFT);
		emailField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		emailField.setColumns(15);
		panel_2.add(emailField);
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		panel_3.setLayout(new FlowLayout(FlowLayout.LEFT, 20, 5));
		
		JLabel lblRol = new JLabel("Rol");
		lblRol.setPreferredSize(new Dimension(120, 25));
		lblRol.setHorizontalAlignment(SwingConstants.RIGHT);
		lblRol.setFont(new Font("Segoe UI", Font.PLAIN, 22));
		panel_3.add(lblRol);
		
		rolSelect = new JComboBox();
		rolSelect.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel_3.add(rolSelect);
		
		
		JPanel panel_4 = new JPanel();
		add(panel_4);
		panel_4.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 15));
		
		JLabel label_3 = new JLabel("Password");
		label_3.setPreferredSize(new Dimension(120, 25));
		label_3.setHorizontalAlignment(SwingConstants.RIGHT);
		label_3.setFont(new Font("Segoe UI", Font.PLAIN, 22));
		panel_4.add(label_3);
		
		passwordField.setHorizontalAlignment(SwingConstants.LEFT);
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		passwordField.setColumns(15);
		panel_4.add(passwordField);
		
		JPanel panel_5 = new JPanel();
		add(panel_5);
		JButton btnEditar = new JButton("GUARDAR");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(_usuario != null) {
					ViewController instance = ViewController.getInstance();
					String email = emailField.getText();
					String clave = passwordField.getText();
					if(email.isEmpty()) {
						instance.showAlert("Falta Email");
					} else if (clave.isEmpty()) {
						instance.showAlert("Falta Clave");
					} else {
						EnumRolesUsuario rol = (EnumRolesUsuario)rolSelect.getSelectedItem();
						UserDT usuario = new UserDT(email,clave,rol);
						boolean update = instance.editarUsuario(usuario);
						if(update) {
							instance.showAlert("Usuario editado con �xito");
						} else {
							instance.showAlert("Error al editar usuario");
						}
					}
				} else {
					ViewController instance = ViewController.getInstance();
					String email = emailField.getText();
					String clave = passwordField.getText();
					if(email.isEmpty()) {
						instance.showAlert("Falta Email");
					} else if (clave.isEmpty()) {
						instance.showAlert("Falta Clave");
					} else {
			    		boolean validMail = ViewController.getInstance().testRegexEmail(email);
			    		if(validMail){
							EnumRolesUsuario rol = (EnumRolesUsuario)rolSelect.getSelectedItem();
							UserDT usuario = new UserDT(email,clave,rol);
							boolean update = instance.crearUsuario(usuario);
							if(update) {
								instance.showAlert("Usuario creado con �xito");
							} else {
								instance.showAlert("Usuario ya existe");
							}
						} else {
							instance.showAlert("Email inv�lido");
						}
					}
				}
			}
		});
		panel_5.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		btnEditar.setFont(new Font("Segoe UI", Font.BOLD, 23));
		panel_5.add(btnEditar);
		btnEditar.setPreferredSize(new Dimension(280,30));
		
		JPanel panel_6 = new JPanel();
		add(panel_6);
		panel_6.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnVolverAUsuarios = new JButton("VOLVER A USUARIOS");
		if(!_perfil) {
			panel_6.add(btnVolverAUsuarios);
		}
		btnVolverAUsuarios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().mostrarPanelUsuarios(1);
			}
		});
		btnVolverAUsuarios.setFont(new Font("Segoe UI", Font.BOLD, 23));
		btnVolverAUsuarios.setPreferredSize(new Dimension(280,30));
		
		this.addRolesToSelect();
	}
	
	private DefaultComboBoxModel comboBox(boolean perfil) {
		return null;
	}
	
	private void addRolesToSelect() {
		DefaultComboBoxModel all = new DefaultComboBoxModel<>(EnumRolesUsuario.values());
		DefaultComboBoxModel noAdmin = new DefaultComboBoxModel<>();
		noAdmin.addElement(EnumRolesUsuario.GESTOR_EQUIPOS);
		noAdmin.addElement(EnumRolesUsuario.INTEGRANTE);
		if(_usuario != null) {
			rolSelect.setSelectedItem(_usuario.getRol());
			boolean condicionalRole = _usuario.getRol() == EnumRolesUsuario.ADMINISTRADOR;
			if(_perfil) {
				// Todos los roles para que muestre Admin bloqueado, rol que tenga queda bloqueado
				rolSelect.setModel(all);
				rolSelect.setEnabled(false);
			} else {
				rolSelect.setModel(noAdmin);
			}
		} else {
			rolSelect.setModel(noAdmin);
			
		}
	}
}
