package view.ventanas;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.Component;
import java.awt.Desktop.Action;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import java.awt.FlowLayout;
import javax.swing.UIManager;
import javax.swing.border.Border;

import datatypes.UserDT;
import view.ViewController;
import view.styles.TextPrompt;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.BorderLayout;

public class PanelLogin extends JPanel  {
	private JTextField emailField;
	private JTextField passwordField;
	private JLabel lblErrorMail;
	private boolean lastshow = false;
	private JButton loginBtn;
	
	public PanelLogin() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JPanel panel_4 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_4.getLayout();
		flowLayout_1.setVgap(15);
		add(panel_4);
		
		JLabel lblGestor = new JLabel("Gestor de equipos - Eduardo Airaudo");
		lblGestor.setHorizontalAlignment(SwingConstants.CENTER);
		lblGestor.setFont(new Font("Source Sans Pro Semibold", Font.BOLD, 18));
		panel_4.add(lblGestor);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setVgap(25);
		add(panel);
		
		JLabel titlePanelLbl = new JLabel("Login");
		titlePanelLbl.setHorizontalAlignment(SwingConstants.CENTER);
		titlePanelLbl.setFont(new Font("Source Sans Pro Semibold", Font.BOLD, 40));
		panel.add(titlePanelLbl);
		
		JPanel panel_1 = new JPanel();
		add(panel_1);
		panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 15));
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setPreferredSize(new Dimension(120, 25));
		lblEmail.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEmail.setFont(new Font("Segoe UI", Font.PLAIN, 22));
		panel_1.add(lblEmail);
		
		emailField = new JTextField("admin@mail.com");
		emailField.setHorizontalAlignment(SwingConstants.LEFT);
		emailField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_1.add(emailField);
		TextPrompt placeholder = new TextPrompt("example@mail.com", emailField);
		emailField.setColumns(15);
		addKeyListenerToField(emailField,"email");
		
		JPanel panel_5 = new JPanel();
		add(panel_5);
		panel_5.setLayout(new BorderLayout(0, 0));
		
		lblErrorMail = new JLabel();
		lblErrorMail.setHorizontalAlignment(SwingConstants.CENTER);
		panel_5.add(lblErrorMail);
		
		JPanel panel_2 = new JPanel();
		add(panel_2);
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 15));
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setPreferredSize(new Dimension(120, 25));
		lblPassword.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPassword.setFont(new Font("Segoe UI", Font.PLAIN, 22));
		panel_2.add(lblPassword);
		
		passwordField = new JPasswordField("123");
		passwordField.setHorizontalAlignment(SwingConstants.LEFT);
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		passwordField.setColumns(15);
		TextPrompt placeholderPsw = new TextPrompt("password", passwordField);
		placeholderPsw.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_2.add(passwordField);
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		panel_3.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 20));
		
		loginBtn = new JButton("LOGIN");
		loginBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String email = emailField.getText();
				ViewController viewC = ViewController.getInstance();
				String password = passwordField.getText();
				if(email.isEmpty()) {
					viewC.showAlert("Falta Email");
				} else if (password.isEmpty()) {
					viewC.showAlert("Falta Clave");
				} else {
					UserDT userLogin = new UserDT(email,password);
					viewC.login(userLogin);
				}
			}
		});
		loginBtn.setFont(new Font("Segoe UI", Font.BOLD, 23));
		panel_3.add(loginBtn);
		
		//TEST 
		loginBtn.setEnabled(true);

		
		
		Border blackline = BorderFactory.createLineBorder(Color.black);
		this.setBorder(blackline);
		
		ViewController.getInstance().setDefaultBtn(this.loginBtn);

		
	}
	
	public void showErrorEmail(boolean show) {
		boolean detectChanges = show != lastshow;
		if(detectChanges) {
			if(show) {
				String text = "Please enter a valid email";
				String errorMessage = "<html><font color='red'>" +text+ "</font></html>";
				lblErrorMail.setText(errorMessage);
				loginBtn.setEnabled(false);
			} else {
				loginBtn.setEnabled(true);
				lblErrorMail.setText("");
			}
			lastshow = show;
		}
	}
	
	public void addKeyListenerToField(JTextField field, String name) {
		KeyListener keyListener = new KeyListener() {
	      public void keyPressed(KeyEvent keyEvent) {
	      }

	      public void keyReleased(KeyEvent keyEvent) {
	    	  if(name == "email") {
	    		  String emailText = field.getText();
	    		  boolean validMail = ViewController.getInstance().testRegexEmail(emailText);
	    		  if(!validMail) {
	    			  ViewController.getInstance().showErrorEmail(true);
	    		  } else {
	    			  ViewController.getInstance().showErrorEmail(false);
	    		  }
	    	  }
	      }

	      public void keyTyped(KeyEvent keyEvent) {
	    	  
	      }

	      private void printIt(String title, KeyEvent keyEvent) {
	        int keyCode = keyEvent.getKeyCode();
	      }
	    };
	    field.addKeyListener(keyListener);
	}
	
	AbstractAction action = new AbstractAction()
	{
	    @Override
	    public void actionPerformed(ActionEvent e)
	    {
	        System.out.println("some action");
	    }
	};
}
