package view.ventanas;
import java.util.ArrayList;

import java.util.Stack;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.SystemColor;
import javax.swing.JPanel;

import datatypes.EquipoDT;
import datatypes.IntegranteDT;
import datatypes.UserDT;
import enums.EnumEstadosItem;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

public class VentanaUnica extends JFrame 
{
	private MenuVentana _menubar = new MenuVentana(new JMenuBar());
	private JPanel _mainContent = new JPanel();
	private PanelLogin panelLogin;
	private PanelPartido panelPartido;
	private PanelUsuario panelUsuario;
	private PanelUsuarios panelUsuarios;
	private PanelIntegrante panelIntegrante;
	private PanelIntegrantes panelIntegrantes;
	private PanelEquipo panelEquipo;
	private PanelEquipos panelEquipos;
	private PanelEquipoIntegrantes panelEquipoIntegrantes;
	
	public VentanaUnica() {
		setBounds(100, 100, 900, 700);
		setBackground(SystemColor.activeCaption);
		// Agregado Barra de Men�
		setJMenuBar(_menubar.getMenuBar());
		FlowLayout flowLayout = (FlowLayout) _mainContent.getLayout();
		flowLayout.setVgap(30);
		//Agregado pantalla principal
		getContentPane().add(this._mainContent, BorderLayout.CENTER);
	}
		
	public void mostrarMenuGestionUsuario(boolean verBajasSolicitadas) {
		_menubar.mostrarMenuGestionUsuario(verBajasSolicitadas);
	}
	
	public void mostrarMenuGestionarEquipos() {
		_menubar.mostrarMenuGestionarEquipos();
	}
	
	public void mostrarMenuGestionarCampeonatos() {
		_menubar.mostrarMenuGestionarCampeonatos();
	}
	
	public void setDefaultBtn(JButton btn) {
		getRootPane().setDefaultButton(btn);
	}
	
	public void addPanel(String panelName, UserDT user, boolean perfil) {
		this._mainContent.removeAll();
		switch (panelName) {
		case "panelUsuario":
			panelUsuario = new PanelUsuario(user,perfil);
			this._mainContent.add(panelUsuario); 
			break;
		default:
			break;
		}
		reloadVentana();
	}
	
	public void addPanel(String panelName) {
		this._mainContent.removeAll();
		switch (panelName) {
		case "panelLogin":
			panelLogin = new PanelLogin();
			this._mainContent.add(panelLogin); 
			break;
		case "panelPartido":
			panelPartido = new PanelPartido();
			this._mainContent.add(panelPartido); 
			break;
		case "panelUsuarios":
			panelUsuarios = new PanelUsuarios();
			this._mainContent.add(panelUsuarios); 
			break;
		case "panelIntegrante":
			panelIntegrante = new PanelIntegrante();
			this._mainContent.add(panelIntegrante); 
			break;
		case "panelIntegrantes":
			panelIntegrantes = new PanelIntegrantes();
			this._mainContent.add(panelIntegrantes); 
			break;
		case "panelCrearEquipo":
			panelEquipo = new PanelEquipo();
			this._mainContent.add(panelEquipo); 
			break;
		case "panelEquipos":
			panelEquipos = new PanelEquipos();
			this._mainContent.add(panelEquipos); 
			break;
		default:
			break;
		}
		reloadVentana();
	}
	
	public void reloadVentana() {
		this.invalidate();
		this.validate();
		this.repaint();
	}
	
	public void showAlert(String message) {
		JOptionPane.showMessageDialog(this, message);
	}

	public void showErrorEmail(boolean b) {
		// TODO Auto-generated method stub
		panelLogin.showErrorEmail(b);
	}

	public void ocultarMenu() {
		_menubar.ocultarMenu();
	}

	public void mostrarPerfil() {
		_menubar.mostrarPerfil();
	}

	public void mostrarInfoUsuarios(ArrayList<UserDT> usuarios,boolean status) {
		panelUsuarios.mostrarInfoUsuarios(usuarios,status, true);
		
	}

	public void mostrarGestionarIntegrantes(boolean b) {
		_menubar.mostrarGestionarIntegrantes(b);
	}

	public void mostrarInfoIntegrantes(ArrayList<IntegranteDT> integrantes, EnumEstadosItem status) {
		panelIntegrantes.mostrarInfoIntegrantes(integrantes,status, true);
	}

	public void addPanelIntegrante(IntegranteDT integrante) {
		this._mainContent.removeAll();
		panelIntegrante = new PanelIntegrante(integrante);
		this._mainContent.add(panelIntegrante); 
		reloadVentana();
	}

	public void mostrarInfoEquipos(ArrayList<EquipoDT> equipos, EnumEstadosItem status) {
		panelEquipos.mostrarInfoEquipos(equipos,status, true);
	}

	public void addPanelEquipo(EquipoDT equipo) {
		this._mainContent.removeAll();
		panelEquipo = new PanelEquipo(equipo);
		this._mainContent.add(panelEquipo); 
		reloadVentana();
	}

	public void addPanelIntegrantesEquipo(EquipoDT equipo) {
		this._mainContent.removeAll();
		panelEquipoIntegrantes = new PanelEquipoIntegrantes(equipo);
		this._mainContent.add(panelEquipoIntegrantes); 
		reloadVentana();
	}

	public void loadIntegrantesSinEquipo(ArrayList<IntegranteDT> integrantesSinEquipo) {
		panelEquipoIntegrantes.loadIntegrantesSinEquipo(integrantesSinEquipo);
		
	}

	public void loadIntegrantesEquipo(ArrayList<IntegranteDT> integrantesEquipo) {
		panelEquipoIntegrantes.loadIntegrantesEquipo(integrantesEquipo);
		
	}
	
	
}