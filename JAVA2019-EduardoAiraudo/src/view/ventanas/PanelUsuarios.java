package view.ventanas;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import datatypes.UserDT;
import view.ViewController;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTabbedPane;
import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import java.awt.Component;

public class PanelUsuarios extends JPanel {
	
	private DefaultTableModel tableModel = new DefaultTableModel() {

	    @Override
	    public boolean isCellEditable(int row, int column) {
	        //all cells false
	        return false;
	    }
	};
	
	private ArrayList<UserDT> _usuarios = new ArrayList<UserDT>();
	private UserDT _userSelected;
	private JTable table;
	private boolean _status;
    private String ci_selected;
    private String email_usuario_selected;
    private ArrayList<UserDT> usuarios;
	private JLabel lblCrearUsuario;
	private JButton eliminarUsuario;
	private JButton btnEditarUsuario;
	private JPanel panel;
	private JButton btnCrearUsuario = new JButton("Crear Usuario");
	private JButton btnRestaurarUsuario;
	private JButton changeTab;
	private JPanel panel_1;
	private JPanel panel_2;
	private JButton btnAnterior;
	private JLabel lblNroPagina;
	private JButton btnSiguiente;
	private int currentPage;


	/**
	 * 
	 */
	public PanelUsuarios() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		panel_1 = new JPanel();
		setPreferredSize(new Dimension(700, 500));
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		add(panel_1);
		lblCrearUsuario = new JLabel("Cargando...");
		lblCrearUsuario.setPreferredSize(new Dimension(350, 40));
		panel_1.add(lblCrearUsuario);
		lblCrearUsuario.setHorizontalAlignment(SwingConstants.LEFT);
		lblCrearUsuario.setFont(new Font("Tekton Pro Cond", Font.PLAIN, 30));
		
		changeTab = new JButton("Cargando..");
		changeTab.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(_status) {
					ViewController.getInstance().mostrarBajasSolicitadasUsuario(1);
				} else {
					ViewController.getInstance().mostrarPanelUsuarios(1);
				}
			}
		});
		changeTab.setPreferredSize(new Dimension(200, 30));
		panel_1.add(changeTab);
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setPreferredSize(new Dimension(600, 400));
		add(scrollPane);
		
		String[] headers = {"Email", "Rol"};
		tableModel.setColumnIdentifiers(headers);
		table.setModel(tableModel);
		
		panel = new JPanel();
		add(panel);
		
		panel_2 = new JPanel();
		add(panel_2);
		
		btnAnterior = new JButton("Anterior");
		btnAnterior.setEnabled(false);
		
		btnAnterior.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSiguiente.setEnabled(true);
				currentPage = Integer.parseInt(lblNroPagina.getText());
				currentPage = currentPage-1;
				ViewController.getInstance().cambiarPagina("panelUsuarios",_status, currentPage);
				lblNroPagina.setText("" + currentPage);
				checkBtnSiguiente();
				if(currentPage == 1) {
					btnAnterior.setEnabled(false);
				}
			}
		});
		panel_2.add(btnAnterior);
		
		lblNroPagina = new JLabel("1");
		currentPage = Integer.parseInt(lblNroPagina.getText());
		panel_2.add(lblNroPagina);
		
		btnSiguiente = new JButton("Siguiente");
		btnSiguiente.setEnabled(false);
		btnSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentPage = Integer.parseInt(lblNroPagina.getText());
				currentPage = currentPage+1;
				ViewController.getInstance().cambiarPagina("panelUsuarios",_status, currentPage);
				btnAnterior.setEnabled(true);
				checkBtnSiguiente();
				lblNroPagina.setText("" + currentPage);
			}
		});
		panel_2.add(btnSiguiente);
		btnEditarUsuario = new JButton("Editar Usuario");

		table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent e) {
	            // do some actions here, for example
	            // print first column value from selected row
				if (!e.getValueIsAdjusting() && table.getSelectedRow() != -1) {
					_userSelected = _usuarios.get(table.getSelectedRow());
					email_usuario_selected = _userSelected.getEmail();
				}
	        }
	    });
		
		
		btnEditarUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(email_usuario_selected == null) {
					ViewController.getInstance().showAlert("No se ha seleccionado usuario");
				} else {
					ViewController.getInstance().infoUsuario(_userSelected);
				}
			}
		});
		
		btnCrearUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().mostrarCrearUsuario();
			}
		});
		eliminarUsuario = new JButton("Cargando..");
		btnRestaurarUsuario = new JButton("Restaurar");
		
		
		btnRestaurarUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(email_usuario_selected == null) {
					ViewController.getInstance().showAlert("No se ha seleccionado usuario");
				} else {
					currentPage = Integer.parseInt(lblNroPagina.getText());
					ViewController.getInstance().restaurar(_userSelected, currentPage);
				}
			}
		});
		
		eliminarUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(email_usuario_selected == null) {
					ViewController.getInstance().showAlert("No se ha seleccionado usuario");
				} else {
					String textDialog;
					if(_status) {
						textDialog = "�Seguro que deseas dar de baja a " + email_usuario_selected;
					} else {
						textDialog = "�Seguro que deseas eliminar permanentemente a " + email_usuario_selected;
					}
					int input = JOptionPane.showConfirmDialog(null, textDialog);
					switch (input) {
					case 0:
						currentPage = Integer.parseInt(lblNroPagina.getText());
						if(_status) {
							ViewController.getInstance().usuarioBaja(_userSelected,currentPage);
						} else {
							ViewController.getInstance().usuarioDelete(_userSelected,currentPage);
						}
						break;
					case 1:
						System.out.println("No");
						break;
					case 2:
						System.out.println("Cancelar");
						break;
					default:
						break;
					}
				}
			}
		});
	}
	
	private void cleanTable() {
		int index = 0;
		table.clearSelection();
		while(tableModel.getRowCount() > 0) {
			tableModel.removeRow(index);
		}
	}
	
	public void mostrarInfoUsuarios(ArrayList<UserDT> usuarios,boolean status, boolean recargarBtns)
	{
		this.cleanTable();
		table.getSelectionModel().clearSelection();
		this._status = status;
		String textTitle;
		String textBtn;
		if (recargarBtns) {
			if(_status) {
				panel.add(btnCrearUsuario);
				textTitle = "Usuarios Activos";
				textBtn = "Dar de Baja";
				changeTab.setText("Ir a Bajas L�gicas");
				panel.add(btnEditarUsuario);
			} else {
				panel.add(btnRestaurarUsuario);
				changeTab.setText("Ir a Usuarios Activos");
				textTitle = "Usuarios con Baja L�gica";
				textBtn = "Eliminar Permanentemente";
			}
			panel.add(eliminarUsuario);
			eliminarUsuario.setText(textBtn);
			lblCrearUsuario.setText(textTitle);
		}
		this._usuarios = usuarios;
		for (int i = 0; i < usuarios.size(); i++) {
			UserDT usuario = _usuarios.get(i);
			String[] personaInfo;
			personaInfo = new String[2];
			personaInfo[0] = usuario.getEmail();
			System.out.println("RECARGANDO:: " +  personaInfo[0]);
			personaInfo[1] = usuario.getRol().toString();
			tableModel.addRow(personaInfo);
		}
		checkBtnSiguiente();
	}
	
	private void checkBtnSiguiente() {
		double amountUsuarios = ViewController.getInstance().countUsers(_status);
		System.out.println(amountUsuarios);
		int maxPage = (int) Math.ceil(amountUsuarios / ViewController.getInstance().getPages());
		System.out.println("MAX PAGE::" + maxPage + " CURRENT PAGE::" + currentPage);
		if(currentPage >= maxPage) {
			btnSiguiente.setEnabled(false);
		} else {
			btnSiguiente.setEnabled(true);
		}
	}
}
