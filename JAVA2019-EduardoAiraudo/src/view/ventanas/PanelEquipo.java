package view.ventanas;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.awt.FlowLayout;
import javax.swing.SwingConstants;

import com.toedter.calendar.JCalendar;

import datatypes.EquipoDT;
import main.Factory;
import view.ViewController;

import java.awt.Dimension;
import javax.swing.JTextField;

public class PanelEquipo extends JPanel {
	private JTextField nombreField;
	private JTextField direccionField;
	private JTextField phoneField;
	private JCalendar calendario;
	public PanelEquipo(EquipoDT equipo) {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JPanel panel = new JPanel();
		add(panel);
		
		JLabel lblNewLabel = new JLabel("Creaci\u00F3n de Equipo");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 24));
		panel.add(lblNewLabel);
		
		JPanel panel_2 = new JPanel();
		add(panel_2);
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 15));
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setPreferredSize(new Dimension(120, 25));
		lblNombre.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNombre.setFont(new Font("Segoe UI", Font.PLAIN, 22));
		panel_2.add(lblNombre);
		
		nombreField = new JTextField();
		panel_2.add(nombreField);
		nombreField.setColumns(20);
		
		JPanel panel_1 = new JPanel();
		add(panel_1);
		panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 15));
		
		JLabel lblDireccin = new JLabel("Direcci\u00F3n");
		lblDireccin.setPreferredSize(new Dimension(120, 25));
		lblDireccin.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDireccin.setFont(new Font("Segoe UI", Font.PLAIN, 22));
		panel_1.add(lblDireccin);
		
		direccionField = new JTextField();
		direccionField.setColumns(20);
		panel_1.add(direccionField);
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		panel_3.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 15));
		
		JLabel lblTelfono = new JLabel("Tel\u00E9fono");
		lblTelfono.setPreferredSize(new Dimension(120, 25));
		lblTelfono.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTelfono.setFont(new Font("Segoe UI", Font.PLAIN, 22));
		panel_3.add(lblTelfono);
		
		phoneField = new JTextField();
		phoneField.setColumns(20);
		panel_3.add(phoneField);
		
		JPanel panel_4 = new JPanel();
		add(panel_4);
		
		panel_4.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 15));
		
		JPanel panel_4_5 = new JPanel();
		add(panel_4_5);
		panel_4_5.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 15));
		
		JLabel lblFechaDeFundacin = new JLabel("Fecha de Fundaci\u00F3n");
		lblFechaDeFundacin.setPreferredSize(new Dimension(250, 25));
		lblFechaDeFundacin.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFechaDeFundacin.setFont(new Font("Segoe UI", Font.PLAIN, 22));
		panel_4.add(lblFechaDeFundacin);
		
		calendario = new JCalendar();
		panel_4_5.add(calendario);
		
		JPanel panel_5 = new JPanel();
		add(panel_5);
		panel_5.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 15));
		
		
		if(equipo != null) {
			nombreField.setText(equipo.get_name());
			phoneField.setText(equipo.get_phone());
			direccionField.setText(equipo.get_adress());
			LocalDate date = equipo.get_creation_date();
			Date dateD = Factory.getLogicCtrl().convertToDateViaInstant(date);
			calendario.setDate(dateD);
		}
		
		
		JButton crear = new JButton("Crear Equipo");
		crear.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Date date_creacion = calendario.getDate();
				LocalDate creation_date = ViewController.getInstance().convertToLocalDate(date_creacion);
				String name = nombreField.getText();
				String direccion = direccionField.getText();
				String phone = phoneField.getText();
				ViewController vw = ViewController.getInstance();
				if(name.isEmpty()) {
					vw.showAlert("Falta nombre");
				} else if (direccion.isEmpty()) {
					vw.showAlert("Falta direcci�n");
				} else if(phone.isEmpty()) {
					vw.showAlert("Falta tel�fono");
				} else {
					EquipoDT equipo = new EquipoDT(name,direccion,phone,creation_date);
					boolean creacion = vw.createEquipo(equipo);
					if(creacion) {
						vw.showAlert("Equipo creado exitosamente");
					} else { 
						vw.showAlert("Error al crear equipo");
					}

				}
			}
		});
		panel_5.add(crear);
	}
	
	public PanelEquipo() {
		this(null);
	}

	
}
