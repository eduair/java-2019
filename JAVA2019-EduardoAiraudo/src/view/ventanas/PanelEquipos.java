package view.ventanas;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import datatypes.EquipoDT;
import enums.EnumEstadosItem;
import view.ViewController;
import view.styles.TextPrompt;

import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JComboBox;

public class PanelEquipos extends JPanel {
	
	private DefaultTableModel tableModel = new DefaultTableModel() {

	    @Override
	    public boolean isCellEditable(int row, int column) {
	        //all cells false
	        return false;
	    }
	};
	
	private ArrayList<EquipoDT> _equipos = new ArrayList<EquipoDT>();
	private EquipoDT _equipoSelected;
	private JTable table;
	private EnumEstadosItem _status;
    private int id_equipo_selected;
    private ArrayList<EquipoDT> equipos;
	private JLabel lblCrearEquipo;
	private JButton eliminarEquipo;
	private JButton btnEditarEquipo;
	private JPanel panel;
	private JButton btnCrearEquipo = new JButton("Crear Equipo");
	private JButton btnRestaurarEquipo;
	private JButton changeTab;
	private JPanel panel_1;
	private JPanel panel_2;
	private JButton btnAnterior;
	private JLabel lblNroPagina;
	private JButton btnSiguiente;
	private int currentPage;
	private JPanel panel_3;
	private EquipoDT _filter;

	private JButton btnAgregarIntegrantes;


	/**
	 * 
	 */
	public PanelEquipos() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		panel_1 = new JPanel();
		setPreferredSize(new Dimension(700, 500));
		FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
		add(panel_1);
		lblCrearEquipo = new JLabel("Cargando...");
		lblCrearEquipo.setPreferredSize(new Dimension(350, 40));
		panel_1.add(lblCrearEquipo);
		lblCrearEquipo.setHorizontalAlignment(SwingConstants.LEFT);
		lblCrearEquipo.setFont(new Font("Tekton Pro Cond", Font.PLAIN, 30));
		
		changeTab = new JButton("Cargando..");
		changeTab.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(_status == EnumEstadosItem.ACTIVO) {
					ViewController.getInstance().mostrarBajasL�gicasEquipo(1);
				} else {
					ViewController.getInstance().mostrarPanelEquipos(1);
				}
			}
		});
		changeTab.setPreferredSize(new Dimension(200, 30));
		panel_1.add(changeTab);
		
		panel_3 = new JPanel();
		add(panel_3);
		
		table = new JTable();
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setPreferredSize(new Dimension(600, 400));
		add(scrollPane);
		
		String[] headers = {"ID", "Nombre", "Direcci�n", "Tel�fono", "Fecha de Fundaci�n", "Status"};
		tableModel.setColumnIdentifiers(headers);
		table.setModel(tableModel);
		
		panel = new JPanel();
		add(panel);
		
		panel_2 = new JPanel();
		add(panel_2);
		
		btnAnterior = new JButton("Anterior");
		btnAnterior.setEnabled(false);
		
		btnAnterior.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSiguiente.setEnabled(true);
				currentPage = Integer.parseInt(lblNroPagina.getText());
				currentPage = currentPage-1;
				ViewController.getInstance().cambiarPagina("panelEquipos",_status, currentPage,_filter);
				lblNroPagina.setText("" + currentPage);
				checkBtnSiguiente();
				if(currentPage == 1) {
					btnAnterior.setEnabled(false);
				}
			}
		});
		panel_2.add(btnAnterior);
		
		lblNroPagina = new JLabel("1");
		currentPage = Integer.parseInt(lblNroPagina.getText());
		panel_2.add(lblNroPagina);
		
		btnSiguiente = new JButton("Siguiente");
		btnSiguiente.setEnabled(false);
		btnSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentPage = Integer.parseInt(lblNroPagina.getText());
				currentPage = currentPage+1;
				ViewController.getInstance().cambiarPagina("panelEquipos",_status, currentPage,_filter);
				btnAnterior.setEnabled(true);
				checkBtnSiguiente();
				lblNroPagina.setText("" + currentPage);
			}
		});
		panel_2.add(btnSiguiente);
		btnEditarEquipo = new JButton("Editar Equipo");
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent e) {
	            // do some actions here, for example
	            // print first column value from selected row
				if (!e.getValueIsAdjusting() && table.getSelectedRow() != -1) {
					_equipoSelected = _equipos.get(table.getSelectedRow());
					id_equipo_selected = _equipoSelected.get_id();
				}
	        }
	    });
		
		
		btnEditarEquipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(id_equipo_selected == 0) {
					ViewController.getInstance().showAlert("No se ha seleccionado Equipo");
				} else {
					ViewController.getInstance().infoEquipo(_equipoSelected);
				}
			}
		});
		
		btnAgregarIntegrantes = new JButton("Editar miembros");
		
		btnAgregarIntegrantes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(id_equipo_selected == 0) {
					ViewController.getInstance().showAlert("No se ha seleccionado Equipo");
				} else {
					ViewController.getInstance().editarMiembrosEquipo(_equipoSelected);
				}
			}
		});
		
		btnCrearEquipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().mostrarCrearEquipo();
			}
		});
		eliminarEquipo = new JButton("Cargando..");
		btnRestaurarEquipo = new JButton("Restaurar");
		
		
		btnRestaurarEquipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(id_equipo_selected == 0) {
					ViewController.getInstance().showAlert("No se ha seleccionado Equipo");
				} else {
					currentPage = Integer.parseInt(lblNroPagina.getText());
					ViewController.getInstance().restaurar(_equipoSelected, currentPage,_filter);
				}
			}
		});
		
		eliminarEquipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(id_equipo_selected == 0) {
					ViewController.getInstance().showAlert("No se ha seleccionado Equipo");
				} else {
					String textDialog;
					if(_status == EnumEstadosItem.ACTIVO) {
						textDialog = "�Seguro que deseas dar de baja a " + id_equipo_selected;
					} else {
						textDialog = "�Seguro que deseas eliminar permanentemente a " + id_equipo_selected;
					}
					int input = JOptionPane.showConfirmDialog(null, textDialog);
					switch (input) {
					case 0:
						currentPage = Integer.parseInt(lblNroPagina.getText());
						if(_status == EnumEstadosItem.ACTIVO) {
							ViewController.getInstance().EquipoBaja(_equipoSelected,currentPage,_filter);
						} else {
							ViewController.getInstance().EquipoDelete(_equipoSelected,currentPage,_filter);
						}
						break;
					case 1:
						System.out.println("No");
						break;
					case 2:
						System.out.println("Cancelar");
						break;
					default:
						break;
					}
				}
			}
		});
	}
	
	private void cleanTable() {
		int index = 0;
		table.clearSelection();
		while(tableModel.getRowCount() > 0) {
			tableModel.removeRow(index);
		}
	}
	
	public void mostrarInfoEquipos(ArrayList<EquipoDT> equipos,EnumEstadosItem status, boolean recargarBtns)
	{
		this.cleanTable();
		System.out.println("MOSTRAR INFO EQUIPOS");
		table.getSelectionModel().clearSelection();
		this._status = status;
		String textTitle;
		String textBtn;
		if (recargarBtns) {
			if(status == EnumEstadosItem.ACTIVO) {
				panel.add(btnCrearEquipo);
				textTitle = "Equipos Activos";
				textBtn = "Dar de Baja";
				changeTab.setText("Ir a Bajas L�gicas");
				panel.add(btnEditarEquipo);
				panel.add(btnAgregarIntegrantes);
			} else {
				panel.add(btnRestaurarEquipo);
				changeTab.setText("Ir a Equipos Activos");
				textTitle = "Equipos con Baja L�gica";
				textBtn = "Eliminar Permanentemente";
			};
			panel.add(eliminarEquipo);
			eliminarEquipo.setText(textBtn);
			lblCrearEquipo.setText(textTitle);
		}
		this._equipos = equipos;
		for (int i = 0; i < equipos.size(); i++) {
			EquipoDT Equipo = _equipos.get(i);
			String[] equipoInfo;
			equipoInfo = new String[6];
			equipoInfo[0] = String.valueOf(Equipo.get_id());
			System.out.println("RECARGANDO:: " +  equipoInfo[0]);
			equipoInfo[1] = Equipo.get_name().toString();
			equipoInfo[2] = Equipo.get_adress().toString();
			equipoInfo[3] = Equipo.get_phone().toString();
			equipoInfo[4] = Equipo.get_creation_date().toString();
			equipoInfo[5] = Equipo.get_status().toString();
			tableModel.addRow(equipoInfo);
		}
		checkBtnSiguiente();
	}
	
	private void checkBtnSiguiente() {
		double amountEquipos = ViewController.getInstance().countEquipos(_status,_filter);
		System.out.println(amountEquipos);
		int maxPage = (int) Math.ceil(amountEquipos / ViewController.getInstance().getPages());
		System.out.println("MAX PAGE::" + maxPage + " CURRENT PAGE::" + currentPage);
		if(currentPage >= maxPage) {
			btnSiguiente.setEnabled(false);
		} else {
			btnSiguiente.setEnabled(true);
		}
	}
}
