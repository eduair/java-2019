package view.ventanas;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

import datatypes.IntegranteDT;
import datatypes.UserDT;
import enums.EnumEstadosItem;
import enums.EnumRolesIntegrante;
import enums.EnumRolesUsuario;
import enums.EnumRolesIntegrante;
import view.ViewController;

import java.awt.FlowLayout;
import java.awt.Dimension;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

// Manipulaci�n fechas JAVA LocalDate tipo de datos
// JCalendar

public class PanelIntegrante extends JPanel {
	private JTextField cedulaField;
	private JTextField nameField;
	private JComboBox<EnumRolesIntegrante> rolSelect;
	private IntegranteDT _integrante;
	private boolean crear_usuario;
	
	public PanelIntegrante(IntegranteDT integrante) {
		this._integrante = integrante;
		String textBtn = "Guardar";
		String textTitle;
		if (_integrante == null) {
			textTitle = "Crear";
		} else {
			textTitle = "Guardar";
		}
		if(_integrante != null) {
			System.out.println("INTEGRANTE CEDULA:: " + _integrante.get_cedula() );
			cedulaField = new JTextField(String.valueOf(_integrante.get_cedula()));
			nameField = new JTextField(_integrante.get_name());
			cedulaField.setEnabled(false);
		} else {
			textBtn = "CREAR";
			cedulaField = new JTextField();
			nameField = new JTextField();
		}

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JPanel panel = new JPanel();
		add(panel);
		
		JLabel label = new JLabel("Gestor de equipos - Eduardo Airaudo");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Dialog", Font.BOLD, 18));
		panel.add(label);
		
		JPanel panel_1 = new JPanel();
		add(panel_1);
		
		JLabel lblIntegrantes = new JLabel(textTitle);
		lblIntegrantes.setHorizontalAlignment(SwingConstants.CENTER);
		lblIntegrantes.setFont(new Font("Dialog", Font.BOLD, 40));
		panel_1.add(lblIntegrantes);
		
		JPanel panel_2 = new JPanel();
		add(panel_2);
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 15));
		
		JLabel label_2 = new JLabel("C�dula");
		label_2.setPreferredSize(new Dimension(120, 25));
		label_2.setHorizontalAlignment(SwingConstants.RIGHT);
		label_2.setFont(new Font("Segoe UI", Font.PLAIN, 22));
		panel_2.add(label_2);
		
		cedulaField.setHorizontalAlignment(SwingConstants.LEFT);
		cedulaField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		cedulaField.setColumns(15);
		panel_2.add(cedulaField);
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		panel_3.setLayout(new FlowLayout(FlowLayout.LEFT, 20, 5));
		
		JLabel lblRol = new JLabel("Rol");
		lblRol.setPreferredSize(new Dimension(120, 25));
		lblRol.setHorizontalAlignment(SwingConstants.RIGHT);
		lblRol.setFont(new Font("Segoe UI", Font.PLAIN, 22));
		panel_3.add(lblRol);
		
		rolSelect = new JComboBox<EnumRolesIntegrante>();
		rolSelect.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel_3.add(rolSelect);
		
		JPanel panel_4 = new JPanel();
		add(panel_4);
		panel_4.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 15));
		
		JLabel label_3 = new JLabel("Nombre");
		label_3.setPreferredSize(new Dimension(120, 25));
		label_3.setHorizontalAlignment(SwingConstants.RIGHT);
		label_3.setFont(new Font("Segoe UI", Font.PLAIN, 22));
		panel_4.add(label_3);
		
		nameField.setHorizontalAlignment(SwingConstants.LEFT);
		nameField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		nameField.setColumns(15);
		panel_4.add(nameField);
		
		JPanel panel_5 = new JPanel();
		add(panel_5);
		JButton btnEditar = new JButton("GUARDAR");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ViewController instance = ViewController.getInstance();
				String cedulaString = cedulaField.getText();
				String nombre = nameField.getText();
				if(cedulaString.isEmpty()) {
					instance.showAlert("Falta C�dula");
				} else if (nombre.isEmpty()) {
					instance.showAlert("Falta Nombre");
				} else {		    		
					int cedula = Integer.parseInt(cedulaString);
					EnumRolesIntegrante rol = (EnumRolesIntegrante)rolSelect.getSelectedItem();
					EnumEstadosItem status;
					if(_integrante == null) {
						status = EnumEstadosItem.ACTIVO;
					} else {
						status = _integrante.get_status();
					};
					IntegranteDT integrante = new IntegranteDT(cedula,nombre,rol,status);
					if(_integrante != null) {
						boolean update = instance.editarIntegrante(integrante);
						if(update) {
							instance.showAlert("Integrante editado con �xito");
						} else {
							instance.showAlert("Error al editar integrante");
						}
					} else {
						boolean create = instance.crearIntegrante(integrante);
						if(create) {
							instance.showAlert("Integrante creado con �xito");
						} else {
							instance.showAlert("Integrante ya existe");
						}
					}
				}
			}
		});
		panel_5.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		btnEditar.setFont(new Font("Segoe UI", Font.BOLD, 23));
		panel_5.add(btnEditar);
		btnEditar.setPreferredSize(new Dimension(280,30));
		
		JPanel panel_6 = new JPanel();
		add(panel_6);
		panel_6.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnVolverAIntegrantes = new JButton("VOLVER A INTEGRANTES");
		btnVolverAIntegrantes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ViewController.getInstance().mostrarPanelIntegrantes(1);
			}
		});
		panel_6.add(btnVolverAIntegrantes);
		btnVolverAIntegrantes.setFont(new Font("Segoe UI", Font.BOLD, 15));
		btnVolverAIntegrantes.setPreferredSize(new Dimension(280,30));
		
		this.addRolesToSelect();
	}
	
	public PanelIntegrante() {
		this(null);
	}
	
	private void addRolesToSelect() {
		DefaultComboBoxModel<EnumRolesIntegrante> noAdmin = new DefaultComboBoxModel<>();
		noAdmin.addElement(EnumRolesIntegrante.DIRECTOR_TECNICO);
		noAdmin.addElement(EnumRolesIntegrante.JUGADOR);
		rolSelect.setModel(noAdmin);
		if(_integrante != null) {
			rolSelect.setSelectedItem(_integrante.get_rol());
		}
	}
}
