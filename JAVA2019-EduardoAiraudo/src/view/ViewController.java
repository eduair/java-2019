package view;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;

import datatypes.EquipoDT;
import datatypes.IntegranteDT;
import datatypes.PartidoDT;
import datatypes.UserDT;
import enums.EnumCrearPartido;
import enums.EnumEstadosItem;
import enums.EnumRolesUsuario;
import interfaces.IViewController;
import main.Factory;
import view.ventanas.VentanaUnica;

public class ViewController implements IViewController {
	private static ViewController _instance;
	private VentanaUnica _ventanaUnica;
	private final int CANT_PER_PAGINA = 10;
	
	public int getPages() {
		return CANT_PER_PAGINA;
	}
	
	public static ViewController getInstance()
	{
		if(_instance == null)
		{
			_instance = new ViewController();
		}	
		return _instance;
	}
	
	private ViewController()
	{
		System.out.println("Creando instancia de ViewController");
	}
	
	public void inicializarVista()
	{
		System.out.println("Inicializando vista");
				
		_ventanaUnica = new VentanaUnica();
		_ventanaUnica.setVisible(true);
		_ventanaUnica.addPanel("panelLogin");
		//_ventanaUnica.addPanel("panelPartido");
	}
	
	private boolean dontLoadMenuAgain;
	public void showMenu(UserDT usuario) {
		if(!dontLoadMenuAgain) {
			EnumRolesUsuario rol_usuario = usuario.getRol();
			_ventanaUnica.mostrarPerfil();
			switch (rol_usuario) {
			case ADMINISTRADOR:
				_ventanaUnica.mostrarMenuGestionUsuario(true);
				_ventanaUnica.mostrarGestionarIntegrantes(true);
				_ventanaUnica.mostrarMenuGestionarEquipos();
				_ventanaUnica.mostrarMenuGestionarCampeonatos();
				break;
			case GESTOR_EQUIPOS:
				_ventanaUnica.mostrarMenuGestionUsuario(false);
				_ventanaUnica.mostrarGestionarIntegrantes(true);
				_ventanaUnica.mostrarMenuGestionarEquipos();
				_ventanaUnica.mostrarMenuGestionarCampeonatos();
				break;	
			default:
				break;
			}
			_ventanaUnica.reloadVentana();
			dontLoadMenuAgain = true;
		}
	}

	public void login(UserDT usuario) {
		System.out.println(usuario.getEmail());
		boolean loginSuccess = Factory.getLogicCtrl().login(usuario);
		if(loginSuccess) {
			usuario = Factory.getLogicCtrl().getUsuarioLogueado(usuario);
			System.out.println("USUARIO " + usuario);
			this.showMenu(usuario);
			this.perfil();
		}else {
			String message = "FAIL";
			_ventanaUnica.showAlert(message);
		}
		
		
	}
	
	public LocalDate convertToLocalDate(java.util.Date date_creacion) {
		return Factory.getLogicCtrl().convertToLocalDate(date_creacion);
	}
	
	public boolean testRegexEmail(String emailStr) {
		boolean validateEmail =  Factory.getLogicCtrl().validateEmail(emailStr);
		return validateEmail;
	}

	public void showAlert(String email) {
		_ventanaUnica.showAlert(email);
	}

	public EnumCrearPartido onCreatePartido(PartidoDT partido) {
		return Factory.getLogicCtrl().crearPartidoPedido(partido);
	}

	public void showErrorEmail(boolean b) {
		_ventanaUnica.showErrorEmail(b);
	}

	public void logout() {
		Factory.getLogicCtrl().logout();
		_ventanaUnica.ocultarMenu();
		dontLoadMenuAgain = false;
		_ventanaUnica.addPanel("panelLogin");
	}

	public void perfil() {
		UserDT usuario_logueado = Factory.getLogicCtrl().getUsuarioLogueado();
		_ventanaUnica.addPanel("panelUsuario",usuario_logueado,true);
	}
	
	public void infoUsuario(UserDT usuario) {
		_ventanaUnica.addPanel("panelUsuario",usuario,false);
	}

	public boolean editarUsuario(UserDT usuario) {
		String newclave = usuario.getClave();
		System.out.println(newclave);
		return Factory.getLogicCtrl().editarUsuario(usuario);
	}

	public void mostrarCrearUsuario() {
		_ventanaUnica.addPanel("panelUsuario",null,false);
	}

	public boolean crearUsuario(UserDT usuario) {
		return Factory.getLogicCtrl().crearUsuario(usuario);
	}


	public void mostrarPanelUsuarios(int page) {
		_ventanaUnica.addPanel("panelUsuarios");
		ArrayList<UserDT> usuarios = Factory.getLogicCtrl().getUsuarios(true, page,CANT_PER_PAGINA);
		_ventanaUnica.mostrarInfoUsuarios(usuarios,true);
	}


	public void mostrarBajasSolicitadasUsuario(int page) {
		_ventanaUnica.addPanel("panelUsuarios");
		ArrayList<UserDT> usuarios = Factory.getLogicCtrl().getUsuarios(false, page,CANT_PER_PAGINA);
		_ventanaUnica.mostrarInfoUsuarios(usuarios,false);
	}

	public void usuarioBaja(UserDT usuario, int newPage) {
		boolean resultBaja = Factory.getLogicCtrl().usuarioBaja(usuario);
		if(resultBaja) {
			cambiarPagina("panelUsuarios", true, newPage);
		} else {
			this.showAlert("Error al dar usuario de baja");
		}
	}

	public void usuarioDelete(UserDT usuario, int newPage) {
		boolean resultBaja = Factory.getLogicCtrl().usuarioDelete(usuario);
		if(resultBaja) {
			cambiarPagina("panelUsuarios", false, newPage);
		} else {
			this.showAlert("Error al borrar usuario");
		}
		
	}

	public void restaurar(UserDT usuario, int newPage) {
		boolean resultBaja = Factory.getLogicCtrl().restaurarUsuario(usuario);
		if(resultBaja) {
			cambiarPagina("panelUsuarios", false, newPage);
		} else {
			this.showAlert("Error al restaurar usuario");
		}
	}

	public void cambiarPagina(String string, boolean status, int page) {
		switch (string) {
			case "panelUsuarios":
				ArrayList<UserDT> usuarios;
				usuarios = Factory.getLogicCtrl().getUsuarios(status,page,CANT_PER_PAGINA);
				System.out.println(usuarios.size());
				_ventanaUnica.mostrarInfoUsuarios(usuarios,status);
				break;
			default:
				break;
			}
		_ventanaUnica.reloadVentana();
	};
	
	public void cambiarPagina(String string, EnumEstadosItem status, int page, IntegranteDT filtro) {
		ArrayList<IntegranteDT> integrantes;
		integrantes = Factory.getLogicCtrl().getIntegrantes(status,page,CANT_PER_PAGINA,filtro);
		System.out.println(integrantes.size());
		_ventanaUnica.mostrarInfoIntegrantes(integrantes,status);
	}
	
	public int countUsers(boolean status) {
		int count = Factory.getLogicCtrl().countUsers(status);
		return count;
	}

	public void mostrarCrearIntegrante() {
		_ventanaUnica.addPanel("panelIntegrante");
	}

	public void mostrarBajasSolicitadasIntegrante(int page) {
		_ventanaUnica.addPanel("panelIntegrantes");
		ArrayList<IntegranteDT> integrantes = Factory.getLogicCtrl().getIntegrantes(EnumEstadosItem.BAJA_SOLICITADA, page,CANT_PER_PAGINA,null);
		_ventanaUnica.mostrarInfoIntegrantes(integrantes,EnumEstadosItem.BAJA_SOLICITADA);
	}

	public void mostrarPanelIntegrantes(int page) {
		_ventanaUnica.addPanel("panelIntegrantes");
		ArrayList<IntegranteDT> integrantes = Factory.getLogicCtrl().getIntegrantes(EnumEstadosItem.ACTIVO, page,CANT_PER_PAGINA,null);
		_ventanaUnica.mostrarInfoIntegrantes(integrantes,EnumEstadosItem.ACTIVO);
	}

	public void infoIntegrante(IntegranteDT integrante) {
		_ventanaUnica.addPanelIntegrante(integrante);
	}

	public void IntegranteBaja(IntegranteDT integrante, int newPage,IntegranteDT filtro) {
		boolean resultBaja = Factory.getLogicCtrl().integranteBaja(integrante);
		if(resultBaja) {
			cambiarPagina("panelIntegrantes", EnumEstadosItem.ACTIVO, newPage,filtro);
		} else {
			this.showAlert("Error al dar integrante de baja");
		}
	}

	public void IntegranteDelete(IntegranteDT integrante, int newPage, IntegranteDT filtro) {
		boolean resultBaja = Factory.getLogicCtrl().integranteDelete(integrante);
		if(resultBaja) {
			cambiarPagina("panelIntegrantes", EnumEstadosItem.BAJA_LOGICA, newPage,filtro);
		} else {
			this.showAlert("Error al borrar integrante");
		}
		
	}

	public double countIntegrantes(EnumEstadosItem _status, IntegranteDT filter) {
		int count = Factory.getLogicCtrl().countIntegrantes(_status,filter);
		return count;
	}

	public void restaurar(IntegranteDT integrantes, int newPage, IntegranteDT filtro) {
		boolean resultBaja = Factory.getLogicCtrl().restaurarIntegrante(integrantes);
		if(resultBaja) {
			cambiarPagina("panelIntegrantes", EnumEstadosItem.BAJA_LOGICA, newPage,filtro);
		} else {
			this.showAlert("Error al restaurar integrante");
		}
	}

	public boolean crearIntegrante(IntegranteDT integrante) {
		return Factory.getLogicCtrl().crearIntegrante(integrante);
	}

	public boolean editarIntegrante(IntegranteDT integrante) {
		return Factory.getLogicCtrl().editarIntegrante(integrante);
	}

	public void aplicarFiltroIntegrante(EnumEstadosItem status,int newPage, IntegranteDT filter) {
		cambiarPagina("panelIntegrantes", status, newPage, filter);
	}

	public void mostrarBajasLógicasIntegrante(int page) {
		_ventanaUnica.addPanel("panelIntegrantes");
		ArrayList<IntegranteDT> integrantes = Factory.getLogicCtrl().getIntegrantes(EnumEstadosItem.BAJA_LOGICA, page,CANT_PER_PAGINA,null);
		_ventanaUnica.mostrarInfoIntegrantes(integrantes,EnumEstadosItem.BAJA_LOGICA);
	}

	public void mostrarCrearEquipo() {
		_ventanaUnica.addPanel("panelCrearEquipo");	
	}

	public void setDefaultBtn(JButton btn) {
		_ventanaUnica.setDefaultBtn(btn);
		
	}

	public boolean createEquipo(EquipoDT equipo) {
		return Factory.getLogicCtrl().createEquipo(equipo);
	}

	public void mostrarBajasLógicasEquipo(int page) {
		_ventanaUnica.addPanel("panelEquipos");
		ArrayList<EquipoDT> equipos = Factory.getLogicCtrl().getEquipos(EnumEstadosItem.BAJA_LOGICA, page,CANT_PER_PAGINA,null);
		_ventanaUnica.mostrarInfoEquipos(equipos,EnumEstadosItem.BAJA_LOGICA);
	}

	public void mostrarPanelEquipos(int page) {
		_ventanaUnica.addPanel("panelEquipos");
		ArrayList<EquipoDT> equipos = Factory.getLogicCtrl().getEquipos(EnumEstadosItem.ACTIVO, page,CANT_PER_PAGINA,null);
		_ventanaUnica.mostrarInfoEquipos(equipos,EnumEstadosItem.ACTIVO);
		
	}

	public void aplicarFiltroEquipo(EnumEstadosItem status, int newPage, EquipoDT filter) {
		cambiarPagina("panelEquipos", status, newPage, filter);
		
	}

	public void cambiarPagina(String string, EnumEstadosItem status, int page, EquipoDT filtro) {
		ArrayList<EquipoDT> equipos;
		equipos = Factory.getLogicCtrl().getEquipos(status,page,CANT_PER_PAGINA,filtro);
		System.out.println(equipos.size());
		_ventanaUnica.mostrarInfoEquipos(equipos,status);
		
	}

	public void infoEquipo(EquipoDT equipo) {
		_ventanaUnica.addPanelEquipo(equipo);
	};

	public void restaurar(EquipoDT equipo, int newPage, EquipoDT filtro) {
		boolean resultBaja = Factory.getLogicCtrl().restaurarEquipo(equipo);
		if(resultBaja) {
			cambiarPagina("panelEquipos", EnumEstadosItem.BAJA_LOGICA, newPage,filtro);
		} else {
			this.showAlert("Error al restaurar equipo");
		}	
	}

	public void EquipoBaja(EquipoDT equipo, int newPage, EquipoDT filtro) {
		boolean resultBaja = Factory.getLogicCtrl().equipoBaja(equipo);
		if(resultBaja) {
			cambiarPagina("panelEquipos", EnumEstadosItem.ACTIVO, newPage,filtro);
		} else {
			this.showAlert("Error al dar equipo de baja");
		}
		
	}

	public void EquipoDelete(EquipoDT equipo, int newPage, EquipoDT filtro) {
		boolean resultBaja = Factory.getLogicCtrl().equipoDelete(equipo);
		if(resultBaja) {
			cambiarPagina("panelIntegrantes", EnumEstadosItem.BAJA_LOGICA, newPage,filtro);
		} else {
			this.showAlert("Error al borrar equipo");
		}
	}

	public double countEquipos(EnumEstadosItem _status, EquipoDT filter) {
		int count = Factory.getLogicCtrl().countEquipos(_status,filter);
		return count;
	}

	public void editarMiembrosEquipo(EquipoDT equipo) {
		_ventanaUnica.addPanelIntegrantesEquipo(equipo);
		ArrayList<IntegranteDT> integrantesSinEquipo = Factory.getLogicCtrl().getIntegranteSinEquipo();
		ArrayList<IntegranteDT> integrantesEquipo = Factory.getLogicCtrl().getIntegranteEquipo(equipo);
		_ventanaUnica.loadIntegrantesEquipo(integrantesEquipo);
		_ventanaUnica.loadIntegrantesSinEquipo(integrantesSinEquipo);
		_ventanaUnica.reloadVentana();
	}

	public void agregarIntegranteEquipo(IntegranteDT integrante, EquipoDT equipo, boolean agregar) {
		boolean integranteAgregado = Factory.getLogicCtrl().agregarIntegranteEquipo(integrante, equipo, agregar);
		this.editarMiembrosEquipo(equipo);
	}
}