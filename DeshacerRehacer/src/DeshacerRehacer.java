import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.Stack;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class DeshacerRehacer extends JFrame {

	private JPanel contentPane;
	private Stack<DTEstado> pilaDeshacer;
	private Stack<DTEstado> pilaRehacer;
	private JTextField txtActual;
	private JList<String> lstDeshacer;
	private JList<String> lstRehacer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeshacerRehacer frame = new DeshacerRehacer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DeshacerRehacer() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 600, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new FlowLayout());
		setContentPane(contentPane);
		
		
		/**
		 * Creaci�n de las pilas para deshacer / rehacer.
		 */
		
		pilaDeshacer = new Stack<DTEstado>();
		pilaRehacer = new Stack<DTEstado>();
		
		
		
		/**
		 * Creaci�n de la interfaz gr�fica.
		 */
		DefaultListModel<String> modelo = new DefaultListModel<String>();
		
		lstRehacer = new JList<String>();
		lstRehacer.setModel(modelo);
		JScrollPane scrollPane = new JScrollPane(lstRehacer);
		scrollPane.setPreferredSize(new Dimension(200,500));
		add(scrollPane);
		
		
		txtActual = new JTextField();
		txtActual.setColumns(10);
		add(txtActual);

		DefaultListModel<String> modelo2 = new DefaultListModel<String>();
		
		lstDeshacer = new JList<String>();
		lstDeshacer.setModel(modelo2);
		JScrollPane scrollPane2 = new JScrollPane(lstDeshacer);
		scrollPane2.setPreferredSize(new Dimension(200,500));
		add(scrollPane2);
		
		JButton btnGuardarEstado = new JButton("Guardar estado");
		btnGuardarEstado.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				guardarEstadoActual();
				
			}
		});
		add(btnGuardarEstado);
		
		JButton btnDeshacer = new JButton("Deshacer");
		btnDeshacer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				deshacer();
				
			}
		});
		add(btnDeshacer);
		
	}

	protected void deshacer() 
	{
		DTEstado estadoDeshecho = pilaDeshacer.pop();
		
		
		pilaRehacer.push(obtenerEstadoActual());
		
		refrescarListas();
	}

	private void aplicarEstadoActual(DTEstado estadoDeshecho) 
	{
		txtActual.setText(estadoDeshecho.getFrase());
		
	}

	private DTEstado obtenerEstadoActual() 
	{
		return new DTEstado(txtActual.getText());
	}

	protected void guardarEstadoActual() 
	{
		DTEstado estadoActual = obtenerEstadoActual();
		
		pilaDeshacer.push(estadoActual);
		
		refrescarListas();
	}

	private void refrescarListas() 
	{
		/**
		 * Refresco lista deshacer.
		 */
		Iterator<DTEstado> it = pilaDeshacer.iterator();
		
		DefaultListModel<String> modelo = (DefaultListModel<String>) lstDeshacer.getModel();
		
		modelo.clear();
		
		
		while (it.hasNext()) 
		{
			DTEstado dtEstado = (DTEstado) it.next();
			modelo.addElement(dtEstado.getFrase());
		}
		
		
		
		/**
		 * Refresco lista rehacer.
		 */
		
		
		it = pilaRehacer.iterator();
		
		modelo = (DefaultListModel<String>) lstRehacer.getModel();
		
		modelo.clear();
		
		
		while (it.hasNext()) 
		{
			DTEstado dtEstado = (DTEstado) it.next();
			modelo.addElement(dtEstado.getFrase());
		}
		
	}

}
