import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;

public class ColoresConJava extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ColoresConJava frame = new ColoresConJava();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ColoresConJava() 
	{
		setUndecorated(true);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setOpacity(0.9f);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new FlowLayout());
		setContentPane(contentPane);
		
		
		JColorChooser selectorDeColores = new JColorChooser();
		add(selectorDeColores);
		
		JLabel lblAColorear = new JLabel("Color�enme");
		lblAColorear.setOpaque(true);
		add(lblAColorear);
		
		JButton btnMostrarColorSeleccionado = new JButton("Mostrar color");
		btnMostrarColorSeleccionado.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				System.out.println("Rojo: " + selectorDeColores.getColor().getRed());
				System.out.println("Verde: " + selectorDeColores.getColor().getGreen());
				System.out.println("Azul: " + selectorDeColores.getColor().getBlue());
				System.out.println("Alpha (antitransparencia): " + selectorDeColores.getColor().getAlpha());
				
				lblAColorear.setBackground(selectorDeColores.getColor());
				
				
			}
		});
		add(btnMostrarColorSeleccionado);
		
		
		JButton btnCambiarColorTexto = new JButton("Cambiar color de texto");
		btnCambiarColorTexto.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				lblAColorear.setForeground(selectorDeColores.getColor());
				
				
			}
		});
		add(btnCambiarColorTexto);
		
		agregarAtajos();
		
		getRootPane().setDefaultButton(btnCambiarColorTexto);
		
		pack();
	}

	private void agregarAtajos() 
	{
		String texto = "shift+5";
		((JPanel) getContentPane()).getInputMap().put(KeyStroke.getKeyStroke("BACK_SPACE"), texto);
		((JPanel) getContentPane()).getActionMap().put(texto, new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				System.out.println("Algo pas�");
			}
		});
		
	}

}
