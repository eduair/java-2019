import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Clase0819 extends JFrame {
// Cada vez que presione un bot�n, mostrar el siguiente n�mero m�ltiplo de 1
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	int _numQueVeoTodosLados = 10;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Clase0819 frame = new Clase0819();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Clase0819() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnHacerAlgo = new JButton("Hacer algo");
		btnHacerAlgo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// aumenta valor inicial en 1 cada vez que se presiona el bot�n
				System.out.println("Bot�n Presionado " + _numQueVeoTodosLados);
				_numQueVeoTodosLados++;
			}
		});
		btnHacerAlgo.setBounds(10, 23, 98, 30);
		contentPane.add(btnHacerAlgo);
	}
}
