
public class ClaseSegunda {

	public static void main(String[] args) {
		int numeroAIngresar = 42;
		boolean esMultiploDe2 = (numeroAIngresar % 2 == 0);
		boolean esMultiploDe7 = (numeroAIngresar % 7 == 0);
		boolean ambas = (esMultiploDe2 && esMultiploDe7);
		
		if((esMultiploDe2 || esMultiploDe7) && !ambas) {
			System.out.println(numeroAIngresar + " cumple una condici�n (Es m�ltiplo de 7 o de 2)");
			System.out.println("M�ltiplo de 7 " + esMultiploDe7);
			System.out.println("M�ltiplo de 2 " + esMultiploDe2);
		} else if (ambas) {
			System.out.println(numeroAIngresar + " cumple ambas condiciones (Es m�ltiplo de 7 y de 2)");
		} else {
			System.out.println(numeroAIngresar + "No cumple ninguna");
		}
	}

}
