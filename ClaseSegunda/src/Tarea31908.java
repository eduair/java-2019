import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
//Adivinar si son iguales, menor o mayor si no son iguales al apretar el bot�n, mostrar en consola
// Se registra la cantidad de intentos y cuando VERY HARD intenta adivinar 6 veces pierde
public class Tarea31908 extends JFrame {

	private JPanel contentPane;
	
	int _numeroGlobal = 10;
	int _numeroAAdivinar = 1;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tarea31908 frame = new Tarea31908();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Tarea31908() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnIncrementar = new JButton("Incrementar");
		btnIncrementar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_numeroGlobal++;
			}
		});
		btnIncrementar.setBounds(43, 35, 113, 23);
		contentPane.add(btnIncrementar);
		
		JButton btnDecrementar = new JButton("Decrementar");
		btnDecrementar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_numeroGlobal--;
			}
		});
		btnDecrementar.setBounds(43, 69, 113, 23);
		contentPane.add(btnDecrementar);
		
		JButton btnAdivinar = new JButton("Adivinar");
		btnAdivinar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(_numeroGlobal == _numeroAAdivinar) {
					System.out.println("Adivin�");
				}
			}
		});
		btnAdivinar.setBounds(43, 103, 113, 23);
		contentPane.add(btnAdivinar);
	}

}
