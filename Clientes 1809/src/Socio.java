
public class Socio extends Cliente {
	private int _numeroDeSocio;
	public Socio(int edad, String nombre, int cantidadPerros, int cantidadGatos, int numeroDeSocio) {
		super(edad,nombre,cantidadPerros,cantidadGatos);
		_numeroDeSocio = numeroDeSocio;
	}
	
	public boolean esSocio() {
		return true;
	}
	
	public int getNumSocio () {
		return _numeroDeSocio;
	}
	
}
