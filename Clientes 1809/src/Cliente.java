
public class Cliente {
	private int _edad;
	private String _nombre;
	private static int cantClientes = 0;
	private int _cantidadPerros;
	private int _cantidadGatos;
	private int _numeroDeSocio;
	private static final int NO_SOCIO = -1;
	public Cliente(int edad, String nombre, int cantidadPerros, int cantidadGatos) {
		this._edad = edad;
		this._nombre = nombre;
		this._cantidadPerros = cantidadPerros;
		this._cantidadGatos = cantidadGatos;
		cantClientes++;
	}
	
	/*public Cliente(int edad, String nombre, int cantidadPerros, int cantidadGatos) {
		this(edad,nombre,cantidadPerros,cantidadGatos,NO_SOCIO);
		System.out.println("Creando No Socio");
	}*/
	
	// Sobrecarga, misma firma pero diferente cantidad de parámetros
	
	public int getEdad() {
		return _edad;
	}
	
	public boolean esMayor(Cliente clienteMayor) {
		return (this._edad > clienteMayor.getEdad());
	};
	
	 public boolean másMascotasQue(Cliente cliente) {
		return (this.get_cantidadMascotas() > cliente.get_cantidadMascotas());
	}
	 
	 public boolean esSocio() {
		return false; 
	 }
	 
	
	
	public int get_cantidadMascotas() {
		return _cantidadPerros + _cantidadGatos;
	}

	static public Cliente obtenerMayorCliente(Cliente[] clientes) {
		Cliente clienteMayor = null;
		Cliente client = null;
		for (int i = 0; i < clientes.length; i++) {
			client = clientes[i];
			if(clienteMayor == null) {clienteMayor = client;}
			else if(client != null && client.esMayor(clienteMayor)) {
				clienteMayor = client;
			}
		}
		return clienteMayor;
	}
	
	/**
	 *  Devuelve la edad del mayor cliente
	 * @param clientes
	 * @return int edad con la mayor edad
	 */
	static public int obtenerEdadDelMayorCliente(Cliente[] clientes) {
		Cliente mayorCliente = Cliente.obtenerMayorCliente(clientes);
		int edadMayor = mayorCliente.getEdad();
		return edadMayor;
	}
	
	
	/* ALT SHIFT R puedo modificar las diferentes variables dentro de una función*/
	static public Cliente obtenerElMásBichero(Cliente[] clientes) {
		Cliente clienteMayor = null;
		Cliente client = null;
		for (int i = 0; i < clientes.length; i++) {
			client = clientes[i];
			if(clienteMayor == null) {clienteMayor = client;}
			else if(client != null && client.másMascotasQue(clienteMayor)) {
				clienteMayor = client;
			}
		}
		return clienteMayor;
	}
	
	static public int cantidadClientes() {
		return Cliente.cantClientes;
	}

	@Override
	public String toString() {
		String printX;
		if (_numeroDeSocio != -1) {
			printX =  " Número de Socios: " + _numeroDeSocio;
		} else {
			printX =  " No es socio";
		}
		String resultado = "Nombre : " + _nombre + " Edad : " + _edad + " Perros : " + _cantidadPerros;
		String resultado2 = " Gatos: " + _cantidadGatos + printX;
		resultado = resultado + " " + resultado2;
		return resultado;
	}
	
	// ((Socio) soc).getNúmeroSocio();
	
	
}
