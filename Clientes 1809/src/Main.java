import java.util.Random;

public class Main {
	private static Random rand = new Random();
	public static void main(String[] args) {
		Cliente[] clientes = new Cliente[20];
				
		int edad;
		int cantidadPerros;
		int cantidadGatos;
		int numeroDeSocio;
		int rangoSuperiorNum = 2;
		int rangoInferiorNum = -1;
		for (int i = 0; i < clientes.length - 1; i++) {
			edad = rand.nextInt(25) + 15;
			cantidadPerros =  rand.nextInt(5);
			cantidadGatos =  rand.nextInt(4);
			numeroDeSocio = rand.nextInt((rangoSuperiorNum - rangoInferiorNum)+1) + rangoInferiorNum;
			String nombre;
			if (numeroDeSocio == -1) {
				nombre = "Pepe";
				clientes[i] = new Cliente(edad,nombre,cantidadPerros,cantidadGatos);
			} else {
				nombre = "Carlos";
				clientes[i] = new Socio(edad,nombre,cantidadPerros,cantidadGatos,numeroDeSocio);
			}
			String printCliente = clientes[i].toString();
			boolean socio = clientes[i].esSocio();
			System.out.println(printCliente + " " + socio);
		}
		
		int edadMayor = Cliente.obtenerEdadDelMayorCliente(clientes);
		Cliente mayor = Cliente.obtenerMayorCliente(clientes);
		String infoCliente = mayor.toString();
		System.out.println(" ");
		System.out.println("Mayor Cliente");
		System.out.println(infoCliente);
		Cliente masBichero = Cliente.obtenerElM�sBichero(clientes);
		String infoMasBichero = masBichero.toString();
		System.out.println(" ");
		System.out.println("M�s Bichero");
		System.out.println(infoMasBichero);
		System.out.println(Cliente.cantidadClientes());

	}
	
	public static void SoyMain() {
		System.out.println("Cargando usuario");
	}
	
	// UML + public - privado. Cliente, Socio. Dise�o UML para hacer el esquema del ABM.
	/* Socio socio = new Socio();
	 * Cliente cli = new Cliente(); */

}
