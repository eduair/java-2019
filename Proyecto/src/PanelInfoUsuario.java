import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.BoxLayout;

public class PanelInfoUsuario extends JPanel {
	private JTextField textUser;
	private JPasswordField passwordField;
	private JTextField textCedula;
	public PanelInfoUsuario() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JPanel panel = new JPanel();
		add(panel);
		
		JLabel lblInformacionUsuario = new JLabel("Informacion Usuario");
		lblInformacionUsuario.setFont(new Font("Tahoma", Font.PLAIN, 39));
		panel.add(lblInformacionUsuario);
		
		JPanel panel_1 = new JPanel();
		add(panel_1);
		
		JLabel label_1 = new JLabel("Nombre");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_1.add(label_1);
		
		textUser = new JTextField();
		textUser.setColumns(20);
		panel_1.add(textUser);
		
		JPanel panel_2 = new JPanel();
		add(panel_2);
		
		JLabel label_2 = new JLabel("Password");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_2.add(label_2);
		
		passwordField = new JPasswordField();
		passwordField.setColumns(20);
		panel_2.add(passwordField);
		
		//label_1.setPreferredSize(new Dimension(150, 100));
		//label_2.setPreferredSize(new Dimension(150, 100));
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		
		JLabel lblCedula = new JLabel("Cedula");
		lblCedula.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_3.add(lblCedula);
		
		textCedula = new JTextField();
		textCedula.setColumns(20);
		panel_3.add(textCedula);
		
	} 

	public void setUsuLogueado(UsuDT usuActualizado) {
		// TODO Auto-generated method stub
		textUser.setText(usuActualizado.get_nombre());
		passwordField.setText(usuActualizado.get_clave());
		textCedula.setText(Integer.toString(usuActualizado.get_cedulaIdentidad()));
	}
	
}
