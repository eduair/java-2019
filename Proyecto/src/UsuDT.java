
public class UsuDT {
	private String _nombre;
	private String _clave;
	private int _cedulaIdentidad;
	
	public UsuDT(String nombre, String clave, int cedulaIdentidad) {
		this._nombre = nombre;
		this._clave = clave;
		this._cedulaIdentidad = cedulaIdentidad;
		//GENERATE CONTACTO USING FIELDS;
	}
	
	public UsuDT() {
		this("Ricardo", "clave", 123);
	}

	public String get_nombre() {
		return _nombre;
	}

	public String get_clave() {
		return _clave;
	}

	public int get_cedulaIdentidad() {
		return _cedulaIdentidad;
	}
	
	public boolean equalsTo(UsuDT usuario) {
		return usuario.get_nombre().equals(this._nombre) && usuario.get_clave().equals(this._clave);
	}
	
	
	
}
