import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PanelCrearUsuario extends JPanel {
	private JTextField textCedula;
	private JPasswordField passwordField;
	private JTextField userField;
	private int HEIGHTLABEL = 123;
	private int WIDTHLABEL = 123;
	public PanelCrearUsuario() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JPanel panel = new JPanel();
		add(panel);
		
		JLabel lblCrearUsuario = new JLabel("Crear Usuario");
		lblCrearUsuario.setFont(new Font("Tahoma", Font.PLAIN, 39));
		panel.add(lblCrearUsuario);
		
		JPanel panel_1 = new JPanel();
		add(panel_1);
		
		JLabel label_1 = new JLabel("Nombre");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_1.setPreferredSize(new Dimension(WIDTHLABEL, HEIGHTLABEL));
		panel_1.add(label_1);
		
		userField = new JTextField();
		userField.setColumns(20);
		panel_1.add(userField);
		
		JPanel panel_2 = new JPanel();
		add(panel_2);
		
		JLabel label_2 = new JLabel("Password");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_2.setPreferredSize(new Dimension(WIDTHLABEL, HEIGHTLABEL));
		panel_2.add(label_2);
		
		passwordField = new JPasswordField();
		passwordField.setColumns(20);
		panel_2.add(passwordField);
		
		JPanel panel_4 = new JPanel();
		add(panel_4);
		
		JLabel lblCedula = new JLabel("Cedula");
		lblCedula.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCedula.setPreferredSize(new Dimension(WIDTHLABEL, HEIGHTLABEL));
		panel_4.add(lblCedula);
		
		textCedula = new JTextField();
		panel_4.add(textCedula);
		textCedula.setColumns(20);
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		
		JButton btnCrearUsuario = new JButton("Crear Usuario");
		btnCrearUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				UsuDT usu = new UsuDT(userField.getText(),passwordField.getText(),Integer.parseInt(textCedula.getText()));
				ViewController.getInstance().crearUsuario(usu);
			}
		});
		panel_3.add(btnCrearUsuario);
		
		
	}

}
