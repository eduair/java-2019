import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;

public class VentanaUnica extends JFrame {
	String LOGIN_LAYOUT = "Login";
	private String estado_actual = LOGIN_LAYOUT;
	String INFO_USUARIO_LOGUEADO = "UsuarioLog";
	private JPanel contentPane;
	private JPanel panelAuxiliar;
	private PanelLogin _loginPanel;
	private JMenuItem logout;
	private JPanel _panelPrincipal;
	private PanelInfoUsuario panelInfoUsuario;	
	
	public VentanaUnica() {
		getContentPane().setLayout(new BorderLayout(0, 0));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		_panelPrincipal = new JPanel();
		contentPane.add(_panelPrincipal, BorderLayout.CENTER);
		
		panelAuxiliar = new JPanel();
		panelAuxiliar.setLayout(new CardLayout(0,0));
		contentPane.add(panelAuxiliar, BorderLayout.EAST);
		_loginPanel = new PanelLogin();
		panelInfoUsuario = new PanelInfoUsuario();
		panelAuxiliar.add(_loginPanel, LOGIN_LAYOUT);
		panelAuxiliar.setPreferredSize(new Dimension(600, 100));
		panelAuxiliar.add(panelInfoUsuario, INFO_USUARIO_LOGUEADO);
		((CardLayout)(panelAuxiliar.getLayout())).show(panelAuxiliar,LOGIN_LAYOUT);
		setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
		
		JMenuBar menubar = new JMenuBar();
		JMenu menGestionarUsuarios = new JMenu("Gestionar usuario");
		JMenu menNavegacion = new JMenu("Navegaci�n");
		logout = new JMenuItem("Logout");
		logout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				(ViewController.getInstance()).logout();
			}
		});
		logout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, ActionEvent.CTRL_MASK));
		JMenuItem mentItCrearUsuario = new JMenuItem("Crear");
		JMenuItem menItBorrarUsuario = new JMenuItem("Borrar");
		JMenuItem menItVerUsuario = new JMenuItem("Ver");
		menubar.add(menGestionarUsuarios);
		menubar.add(menNavegacion);
		menubar.add(logout);
		logout.setEnabled(false);
		setJMenuBar(menubar);
		menGestionarUsuarios.add(mentItCrearUsuario);
		menGestionarUsuarios.add(menItBorrarUsuario);
		menGestionarUsuarios.add(menItVerUsuario);
		menGestionarUsuarios.setMnemonic('G');
		mentItCrearUsuario.setMnemonic('C');
		
		mentItCrearUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				(ViewController.getInstance()).mostrarCrearUsuario();
			}
		});
		
		menItBorrarUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//(ViewController.getInstance()).mostrarVerUsuario();
				int cedulaInt = VentanaUnica.this.getCedulaFromDialog();
				(ViewController.getInstance()).borrarUsuarioSiExiste(cedulaInt);
			}
		});
		
		
		menItVerUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//(ViewController.getInstance()).mostrarVerUsuario();
				int cedulaInt = VentanaUnica.this.getCedulaFromDialog();
				(ViewController.getInstance()).mostrarUsuarioSiExiste(cedulaInt);
			};
		});
	}
	
	public int getCedulaFromDialog() {
		boolean hayError = true;
		int cedulaInt = 0;
		while(hayError) {
			try {
				String cedula_dialog = JOptionPane.showInputDialog(VentanaUnica.this,"Ingresar C�dula de Identidad");
				if(cedula_dialog.equals(null)) {
					hayError = false;
				}
				cedulaInt = Integer.parseInt(cedula_dialog);
				hayError = false;
			} catch (Exception e) {
				JOptionPane.showMessageDialog(VentanaUnica.this, "Error, se espera un n�mero");
			}
		}
		return cedulaInt;
	}
		
	
	public void mostrarLogin() {
		estado_actual = LOGIN_LAYOUT;
		logout.setEnabled(false);
		_loginPanel.cleanUserPass();
		((CardLayout)(panelAuxiliar.getLayout())).show(panelAuxiliar,estado_actual);
	}
	
	public void mostrarInfoUsuarioLogueado() {
		logout.setEnabled(true);
		estado_actual = INFO_USUARIO_LOGUEADO;
		((CardLayout)(panelAuxiliar.getLayout())).show(panelAuxiliar,estado_actual);
	}
	
	public void mostrarErrorLogin() {
		_loginPanel.cleanUserPass();
		JOptionPane.showMessageDialog(contentPane, "Login Inv�lido");
	}
	
	public void mostrarCrearUsuario(boolean param) {
		if (param == true) {
			_panelPrincipal.removeAll();
			_panelPrincipal.add(new PanelCrearUsuario());
			_panelPrincipal.validate();
		} else {
			_panelPrincipal.removeAll();
			_panelPrincipal.revalidate();
			_panelPrincipal.repaint();
		}
	}

	public void setUsuLogueado(UsuDT usuActualizado) {
		panelInfoUsuario.setUsuLogueado(usuActualizado);
		
	}

	public void mostrarVentana(boolean param,JPanel ventanaVerUsuario) {
		if (param == true) {
			_panelPrincipal.removeAll();
			_panelPrincipal.add(ventanaVerUsuario);
			_panelPrincipal.validate();
		} else {
			_panelPrincipal.removeAll();
			_panelPrincipal.revalidate();
			_panelPrincipal.repaint();
		}
		
	}

	public void cargarUsuarioPorCedula(UsuDT usuarioPorCedula) {
		JPanel ventanaVerUsuario = new PanelVerUsuario(usuarioPorCedula);
		mostrarVentana(true,ventanaVerUsuario);
	}

	public void alertError(String input) {
		JOptionPane.showMessageDialog(VentanaUnica.this, input);
	}
	
}
