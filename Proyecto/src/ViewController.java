import javax.swing.JOptionPane;

/*Singleton: me protege de otras instancias*/
public class ViewController {
	private static ViewController _instance;
	private VentanaUnica _ventanaUnica;
	private ConectarBD _bd = new ConectarBD();
	
	public static ViewController getInstance() {
		
		if(_instance == null) {
			_instance = new ViewController();
		}
		
		return _instance;
	}
	
	private ViewController() 
	{
		System.out.println("Creando instancia de ViewController");
	}
	
	public void inicializarVista() {
		_ventanaUnica = new VentanaUnica(); // ALT + SHIFT + T - Convert Local Var to Field
		_ventanaUnica.setVisible(true);
		System.out.println("Inicializando Vista");
	}
	
	public void validarUsuario(UsuDT usuario) {
		if(_bd.validarUsu(usuario)) {
			_ventanaUnica.mostrarInfoUsuarioLogueado();
			UsuDT usuActualizado = _bd.getUsuarioPorNombre(usuario.get_nombre());
			_ventanaUnica.setUsuLogueado(usuActualizado);
		} else {
			_ventanaUnica.mostrarErrorLogin();
		}
	}
	
	public void crearUsuario(UsuDT usu) {
		boolean creado = _bd.crearUsuario(usu);
		if (creado == true) {
			JOptionPane.showMessageDialog(null, "User created successfully");
		}
	}
	
	public void logout() {
		System.out.println("Logout");
		_ventanaUnica.mostrarLogin();
		_ventanaUnica.mostrarCrearUsuario(false);
	}
	
	public void mostrarCrearUsuario() {
		_ventanaUnica.mostrarCrearUsuario(true);
	}

	public void mostrarUsuarioSiExiste(int cedula) {
		boolean validacion = _bd.validarCedula(cedula);
		if (validacion) {
			UsuDT usuarioPorCedula = _bd.getUsuarioPorCedula(cedula);
			_ventanaUnica.cargarUsuarioPorCedula(usuarioPorCedula);
		} else {
			String input = "No se encontr� usuario";
			_ventanaUnica.alertError(input);
		}
	}

	public void borrarUsuarioSiExiste(int cedula) {
		// TODO Auto-generated method stub
		boolean validacion = _bd.validarCedula(cedula);
		if (validacion) {
			boolean borradoUsuarioCedula = _bd.eliminadoUsuarioPorCedula(cedula);
			if(borradoUsuarioCedula == true) {
				String nombreUsuario = _bd.getUsuarioPorCedula(cedula).get_nombre();
				_ventanaUnica.alertError("Se ha borrado el usuario " + nombreUsuario);
			}
		} else {
			String input = "No se encontr� usuario para borrarlo";
			_ventanaUnica.alertError(input);
		}
	}
}
