import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;

public class PanelLogin extends JPanel {
	private JTextField nombreField;
	private JPasswordField passwordField;
	public PanelLogin() {
		setBackground(SystemColor.activeCaption);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JPanel panel = new JPanel();

		
		JLabel lblNewLabel = new JLabel("Login");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 39));
		panel.add(lblNewLabel);
		
		JPanel panel_4 = new JPanel();
		add(panel_4);
		
		JLabel lblNewLabel_2 = new JLabel("Login");
		lblNewLabel_2.setFont(new Font("Sylfaen", Font.PLAIN, 36));
		panel_4.add(lblNewLabel_2);
		
		JPanel panel_1 = new JPanel();
		add(panel_1);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre");
		panel_1.add(lblNewLabel_1);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		nombreField = new JTextField();
		panel_1.add(nombreField);
		nombreField.setColumns(20);

		
		JPanel panel_2 = new JPanel();
		add(panel_2);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_2.add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setColumns(20);
		panel_2.add(passwordField);
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		
		JButton btnLogin = new JButton("LOGIN");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				(ViewController.getInstance()).validarUsuario(new UsuDT(nombreField.getText(),passwordField.getText(),0));
			}
		});
		panel_3.add(btnLogin);
	}
	
	public void cleanUserPass() {
		passwordField.setText("");
		nombreField.setText("");
	}
}
