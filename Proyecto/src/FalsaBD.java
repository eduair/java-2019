
public class FalsaBD {
	private UsuDT usuarios [] = new UsuDT[20];
	
	public FalsaBD() {
		usuarios[0] = new UsuDT("admin", "123", 123);
	}
	
	public boolean validarUsu(UsuDT usuario) {
		int i = 0;
		while (i < usuarios.length && usuarios[i] != null && !usuarios[i].equalsTo(usuario)) {
			i++;	
		}
		return usuarios[i] != null && usuarios[i].equalsTo(usuario);
	}
	
	public boolean crearUsuario (UsuDT usuario) {
		int i = 0;
		while (i < usuarios.length && usuarios[i] != null) {
			i++;	
		}
		if(usuarios[i] == null) {
			usuarios[i] = usuario;
			return true;
		} else {
			System.out.println("BASE DE DATOS LLENA");
			return false;
		}
	}
	

	public boolean guardarUsu(UsuDT usuario) {
		return true;
	}

	public UsuDT getUsuarioPorNombre(String nombre) {
		int i = 0;
		while (i < usuarios.length && !usuarios[i].get_nombre().equals(nombre)){
			i++;
			
		}
		System.out.println(usuarios[i]);
		return usuarios[i];
	}
	
	public UsuDT getUsuarioPorCedula(int cedula) {
		int i = 0;
		while (i < usuarios.length && !(usuarios[i].get_cedulaIdentidad() == cedula)){
			i++;
			
		}
		System.out.println(usuarios[i]);
		return usuarios[i];
	}
	
	public boolean validarCedula(int cedula) {
		int i = 0;
		while (i < usuarios.length && usuarios[i] != null && !(usuarios[i].get_cedulaIdentidad() == cedula)) {
			i++;	
		}
		return usuarios[i] != null && usuarios[i].get_cedulaIdentidad() == cedula;
	}
	
	public boolean eliminadoUsuarioPorCedula(int cedula) {
		int i = 0;
		while (i < usuarios.length && !(usuarios[i].get_cedulaIdentidad() == cedula)){
			i++;		
		}
		usuarios[i] = null;
		if(i < usuarios.length) {
			while (i < (usuarios.length) - 1) {
				usuarios[i] = usuarios[i+1];
				i++;
			}
		}
		return true;
	}
	
	
}
