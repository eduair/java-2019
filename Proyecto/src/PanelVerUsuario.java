import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Dimension;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.BoxLayout;

public class PanelVerUsuario extends JPanel {
	private JTextField textFieldNombre;
	private JPasswordField passwordField;
	private JTextField cedulaField;
	public PanelVerUsuario(UsuDT usuario) {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JPanel panel = new JPanel();
		add(panel);
		
		JLabel lblVerUsuario = new JLabel("Ver Usuario");
		lblVerUsuario.setFont(new Font("Tahoma", Font.PLAIN, 39));
		panel.add(lblVerUsuario);
		
		JPanel panel_1 = new JPanel();
		add(panel_1);
		
		JLabel label_1 = new JLabel("Nombre");
		label_1.setPreferredSize(new Dimension(123, 123));
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_1.add(label_1);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setColumns(20);
		panel_1.add(textFieldNombre);
		
		JPanel panel_2 = new JPanel();
		add(panel_2);
		
		JLabel label_2 = new JLabel("Password");
		label_2.setPreferredSize(new Dimension(123, 123));
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_2.add(label_2);
		
		passwordField = new JPasswordField();
		passwordField.setColumns(20);
		panel_2.add(passwordField);
		
		JPanel panel_3 = new JPanel();
		add(panel_3);
		
		JLabel label_3 = new JLabel("Cedula");
		label_3.setPreferredSize(new Dimension(123, 123));
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_3.add(label_3);
		
		cedulaField = new JTextField();
		cedulaField.setColumns(20);
		panel_3.add(cedulaField);
		
		JPanel panel_4 = new JPanel();
		add(panel_4);
		
		textFieldNombre.setText(usuario.get_nombre());
		passwordField.setText(usuario.get_clave());
		cedulaField.setText(Integer.toString(usuario.get_cedulaIdentidad()));
		
		JButton btnVerificarUsuario = new JButton("Verificar Usuario");
		panel_4.add(btnVerificarUsuario);
	}

}
