package ventana;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

import javax.swing.JEditorPane;
import Main.Main;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import java.awt.Component;
import javax.swing.SwingConstants;
import java.awt.Font;

public class Ventana extends JFrame {

	private JPanel contentPane;

	public Ventana() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		String a�osString = String.valueOf(Main.a�os);
		
		JPanel panel = new JPanel();
		contentPane.add(panel);
				panel.setLayout(null);
				
				JEditorPane editorPane = new JEditorPane();
				contentPane.add(editorPane);
				editorPane.setContentType("text/html");
				
				JScrollPane scrollPane = new JScrollPane(editorPane);
				contentPane.add(scrollPane);
		
				JButton btnNewButton = new JButton("Pasar a\u00F1o");
				btnNewButton.setBounds(46, 90, 91, 23);
				panel.add(btnNewButton);
				btnNewButton.setAlignmentY(Component.TOP_ALIGNMENT);
				
				JButton btnMostrarInfo = new JButton("Mostrar info");
				btnMostrarInfo.setBounds(46, 124, 91, 23);
				panel.add(btnMostrarInfo);
				btnMostrarInfo.setAlignmentX(Component.CENTER_ALIGNMENT);
				JLabel lblA�os = new JLabel("A�os");
				lblA�os.setHorizontalAlignment(SwingConstants.CENTER);
				lblA�os.setFont(new Font("Tahoma", Font.PLAIN, 20));
				lblA�os.setBounds(81, 158, 27, 66);
				panel.add(lblA�os);
				lblA�os.setAlignmentY(Component.TOP_ALIGNMENT);
				lblA�os.setText(a�osString);
				btnMostrarInfo.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String resultado = Main.printMascotas(Main.seresvivos);
						editorPane.setText(resultado);
					}
				});
				btnNewButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						Main.pasaUnA�o(Main.seresvivos);
						String a�osString = String.valueOf(Main.a�os);
						lblA�os.setText(a�osString);
					}
				});
	}
}
