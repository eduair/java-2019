package seresvivos;
import java.util.Random;

public class MariposaLongeva extends SerVivo {
	private int _a�osDeVida = 1;
	private boolean _vivo = false;
	Random rand = new Random();
	private int _cantidadColores = rand.nextInt(5);
	private boolean _rara = false;
	public MariposaLongeva(int edad, int colores) {
		this._cantidadColores = colores;
		this._edad = edad;
		if (colores > 2) {
			this._rara = true;
		}
		if (_edad <= 1) {
			this._vivo = true;
		}
	}
	
	public void reaccionarAA�osTranscurridos (int a�os) {
		this._edad += a�os;
		if (this._edad <= 1) {
			this._vivo = true;
		} else {
			this._vivo = false;
		}
	}
	
	private boolean getRara() {
		return _cantidadColores > 2;
	}
	
	public int getEspecie() {
		return SerVivo.MARIPOSA;
	}
	
	public String getInfo() {
		String Mariposa = "<p><strong>Mariposa Longeva</strong></p>\r\n" + 
				"<ul>\r\n" + 
				"<li>Edad: "+this._edad+"</li>\r\n" + 
				"<li>Colores:&nbsp; "+this._cantidadColores+"</li>\r\n" + 
				"<li>Rara:&nbsp; "+convertToString(getRara())+"</li>\r\n" + 
				"<li>&iquest;Vivo?: "+convertToString(_vivo)+"</li>\r\n" + 
				"</ul>";
		return Mariposa;
	}
	
	private String convertToString(boolean input) {
		if (input == false) {
			return "No";
		} else {
			return "S�";
		}
	}
	
}
