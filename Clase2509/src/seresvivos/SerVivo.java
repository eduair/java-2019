package seresvivos;

public abstract class SerVivo {
	public final static int HUMANO = 1;
	public final static int PERRO = 2;
	public final static int MARIPOSA = 3;
	protected int _edad = 10;
	
	protected static String NOMBRE_CLASE;
	public void reaccionarAAñosTranscurridos (int años) {
		this._edad += años;
	}
	
	public abstract int getEspecie();
	
	public abstract String getInfo();
	
}


//