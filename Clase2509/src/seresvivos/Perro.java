package seresvivos;

public class Perro extends SerVivo {
	private String _raza = "Labrador";
	public Perro(int edad, String raza) {
		this._edad = edad;
		this._raza = raza;
	}
	
	@Override
	public void reaccionarAAñosTranscurridos (int años) {
		this._edad += 7*años;
	}

	public int getEspecie() {
		return SerVivo.PERRO;
	}
	
	public String getInfo() {
		String Perro = "<p><strong>Perro</strong></p>\r\n" + 
				"<ul>\r\n" + 
				"<li>Edad: "+this._edad+"</li>\r\n" + 
				"<li>Raza:&nbsp; "+this._raza+"</li>\r\n" + 
				"</ul>";
		return Perro;
	}
	
	
}
