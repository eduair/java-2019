package seresvivos;

public class Humano extends SerVivo {
	private String _nombre = "Mario";
	private String _genero = "Hombre";
	
	public Humano(int edad, String nombre, String genero) {
		this._nombre = nombre;
		this._edad = edad;
		this._genero = genero;
	}
	
	
	public int getEspecie() {
		return SerVivo.HUMANO;
	}
	
	public String getInfo() {
		String Humano = "<p><strong>Humano</strong></p>\r\n" + 
				"<ul>\r\n" + 
				"<li>Edad: "+this._edad+"</li>\r\n" + 
				"<li>Nombre:&nbsp; "+this._nombre+"</li>\r\n" + 
				"<li>G�nero:&nbsp; "+this._genero+"</li>\r\n" + 
				"</ul>";
		return Humano;
	}
	
}
