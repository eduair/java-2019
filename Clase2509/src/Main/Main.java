package Main;
import seresvivos.Humano;
import seresvivos.MariposaLongeva;
import seresvivos.Perro;
import seresvivos.SerVivo;
import ventana.Ventana;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import javax.swing.BoxLayout;
import java.awt.BorderLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import java.awt.FlowLayout;
import java.awt.CardLayout;

public class Main {

	public static int a�os = 0;
	public static final int UNA�O = 1;

	public static SerVivo [] seresvivos = new SerVivo[6];
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Crear inicialmente dos humanos, dos perros y dos mariposas, 
		//guard�ndolos a todos en un mismo array
		seresvivos[0] = new Humano(10,"Carlos", "Hombre");
		seresvivos[1] = new Humano(10,"Maria", "Mujeres");
		
		seresvivos[2] = new Perro(10,"Labrador");
		seresvivos[3] = new Perro(5,"Aleman");
		
		seresvivos[4] = new MariposaLongeva(0,5);
		seresvivos[5] = new MariposaLongeva(5,1);
		String resultado = printMascotas(seresvivos);
		System.out.println(resultado);
		
		Ventana ven = new Ventana();
		ven.getContentPane().setLayout(new BoxLayout(ven.getContentPane(), BoxLayout.X_AXIS));
		ven.setVisible(true);
		//Pasar a�os
	}
	
	public static void pasaUnA�o(SerVivo [] seresvivos) {
		a�os++;
		for (int i = 0; i < seresvivos.length; i++) {
			seresvivos[i].reaccionarAA�osTranscurridos(UNA�O);
		}
	}
	
	public static String printMascotas(SerVivo [] seresvivos) {
		String info = "";
		for (int i = 0; i < seresvivos.length; i++) {
			if (i == 0) {
				info += seresvivos[i].getInfo();
			} else {
				info += "\n" +  seresvivos[i].getInfo();
			}
		}
		return info;
	}
}

/* Los par�metros de entrada son obligatorios */
/* m�todo final: no puede ser sobrescrito
 * clase final: no puede ser extendida */
