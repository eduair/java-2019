import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;

public class VentanaGanador extends JFrame {

	private JPanel contentPane;

	public VentanaGanador() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(123, 104, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblGan = new JLabel("GAN\u00D3");
		lblGan.setHorizontalAlignment(SwingConstants.CENTER);
		lblGan.setFont(new Font("Tahoma", Font.BOLD, 58));
		lblGan.setForeground(new Color(106, 90, 205));
		lblGan.setBounds(10, 65, 414, 100);
		contentPane.add(lblGan);
	}

}
