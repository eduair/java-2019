import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.SwingConstants;

public class AdivinarJFrame extends JFrame {

	private JPanel contentPane;
	private int _globalValue = 10;
	private int _numeroAdivinar = 5;
	private int _maximoNumIntentos = 5;
	private int numeroIntentos = 0;
	private int intentosRestantes;
	private	JLabel lblLabelMensajeAdivinar = new JLabel("");
	private JLabel lblIntentos = new JLabel("Intentos");
	private JLabel lblIntentosRestantes = new JLabel("Aqu� ver�s tu progreso");
	JButton btnReinciar = new JButton("Comenzar");
	JButton btnNewButton = new JButton("Adivinar");
	private JTextField txtMinrango;
	private JTextField txtMaxrango;
	private int rangoInferiorNum;
	private int rangoSuperiorNum;
	private String mensaje;
	private JButton btnAumentar = new JButton("Aumentar");
	// si quisiera usar "this" dentro del action performed, lo soluciono as�:
	AdivinarJFrame instanciaDeMiMismo = this;
	JButton btnDecrementar = new JButton("Decrementar");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdivinarJFrame frame = new AdivinarJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdivinarJFrame() {
		Scanner scan = new Scanner(System.in);
		Random rand = new Random();
		// Cuando quedan 4 intentos "Tranquile, todavi� hay tiempo"
		// Cuando quedan 3 intentos: "Bueno, �se no sali, el pr�ximo quiz�s aciertes>"
		// Cuando queden 2 " Casi perd�s.."
		// Cuando quede 1 "Fallas y fuiste"
		//System.out.println("Ingresa el rango Inferior");
		//String rangoInferiorStr = scan.nextLine();
		//rangoInferiorNum = Integer.parseInt(rangoInferiorStr);
		
		//System.out.println("Ingresa el rango superior");
		//String rangoSuperiorStr = scan.nextLine();
		//rangoSuperiorNum = Integer.parseInt(rangoSuperiorStr);
		
		//System.out.println("Rango: " + rangoInferiorNum + "-" + rangoSuperiorNum);
		
		//_numeroAdivinar = rand.nextInt(rangoSuperiorNum - rangoInferiorNum) + rangoInferiorNum;
		//_numeroAdivinar = (int )(Math.random() * (rangoSuperiorNum-rangoInferiorNum) + rangoInferiorNum);
		//_globalValue = (int) Math.ceil((rangoSuperiorNum-rangoInferiorNum)/2 + rangoInferiorNum);
		//lblLabelMensajeAdivinar.setText("Empiezas en : " + _globalValue + ", n�mero m�ximo de intentos: " + _maximoNumIntentos);
		//System.out.println("N�mero de adivinar : " + _numeroAdivinar);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		lblIntentos.setHorizontalAlignment(SwingConstants.CENTER);
		lblIntentos.setFont(new Font("Tahoma", Font.PLAIN, 19));
		
		lblIntentos.setBounds(156, 116, 120, 32);
		contentPane.add(lblIntentos);
		lblIntentosRestantes.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		lblIntentosRestantes.setBounds(25, 193, 358, 32);
		contentPane.add(lblIntentosRestantes);
		
		lblIntentos.setText(String.valueOf(_globalValue));
		
		lblLabelMensajeAdivinar.setBounds(25, 11, 399, 26);
		contentPane.add(lblLabelMensajeAdivinar);
		
		txtMinrango = new JTextField();
		txtMinrango.setBounds(297, 63, 86, 20);
		contentPane.add(txtMinrango);
		txtMinrango.setColumns(10);
		
		txtMaxrango = new JTextField();
		txtMaxrango.setBounds(297, 122, 86, 20);
		contentPane.add(txtMaxrango);
		txtMaxrango.setColumns(10);
		
		btnAumentar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_globalValue++;
				lblIntentos.setText(String.valueOf(_globalValue));
				System.out.println("Aumento a " + _globalValue);
				lblLabelMensajeAdivinar.setText("Aumento a " + _globalValue);
			}
		});
		btnAumentar.setBounds(18, 62, 107, 23);
		contentPane.add(btnAumentar);
		
		btnDecrementar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_globalValue--;
				lblIntentos.setText(String.valueOf(_globalValue));
				System.out.println("Decremento a " + _globalValue);
				lblLabelMensajeAdivinar.setText("Decremento a " + _globalValue);
			}
		});
		btnDecrementar.setBounds(18, 96, 107, 23);
		contentPane.add(btnDecrementar);
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(numeroIntentos < _maximoNumIntentos) {
					numeroIntentos++;
					if(_globalValue == _numeroAdivinar) {
						System.out.println("Adivin�");
						lblIntentosRestantes.setText("�Bien, no fallaste!");
						lblLabelMensajeAdivinar.setText("Adivin�");
						habilitarBotones(false);
						VentanaGanador miVentana = new VentanaGanador();
						// Podr�a usar instancia de mi mismo
						instanciaDeMiMismo.setVisible(false);
						miVentana.setVisible(true);
					} else {
						intentosRestantes = _maximoNumIntentos - numeroIntentos;
						if (intentosRestantes == 0) {
							lblLabelMensajeAdivinar.setText("Perdi� por Intentos, el n�mero a adivinar era " + _numeroAdivinar);
							habilitarBotones(false);
						} else {
							if (_globalValue < _numeroAdivinar) {
							lblLabelMensajeAdivinar.setText("Debe aumentar, n�meros de intentos restantes " + intentosRestantes);
							} else if (_globalValue > _numeroAdivinar) {
								lblLabelMensajeAdivinar.setText("Debe decrementar, n�meros de intentos restantes " + intentosRestantes);
							}
						}
						
						switch (intentosRestantes) {
							case 4:
								mensaje = "Tranquile, todavi� hay tiempo";
								break;
							case 3:
								mensaje = "Bueno, �se no sali, el pr�ximo quiz�s aciertes";
								break;
							case 2:
								mensaje = "Casi perd�s..";
								break;
							case 1:
								mensaje = "Fallas y fuiste..";
								break;
							case 0:
								mensaje = "�Fuiste, tendr�s que reiniciar!";
								break;
							default:
								mensaje = "";
								break;
						}
						mostrarEnLblIntentos(mensaje);
					}
				} else {
					lblLabelMensajeAdivinar.setText("Perdi� por Intentos");
				}
			}
			//void = No devuelve nada
			private void mostrarEnLblIntentos(String mensaje) {
				lblIntentosRestantes.setText(mensaje);
			}
		});
		btnNewButton.setBounds(18, 130, 107, 23);
		contentPane.add(btnNewButton);
		habilitarBotones(false);
		btnReinciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnReinciar.getText().equals("Reiniciar")) {
					lblIntentosRestantes.setText("Bien, empecemos de nuevo");
				}
				numeroIntentos = 0;
				if (txtMaxrango.getText().equals("") || txtMinrango.getText().equals("")) {
					lblLabelMensajeAdivinar.setText("Por favor complete los campos correspondientes");
				} else {
					try {
						rangoInferiorNum = Integer.parseInt(txtMinrango.getText());
						rangoSuperiorNum = Integer.parseInt(txtMaxrango.getText());
						if (rangoSuperiorNum <= rangoInferiorNum) {
							lblLabelMensajeAdivinar.setText("El rango inferior no puede ser menor o igual al superior");
						    return;
						}
						_numeroAdivinar = rand.nextInt((rangoSuperiorNum - rangoInferiorNum)+1) + rangoInferiorNum;
						_globalValue = (int) Math.ceil((rangoSuperiorNum-rangoInferiorNum)/2 + rangoInferiorNum);
						lblIntentos.setText(String.valueOf(_globalValue));
						System.out.println(_numeroAdivinar + " nuevo n�mero a adivinar");
						lblLabelMensajeAdivinar.setText("Empiezas en : " + _globalValue + ", n�mero m�ximo de intentos: " + _maximoNumIntentos);
						habilitarBotones(true);
					} catch (Exception h){
						lblLabelMensajeAdivinar.setText("Rompiste el programa, �Gracias!");

					}
				}
			}
		});
		btnReinciar.setBounds(156, 63, 120, 44);
		contentPane.add(btnReinciar);	
		
		JLabel lblNewLabel = new JLabel("Rango M\u00E1ximo");
		lblNewLabel.setBounds(297, 100, 200, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblRangoMnimo = new JLabel("Rango m\u00EDnimo");
		lblRangoMnimo.setBounds(297, 41, 200, 14);
		contentPane.add(lblRangoMnimo);
		
	}

	private void habilitarBotones(boolean habilitar) {
		btnAumentar.setEnabled(habilitar);
		btnDecrementar.setEnabled(habilitar);
		btnNewButton.setEnabled(habilitar);
		if (habilitar == true) {
			btnReinciar.setText("Reiniciar");
		}
		btnReinciar.setEnabled(!habilitar);
	}
}
