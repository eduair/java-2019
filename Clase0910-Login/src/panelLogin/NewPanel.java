package panelLogin;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;

public class NewPanel extends JFrame {

	private JPanel contentPane;

	public NewPanel() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 593, 348);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		PaneLogin paneLogin = new PaneLogin();
		contentPane.add(paneLogin);
		
		JPanel header = new JPanel();
		FlowLayout flowLayout = (FlowLayout) header.getLayout();
		flowLayout.setVgap(5);
		contentPane.add(header, BorderLayout.NORTH);
		
		JPanel footer = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) footer.getLayout();
		flowLayout_1.setVgap(5);
		contentPane.add(footer, BorderLayout.SOUTH);
		
		JPanel siderbar_left = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) siderbar_left.getLayout();
		flowLayout_2.setHgap(10);
		contentPane.add(siderbar_left, BorderLayout.WEST);
		
		JPanel siderbar_right = new JPanel();
		FlowLayout flowLayout_3 = (FlowLayout) siderbar_right.getLayout();
		flowLayout_3.setHgap(10);
		contentPane.add(siderbar_right, BorderLayout.EAST);
	}

}
