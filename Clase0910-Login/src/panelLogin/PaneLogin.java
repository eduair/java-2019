package panelLogin;

import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.BoxLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import Session.Main;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PaneLogin extends JPanel {
	private JTextField textFieldNombre;
	private JTextField passowordField;
	private JButton btnSalir = new JButton("SALIR");

	/**
	 * Create the panel.
	 */
	public PaneLogin() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JPanel panelTextoLogIn = new JPanel();
		add(panelTextoLogIn);
		panelTextoLogIn.setLayout(new BoxLayout(panelTextoLogIn, BoxLayout.X_AXIS));
		
		JLabel loginText = new JLabel("Login");
		panelTextoLogIn.add(loginText);
		loginText.setFont(new Font("Tahoma", Font.PLAIN, 32));
		loginText.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel panelNombre = new JPanel();
		panelNombre.setMinimumSize(new Dimension(200,30));

		add(panelNombre);
		panelNombre.setLayout(new FlowLayout(FlowLayout.CENTER, 40, 30));
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNombre.setPreferredSize(new Dimension(100,30));
		panelNombre.add(lblNombre);
		
		textFieldNombre = new JTextField();
		panelNombre.add(textFieldNombre);
		textFieldNombre.setColumns(30);
		
		JPanel panelClave = new JPanel();
		add(panelClave);
		panelClave.setLayout(new FlowLayout(FlowLayout.CENTER, 40, 30));

		JLabel lblClave = new JLabel("Clave");
		lblClave.setHorizontalAlignment(SwingConstants.CENTER);
		lblClave.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblClave.setPreferredSize(new Dimension(100,30));
		panelClave.add(lblClave);
		
		passowordField = new JPasswordField();
		passowordField.setColumns(30);

		panelClave.add(passowordField);
		
		JPanel panelBotones = new JPanel();
		panelBotones.setAlignmentY(Component.TOP_ALIGNMENT);
		add(panelBotones);
		panelBotones.setLayout(new FlowLayout(FlowLayout.CENTER, 40, 30));
		
		JButton btnIngresar = new JButton("INGRESAR");
		btnIngresar.setPreferredSize(new Dimension(100,30));
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String inputNombreText = textFieldNombre.getText();
				String inputPasswordText = passowordField.getText();
				System.out.println(inputNombreText);
				boolean checkUsername = inputNombreText.equals(Main.VALID_USERNAME);
				boolean checkPassword = inputPasswordText.equals(Main.VALID_PASSWORD);
				if (checkUsername && checkPassword) {
					Main.setVisibleMain();
				} else {
					JOptionPane.showMessageDialog(null, "Error");
				}
			}
		});
		panelBotones.add(btnIngresar);
		
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.cerrarGestor();
			}
		});
		btnSalir.setPreferredSize(new Dimension(100,30));
		panelBotones.add(btnSalir);

	}
}
