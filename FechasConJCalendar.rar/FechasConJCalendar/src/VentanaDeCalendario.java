import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JCalendar;

public class VentanaDeCalendario extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaDeCalendario frame = new VentanaDeCalendario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaDeCalendario() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new FlowLayout());
		setContentPane(contentPane);
		
		JCalendar calendario = new JCalendar();
		
		add(calendario);
		
		JButton btnMostrarFecha = new JButton("Mostrar fecha seleccionada");
		btnMostrarFecha.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Date fecha = calendario.getDate();
				LocalDate fechaComoLD = fecha.toInstant()
			      .atZone(ZoneId.systemDefault())
			      .toLocalDate();
				System.out.println(fechaComoLD.toString());
				System.out.println(fechaComoLD.getDayOfMonth());
				System.out.println(fechaComoLD.getMonthValue());
				System.out.println(fechaComoLD.getYear());
			}
		});
		add(btnMostrarFecha);
	}

}
