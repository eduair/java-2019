
public class EstadoDT {
	private String _estado;
	private int _x;
	private int _y;
	
	public EstadoDT(String estado, int x, int y) {
		this._estado = estado;
		this._y = y;
		this._x = x;
	}
	
	public EstadoDT(int x, int y) {
		this(null, x, y);
	}
	
	public EstadoDT(String estado) {
		this(estado,0, 0);
	}
	
	public int get_x() {
		return _x;
	}

	public int get_y() {
		return _y;
	}

	public String getEstado() {
		return this._estado;
	}
	
	

}	
