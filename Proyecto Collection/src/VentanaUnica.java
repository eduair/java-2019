import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.time.chrono.IsoChronology;
import java.util.Iterator;
import java.util.Random;
import java.util.Stack;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JList;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.Font;

public class VentanaUnica extends JFrame {
	String LOGIN_LAYOUT = "Login";
	private String estado_actual = LOGIN_LAYOUT;
	private JPanel contentPane;
	private JPanel panelAuxiliar;
	private JMenuItem logout;
	private JPanel _panelPrincipal;
	private JTextField estado_field;
	private JList listDeshacer;
	private JList listRehacer;
	private DefaultListModel modelDeshacer;
	private DefaultListModel<String> modelRehacer;
	private JButton btnDeshacer;
	private JButton btnRehacer;
	private JButton btnGuardar;
	
	
	public VentanaUnica() {
		
		JMenuBar menubar = new JMenuBar();
		JMenu menGestionarUsuarios = new JMenu("Gestionar");
		JMenuItem menuUndo = new JMenuItem("Deshacer");
		JMenuItem menuRehacer = new JMenuItem("Rehacer");
		JMenuItem menuSave = new JMenuItem("Guardar");

		menuUndo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnDeshacer.doClick();
			}
		});
		menuRehacer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnRehacer.doClick();
			}
		});
		
		menuSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnGuardar.doClick();
			}
		});
		setJMenuBar(menubar);
		menuUndo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, ActionEvent.CTRL_MASK));
		menuSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		menuRehacer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, ActionEvent.CTRL_MASK));

		menubar.add(menGestionarUsuarios);
		menGestionarUsuarios.add(menuUndo);
		menGestionarUsuarios.add(menuRehacer);
		menGestionarUsuarios.add(menuSave);
		
		
		
		getContentPane().setLayout(new BorderLayout(0, 0));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		modelDeshacer = new DefaultListModel<String>();
		
		
		modelRehacer = new DefaultListModel<String>();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		JPanel panel_5 = new JPanel();
		contentPane.add(panel_5);
		panel_5.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		listDeshacer = new JList<String>(modelDeshacer);
		JScrollPane scrollPane1 = new JScrollPane(listDeshacer);
		panel_5.add(scrollPane1);
		scrollPane1.setPreferredSize(new Dimension(200,300));
		
		JPanel panel = new JPanel();
		panel_5.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		
		estado_field = new JTextField();
		panel_1.add(estado_field);
		estado_field.setColumns(20);
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2);
		
		btnGuardar = new JButton("Guardar Estado");
		panel_2.add(btnGuardar);
		
		JPanel panel_3 = new JPanel();
		panel.add(panel_3);
		
		btnDeshacer = new JButton("Deshacer Estado");
		panel_3.add(btnDeshacer);
		
		JPanel panel_4 = new JPanel();
		panel.add(panel_4);
		
		btnRehacer = new JButton("Rehacer Estado");
		panel_4.add(btnRehacer);
		listRehacer = new JList<String>(modelRehacer);
		JScrollPane scrollPane = new JScrollPane(listRehacer);
		panel_5.add(scrollPane);
		scrollPane.setPreferredSize(new Dimension(200,300));
		
		JPanel panel_6 = new JPanel();
		contentPane.add(panel_6);
		panel_6.setLayout(null);
		panel_6.setPreferredSize(new Dimension(500,300));
		int width = 800;
		int height = 100;
		Random rand = new Random();
		JLabel lbl1 = new JLabel("1");
		lbl1.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lbl1.setBounds(rand.nextInt(width),rand.nextInt(height), 60, 60);
		panel_6.add(lbl1);
		
		JLabel lbl2 = new JLabel("2");
		lbl2.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lbl2.setBounds(rand.nextInt(width),rand.nextInt(height), 60, 60);
		panel_6.add(lbl2);
		
		JLabel lbl3 = new JLabel("3");
		lbl3.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lbl3.setBounds(rand.nextInt(width),rand.nextInt(height), 60, 60);
		panel_6.add(lbl3);
		
		JLabel lbl4 = new JLabel("4");
		lbl4.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lbl4.setBounds(rand.nextInt(width),rand.nextInt(height), 60, 60);
		panel_6.add(lbl4);
		
		JLabel lbl5 = new JLabel("5");
		lbl5.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lbl5.setBounds(rand.nextInt(width),rand.nextInt(height), 60, 60);
		panel_6.add(lbl5);
		
		EstadoDT [] estadosPos = new EstadoDT[5];
		for (int i = 0; i < estadosPos.length; i++) {
			estadosPos[i] = new EstadoDT(10,20);
		}
		
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EstadoDT estado = obtenerEstadoActual();
				ViewController.getInstance().guardarEstado(estado);
				ViewController.getInstance().refrescarListas();
			}
		});
		
		btnDeshacer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EstadoDT estado = obtenerEstadoActual();
				EstadoDT escribir = ViewController.getInstance().deshacerEstado(estado);
				estado_field.setText(escribir.getEstado());
				ViewController.getInstance().refrescarListas();
			}
		});
		
		btnRehacer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EstadoDT estado = obtenerEstadoActual();
				EstadoDT escribir = ViewController.getInstance().rehacerEstado(estado);
				estado_field.setText(escribir.getEstado());
				ViewController.getInstance().refrescarListas();
			}
		});
	}
	
	private EstadoDT obtenerEstadoActual() {
		return new EstadoDT(estado_field.getText().toString());
	}
	
	public void refrescarListas(Stack<EstadoDT> estadosGuardados,  Stack<EstadoDT> estadosDesechos) 
	{
		/**
		 * Refresco lista deshacer.
		 */
		Iterator<EstadoDT> it = estadosDesechos.iterator();
		
		DefaultListModel<String> modelo = (DefaultListModel<String>) listDeshacer.getModel();
		
		modelo.clear();
		
		
		while (it.hasNext()) 
		{
			EstadoDT dtEstado = (EstadoDT) it.next();
			modelo.addElement(dtEstado.getEstado());
		}
		
		
		
		/**
		 * Refresco lista rehacer.
		 */
		
		
		it = estadosGuardados.iterator();
		
		modelo = (DefaultListModel<String>) listRehacer.getModel();
		
		modelo.clear();
		
		
		while (it.hasNext()) 
		{
			EstadoDT dtEstado = (EstadoDT) it.next();
			modelo.addElement(dtEstado.getEstado());
		}
		
		this.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				System.out.println(e.getKeyCode());
				System.out.println(KeyEvent.VK_Z);
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				System.out.println(e.getKeyCode());
				System.out.println(KeyEvent.VK_Z);
				if (e.getKeyCode() == KeyEvent.VK_Z && e.isControlDown()) {
					 System.out.println("woot!");
				 }
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				System.out.println(e.getKeyCode());
				System.out.println(KeyEvent.VK_Z);
			};
		});
		
	}
}
