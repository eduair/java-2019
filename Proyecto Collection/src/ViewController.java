import java.util.Stack;

import javax.swing.JOptionPane;

/*Singleton: me protege de otras instancias*/
public class ViewController {
	private static ViewController _instance;
	private VentanaUnica _ventanaUnica;
	private Stack<EstadoDT> estadosGuardados = new Stack<EstadoDT>();
	private Stack<EstadoDT> estadosDesechos = new Stack<EstadoDT>();
	public static ViewController getInstance() {
		
		if(_instance == null) {
			_instance = new ViewController();
		}
		
		return _instance;
	}
	
	private ViewController() 
	{
		System.out.println("Creando instancia de ViewController");
	}
	
	public void inicializarVista() {
		_ventanaUnica = new VentanaUnica(); // ALT + SHIFT + T - Convert Local Var to Field
		_ventanaUnica.setVisible(true);
		System.out.println("Inicializando Vista");
	}

	public void guardarEstado(EstadoDT estado) {
		if(estadosGuardados.size() == 0 || !estado.equals(estadosGuardados.peek())) {
			estadosGuardados.push(estado);
		}
		estadosDesechos.clear();
		
	}

	public EstadoDT deshacerEstado(EstadoDT estado) {
		EstadoDT returnEstado = estado;
		if(estadosGuardados.size() > 0) {
			estadosDesechos.push(estado);
			returnEstado = estadosGuardados.pop();
		}
		return returnEstado;
	}

	public EstadoDT rehacerEstado(EstadoDT estado) {
		EstadoDT returnEstado = estado;
		if(estadosDesechos.size() > 0) {
			estadosGuardados.push(estado);
			returnEstado = estadosDesechos.pop();
		}
		return returnEstado;
	}
	
	public void refrescarListas() {
		_ventanaUnica.refrescarListas(this.estadosGuardados, this.estadosDesechos);
	}
	
}
