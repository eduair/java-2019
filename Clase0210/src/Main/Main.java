 package Main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import Objetos.Auto;
import Objetos.Carga;
import Objetos.ITransportable;
import Objetos.ITransporte;
import Objetos.MonopatínGrin;
import Objetos.Persona;
import Ventana.Ventana;

/* Interfaces, asociación entre clases
 * UML -- <<interface>> Nombre*/
public class Main {
	public static final int GLOBAL_WIDTH = 70;
	public static final int GLOBAL_HEIGHT = 30;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("MAIN INICIANDO::");
		Ventana ventana = new Ventana();
		ventana.setVisible(true);
		
		ITransporte transportito = new MonopatínGrin();
		ITransportable[] transportados = new ITransportable[1];
		//transportados[0] = new Persona(0,0);
		//transportados[1] = new Persona(70,0);
		transportados[0] = new Carga(70,50,700);
		
		
		JButton btnMover = new JButton("Mover");
		btnMover.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				transportito.transportar(transportados, 0, 0);
			}
		});
		btnMover.setBounds(0, 222, 126, 40);
		ventana.AddButton(btnMover);

		
	}

}
