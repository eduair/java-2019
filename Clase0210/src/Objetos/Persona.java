package Objetos;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import Main.Main;
import Ventana.Ventana;

public class Persona implements ITransportable {
	private int _x = 20;
	private int _y = 10;
	private JLabel _labelPersona;
	public Persona(int x, int y) {
		this._x = x;
		this._y = y;
		Border border = BorderFactory.createLineBorder(Color.RED, 1);
		_labelPersona = new JLabel("Persona " + Ventana.getNumItem());
		_labelPersona.setBounds(_x, _y, Main.GLOBAL_WIDTH, Main.GLOBAL_HEIGHT);
		_labelPersona.setHorizontalAlignment(SwingConstants.CENTER);
		_labelPersona.setBorder(border);
		Ventana.AddJLabel(_labelPersona);
	}
	@Override
	public void setX(int x) {
		this._x = x;
		_labelPersona.setBounds(_x, _y, Main.GLOBAL_WIDTH, Main.GLOBAL_HEIGHT);
		System.out.println("La persona fue movida a x: " + this._x);
	}
	@Override
	public void setY(int y) {
		this._y = y;
		_labelPersona.setBounds(_x, _y, Main.GLOBAL_WIDTH, Main.GLOBAL_HEIGHT);
		System.out.println("La persona fue movida a y: " + this._y);
	}
	@Override
	public int getPeso() {
		// TODO Auto-generated method stub
		return 0;
	}}


