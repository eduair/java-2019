package Objetos;
// Es obligatorio que las interfaces no le den cuerpo a las funciones
// Diferencia con abstracto, no tiene la palabra abstract
// Solamente permiten definir qu� funciones quiero hacer
// Solo define funciones y solo son p�blicas
public interface ITransporte {
	public void transportar(ITransportable[] transportable, int x, int y);
}
