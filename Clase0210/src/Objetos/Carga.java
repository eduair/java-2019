package Objetos;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import Main.Main;
import Ventana.Ventana;

public class Carga implements ITransportable {
	 private int _x;
	 private int _y;
	 private int _cantKg;
	 private JLabel _labelCarga;
	public Carga(int x, int y, int cantKg) {
		this._x = x;
		this._y = y;
		this._cantKg = cantKg;
		Border border = BorderFactory.createLineBorder(Color.BLUE, 1);
		_labelCarga = new JLabel("Carga " + Ventana.getNumItem());
		_labelCarga.setHorizontalAlignment(SwingConstants.CENTER);
		_labelCarga.setBounds(_x, _y, Main.GLOBAL_WIDTH, Main.GLOBAL_HEIGHT);
		_labelCarga.setBorder(border);
		Ventana.AddJLabel(_labelCarga);
	}
	
	
	@Override
	public void setX(int x) {
		this._x = x;
		_labelCarga.setBounds(_x, _y, Main.GLOBAL_WIDTH, Main.GLOBAL_HEIGHT);
		System.out.println("La persona fue movida a x: " + this._x);
		
	}

	@Override
	public void setY(int y) {
		this._y = y;
		_labelCarga.setBounds(_x, _y, Main.GLOBAL_WIDTH, Main.GLOBAL_HEIGHT);
		System.out.println("La persona fue movida a y: " + this._y);
	}


	@Override
	public int getPeso() {
		return _cantKg;
	}

}
