package Objetos;

public class Auto implements ITransporte, ITransportable {

	private static final int MAX_CAPACIDAD_CARGA = 800; //KG
	@Override
	public void transportar(ITransportable [] transportable, int x, int y) {
		// TODO Auto-generated method stub
		int pesoATransportar = 0;
		for (int i = 0; i < transportable.length; i++) {
			pesoATransportar= pesoATransportar + transportable[i].getPeso();
		}
		if (pesoATransportar > MAX_CAPACIDAD_CARGA) {
			System.out.println("No se puede transportar, peso superior a " + MAX_CAPACIDAD_CARGA);
		} else {
			for (int i = 0; i < transportable.length; i++) {
				transportable[i].setX(x);
				transportable[i].setY(y);
			}
		}
	}

	@Override
	public void setX(int x) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setY(int y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPeso() {
		// TODO Auto-generated method stub
		return 0;
	}

}
