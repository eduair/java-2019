package Objetos;

public interface ITransportable {
	public void setX(int x);
	public void setY (int y);
	public int getPeso();
}
