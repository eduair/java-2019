package Objetos;

public class MonopatínGrin implements ITransporte {

	@Override
	public void transportar(ITransportable[] transportable, int x, int y) {
		boolean validarPersona = transportable[0] instanceof Persona;
		if (transportable.length == 1 && validarPersona) {
			transportable[0].setX(x);
			transportable[0].setY(y);
		} else {
			System.out.println("Error, cantidad o tipo de pasajeros inválido");
		}
	}

}
