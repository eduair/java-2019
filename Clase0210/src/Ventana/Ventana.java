package Ventana;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ventana extends JFrame {
	public static int numItem = 0;

	private static JPanel contentPane = new JPanel();

	public Ventana() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	}
	
	public void AddButton(JButton button) {
		contentPane.add(button);
	}
	
	public static void AddJLabel(JLabel jlabel) {
		contentPane.add(jlabel);
	}

	public static int getNumItem() {
		int oldVentana = numItem;
		numItem++;
		return oldVentana;
	}

	public static void setNumItem(int numItem) {
		Ventana.numItem = numItem;
	}
}
